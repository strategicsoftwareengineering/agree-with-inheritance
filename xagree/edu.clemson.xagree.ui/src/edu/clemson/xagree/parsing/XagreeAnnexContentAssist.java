package edu.clemson.xagree.parsing;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.DataPort;
import org.osate.aadl2.DefaultAnnexLibrary;
import org.osate.aadl2.DefaultAnnexSubclause;
import org.osate.aadl2.EventDataPort;
import org.osate.aadl2.NamedElement;
import org.osate.annexsupport.AnnexContentAssist;
import org.osate.annexsupport.AnnexUtil;
import org.osate.xtext.aadl2.properties.ui.contentassist.PropertiesProposalProvider;

import com.google.inject.Injector;
import edu.clemson.xagree.xagree.AgreeContract;
import edu.clemson.xagree.xagree.AgreeContractLibrary;
import edu.clemson.xagree.xagree.AgreeLibrary;
import edu.clemson.xagree.xagree.XagreePackage;
import edu.clemson.xagree.xagree.Arg;
import edu.clemson.xagree.xagree.ConstStatement;
import edu.clemson.xagree.xagree.Contract;
import edu.clemson.xagree.xagree.NestedDotID;
import edu.clemson.xagree.xagree.RecordDefExpr;
import edu.clemson.xagree.xagree.RecordType;
import edu.clemson.xagree.xagree.SpecStatement;
import edu.clemson.xagree.xagree.Type;
import edu.clemson.xagree.ui.internal.XagreeActivator;

import edu.clemson.xagree.ui.contentassist.XagreeProposalProvider;

public class XagreeAnnexContentAssist implements AnnexContentAssist {
	final private Injector injector = XagreeActivator.getInstance().getInjector(
			XagreeActivator.EDU_CLEMSON_XAGREE_XAGREE);


	private PropertiesProposalProvider propPropProv;
	private XagreeAnnexParser parser;
	private EObjectAtOffsetHelper offsetHelper;


	protected PropertiesProposalProvider getLinkingService() {
		if (propPropProv == null) {
			propPropProv = injector.getInstance(XagreeProposalProvider.class);
		}
		return propPropProv;
	}
	
	protected EObjectAtOffsetHelper getOffsetHelper() {
		if(offsetHelper == null){
			offsetHelper = injector.getInstance(EObjectAtOffsetHelper.class);
		}
		return offsetHelper;
	}

	@Override
	public List<String> annexCompletionSuggestions(EObject defaultAnnex, int offset) {

		offset = (offset <= 0) ? 0 : offset - 1; //get one character back
		EObjectAtOffsetHelper helper = getOffsetHelper();
		EObject grammerObject = null;
		//EObjectAtOffsetHelper
		if(defaultAnnex instanceof DefaultAnnexLibrary){
			AnnexLibrary annexLib = ((DefaultAnnexLibrary)defaultAnnex).getParsedAnnexLibrary();
			XtextResource resource = (XtextResource)annexLib.eResource();
			grammerObject = helper.resolveContainedElementAt(resource, offset);
		}else if(defaultAnnex instanceof DefaultAnnexSubclause){
			AnnexSubclause annexSub = ((DefaultAnnexSubclause)defaultAnnex).getParsedAnnexSubclause();
			XtextResource resource = (XtextResource)annexSub.eResource();
			grammerObject = helper.resolveContainedElementAt(resource, offset);
		}
		
		List<String> results = new ArrayList<>();
		if(grammerObject instanceof NestedDotID){
			results.addAll(getNestedDotIDCandidates((NestedDotID)grammerObject));
		}
		
		return results;
	}
	
	private List<String> getNestedDotIDCandidates(AadlPackage aadlPackage) {
		
		AgreeContract contract = null;
		List<String> results = new ArrayList<>();
		for (AnnexLibrary annex :  AnnexUtil.getAllActualAnnexLibraries(aadlPackage, XagreePackage.eINSTANCE.getAgreeContractLibrary())) {
            if (annex instanceof AgreeLibrary) { 
            	contract = (AgreeContract) ((AgreeContractLibrary) annex).getContract();
            }
        }
		
		if(contract != null){
			for(SpecStatement spec : contract.getSpecs()){
				if(spec instanceof ConstStatement){
					results.add(((ConstStatement) spec).getName());
				}
			}

		}
		
		return results;
	}
	
	private List<String> getNestedDotIDCandidates(NamedElement namedEl) {
		List<String> results = new ArrayList<>();
		
		List<NamedElement> namedEls = new ArrayList<NamedElement>();
		if(namedEl instanceof ComponentImplementation){
			namedEls.addAll(((ComponentImplementation) namedEl).getAllSubcomponents());
		}else if(namedEl instanceof RecordDefExpr){
			namedEls.addAll(((RecordDefExpr) namedEl).getArgs());
		}
		for(NamedElement el : namedEls){
			results.add(el.getName());
		}
		return results;
	}
	
	private List<String> getNestedDotIDCandidates(NestedDotID id) {
		
		NamedElement base = id.getBase();
		NamedElement namedEl = null;
		
		if(base instanceof Arg){
			Type type = ((Arg) base).getType();
			NestedDotID elID = ((RecordType) type).getRecord();
    		namedEl = elID.getBase();
		}else if(base instanceof DataPort){
			namedEl = ((DataPort) base).getDataFeatureClassifier();
		}else if(base instanceof EventDataPort){
			namedEl = ((EventDataPort) base).getDataFeatureClassifier();
		}else if(base instanceof AadlPackage){
			return getNestedDotIDCandidates((AadlPackage)base);
		}else{
			return new ArrayList<>();
		}
		
		return getNestedDotIDCandidates(namedEl);
	}

}

package edu.clemson.xagree.parsing;

import org.osate.aadl2.AnnexLibrary;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.modelsupport.errorreporting.ParseErrorReporter;
import org.osate.annexsupport.AnnexParseUtil;
import org.osate.annexsupport.AnnexParser;

import com.google.inject.Injector;
import edu.clemson.xagree.parser.antlr.XagreeParser;
import edu.clemson.xagree.services.XagreeGrammarAccess;
import edu.clemson.xagree.ui.internal.XagreeActivator;

// Based on EMV2AnnexParser from Error Model annex
public class XagreeAnnexParser implements AnnexParser {
    private XagreeParser parser;

    protected XagreeParser getParser() {
        if (parser == null) {
            Injector injector = XagreeActivator.getInstance().getInjector(
                    XagreeActivator.EDU_CLEMSON_XAGREE_XAGREE);
            parser = injector.getInstance(XagreeParser.class);
        }
        return parser;
    }

    protected XagreeGrammarAccess getGrammarAccess() {
        return getParser().getGrammarAccess();
    }

    public AnnexLibrary parseAnnexLibrary(String annexName, String source, String filename,
            int line, int column, ParseErrorReporter errReporter) {
        return (AnnexLibrary) AnnexParseUtil.parse(getParser(),source, getGrammarAccess().getAgreeLibraryRule(), filename, line,
                column, errReporter);
    }

    public AnnexSubclause parseAnnexSubclause(String annexName, String source, String filename,
            int line, int column, ParseErrorReporter errReporter) {
        return (AnnexSubclause) AnnexParseUtil.parse(getParser(),source,getGrammarAccess().getAgreeSubclauseRule(),filename,line,column, errReporter);
    }


}
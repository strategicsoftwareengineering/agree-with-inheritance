package edu.clemson.xagree.analysis;

public class XagreeException extends RuntimeException {
    private static final long serialVersionUID = -2404922400955586291L;

    public XagreeException(String message) {
        super(message);
    }
}

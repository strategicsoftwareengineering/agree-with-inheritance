package edu.clemson.xagree.analysis.realtime;

import java.math.BigInteger;

import org.eclipse.emf.ecore.EObject;

import edu.clemson.xagree.xagree.AlwaysStatement;
import edu.clemson.xagree.xagree.ClosedTimeInterval;
import edu.clemson.xagree.xagree.OpenLeftTimeInterval;
import edu.clemson.xagree.xagree.OpenRightTimeInterval;
import edu.clemson.xagree.xagree.OpenTimeInterval;
import edu.clemson.xagree.xagree.PeriodicStatement;
import edu.clemson.xagree.xagree.SporadicStatement;
import edu.clemson.xagree.xagree.TimeInterval;
import edu.clemson.xagree.xagree.WhenHoldsStatement;
import edu.clemson.xagree.xagree.WhenOccursStatment;
import edu.clemson.xagree.xagree.WheneverBecomesTrueStatement;
import edu.clemson.xagree.xagree.WheneverHoldsStatement;
import edu.clemson.xagree.xagree.WheneverImpliesStatement;
import edu.clemson.xagree.xagree.WheneverOccursStatement;
import edu.clemson.xagree.xagree.util.XagreeSwitch;

import edu.clemson.xagree.analysis.XagreeException;
import edu.clemson.xagree.analysis.ast.XagreeASTBuilder;
import edu.clemson.xagree.analysis.ast.XagreeStatement;
import edu.clemson.xagree.analysis.realtime.XagreeCauseEffectPattern.TriggerType;
import edu.clemson.xagree.analysis.realtime.XagreePatternInterval.IntervalType;
import jkind.lustre.BinaryExpr;
import jkind.lustre.BinaryOp;
import jkind.lustre.Expr;
import jkind.lustre.IdExpr;
import jkind.lustre.IntExpr;

public class XagreePatternBuilder extends XagreeSwitch<XagreeStatement> {

    private final String str;
    private final EObject ref;
    private final XagreeASTBuilder builder;

    public XagreePatternBuilder(String str, EObject ref, XagreeASTBuilder builder) {
        this.str = str;
        this.ref = ref;
        this.builder = builder;
    }

    @Override
    public XagreeStatement caseAlwaysStatement(AlwaysStatement object) {
        return new XagreeStatement(str, builder.doSwitch(object.getExpr()), ref);
    }

    @Override
    public XagreeStatement casePeriodicStatement(PeriodicStatement object){
        IdExpr event = (IdExpr) builder.doSwitch(object.getEvent());
        
        edu.clemson.xagree.xagree.Expr jitter = object.getJitter();
        Expr jitterExpr = null;
        if (jitter != null) {
            jitterExpr = builder.doSwitch(jitter);
        }
        Expr period = builder.doSwitch(object.getPeriod());
        
        return new XagreePeriodicPattern(str, ref, event, period, jitterExpr);
    }
    
    @Override
    public XagreeStatement caseSporadicStatement(SporadicStatement object){
        IdExpr event = (IdExpr) builder.doSwitch(object.getEvent());
        edu.clemson.xagree.xagree.Expr jitter = object.getJitter();
        Expr jitterExpr = null;
        if (jitter != null) {
            jitterExpr = builder.doSwitch(jitter);
        }
        Expr iat = builder.doSwitch(object.getIat());
        
        return new XagreeSporadicPattern(str, ref, event, iat, jitterExpr);
    }
    
    @Override
    public XagreeStatement caseWheneverHoldsStatement(WheneverHoldsStatement object) {
        IdExpr cause = (IdExpr) builder.doSwitch(object.getCause());
        IdExpr effect = (IdExpr) builder.doSwitch(object.getEffect());
        boolean exclusive = object.getExcl() != null;
        
        XagreePatternInterval interval = getIntervalType(object.getInterval());
        
        return new XagreeCauseEffectPattern(str, ref, exclusive, cause, effect, null, interval, TriggerType.EVENT,
                TriggerType.CONDITION);
    }

    @Override
    public XagreeStatement caseWheneverImpliesStatement(WheneverImpliesStatement object) {
        IdExpr cause = (IdExpr) builder.doSwitch(object.getCause());
        IdExpr lhs = (IdExpr) builder.doSwitch(object.getLhs());
        IdExpr rhs = (IdExpr) builder.doSwitch(object.getRhs());
        boolean exclusive = object.getExcl() != null;
        XagreePatternInterval effectInterval = getIntervalType(object.getInterval());

        Expr effect = new BinaryExpr(lhs, BinaryOp.IMPLIES, rhs);
        throw new XagreeException("We do not support this pattern currently");
//        return new AgreeCauseEffectPattern(str, ref, exclusive, cause, effect, null, effectInterval, TriggerType.EVENT,
//                TriggerType.CONDITION);
    }

    @Override
    public XagreeStatement caseWheneverOccursStatement(WheneverOccursStatement object) {
        IdExpr cause = (IdExpr) builder.doSwitch(object.getCause());
        IdExpr effect = (IdExpr) builder.doSwitch(object.getEffect());
        boolean exclusive = object.getExcl() != null;
        XagreePatternInterval effectInterval = getIntervalType(object.getInterval());

        return new XagreeCauseEffectPattern(str, ref, exclusive, cause, effect, null, effectInterval, TriggerType.EVENT,
                TriggerType.EVENT);
    }

    @Override
    public XagreeStatement caseWheneverBecomesTrueStatement(WheneverBecomesTrueStatement object) {
        IdExpr cause = (IdExpr) builder.doSwitch(object.getCause());
        IdExpr effect = (IdExpr) builder.doSwitch(object.getEffect());
        boolean exclusive = object.getExcl() != null;
        XagreePatternInterval effectInterval = getIntervalType(object.getInterval());

        // make the effect rising edge sensitive
//        Expr preEffect = new UnaryExpr(UnaryOp.PRE, effect);
//        Expr notPreEffect = new UnaryExpr(UnaryOp.NOT, preEffect);
//        Expr edgeEffect = new BinaryExpr(notPreEffect, BinaryOp.AND, effect);
//        effect = new BinaryExpr(effect, BinaryOp.ARROW, edgeEffect);
        return new XagreeCauseEffectPattern(str, ref, exclusive, cause, effect, null, effectInterval, TriggerType.EVENT,
                TriggerType.EVENT);
    }

    @Override
    public XagreeStatement caseWhenHoldsStatement(WhenHoldsStatement object) {
        IdExpr condition = (IdExpr) builder.doSwitch(object.getCondition());
        IdExpr effect = (IdExpr) builder.doSwitch(object.getEvent());
        boolean exclusive = object.getExcl() != null;
        XagreePatternInterval conditionInterval = getIntervalType(object.getConditionInterval());
        XagreePatternInterval effectInterval = getIntervalType(object.getEventInterval());

        return new XagreeCauseEffectPattern(str, ref, exclusive, condition, effect, conditionInterval, effectInterval,
                TriggerType.CONDITION, TriggerType.EVENT);
    }

    @Override
    public XagreeStatement caseWhenOccursStatment(WhenOccursStatment object) {
        IdExpr condition = (IdExpr) builder.doSwitch(object.getCondition());
        IdExpr effect = (IdExpr) builder.doSwitch(object.getEvent());
        Expr timesExpr = builder.doSwitch(object.getTimes());
        boolean exclusive = object.getExcl() != null;
        if (!(timesExpr instanceof IntExpr)) {
            throw new XagreeException("Expected an integer literal in 'When Occurs' pattern");
        }
        BigInteger times = ((IntExpr) timesExpr).value;
        XagreePatternInterval interval = getIntervalType(object.getInterval());

        return new XagreeTimesPattern(str, ref, exclusive, condition, effect, interval, null, TriggerType.CONDITION,
                TriggerType.CONDITION, times, null);
    }
    
    private XagreePatternInterval getIntervalType(TimeInterval interval) {
        if(interval == null){
            return null;
        }
        Expr low = builder.doSwitch(interval.getLow());
        Expr high = builder.doSwitch(interval.getHigh());
        IntervalType type;

        if (interval instanceof OpenTimeInterval) {
            type = IntervalType.OPEN;
        } else if (interval instanceof OpenLeftTimeInterval) {
            type = IntervalType.OPEN_LEFT;
        } else if (interval instanceof OpenRightTimeInterval) {
            type = IntervalType.OPEN_RIGHT;
        } else if (interval instanceof ClosedTimeInterval) {
            type = IntervalType.CLOSED;
        } else {
            throw new XagreeException("Unhandled TimeInterval type: " + interval.getClass());
        }
        return new XagreePatternInterval(type, low, high);
    }


}

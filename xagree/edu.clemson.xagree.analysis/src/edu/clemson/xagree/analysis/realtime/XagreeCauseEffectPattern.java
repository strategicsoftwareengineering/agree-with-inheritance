package edu.clemson.xagree.analysis.realtime;

import org.eclipse.emf.ecore.EObject;

import edu.clemson.xagree.xagree.TimeInterval;

import edu.clemson.xagree.analysis.ast.XagreeStatement;
import jkind.lustre.Expr;
import jkind.lustre.IdExpr;

public class XagreeCauseEffectPattern extends XagreePattern {

    public enum TriggerType {
        EVENT, CONDITION
    };

    public final boolean effectIsExclusive;
    public final IdExpr cause;
    public final IdExpr effect;

    public final XagreePatternInterval causeInterval;
    public final XagreePatternInterval effectInterval;

    public final TriggerType causeType;
    public final TriggerType effectType;

    public XagreeCauseEffectPattern(String string, EObject reference, boolean effectIsExclusive, IdExpr cause, IdExpr effect,
            XagreePatternInterval causeInterval, XagreePatternInterval effectInterval, TriggerType causeType,
            TriggerType effectType) {
        super(string, null, reference);
        this.effectIsExclusive = effectIsExclusive;
        this.cause = cause;
        this.effect = effect;
        this.causeInterval = causeInterval;
        this.effectInterval = effectInterval;
        this.causeType = causeType;
        this.effectType = effectType;
    }

   
}

package edu.clemson.xagree.analysis.realtime;

import jkind.lustre.Expr;

public class XagreePatternInterval {
    public enum IntervalType {
        OPEN, OPEN_LEFT, OPEN_RIGHT, CLOSED
    };
    
    public final IntervalType type;
    public final Expr low;
    public final Expr high;
    
    public XagreePatternInterval(IntervalType type, Expr low, Expr high){
        this.type = type;
        this.low = low;
        this.high = high;
    }

}

package edu.clemson.xagree.analysis.realtime;

import org.eclipse.emf.ecore.EObject;

import jkind.lustre.Expr;
import jkind.lustre.IdExpr;

public class XagreePeriodicPattern extends XagreeRealtimePattern{

    public XagreePeriodicPattern(String string, EObject reference, IdExpr event, Expr period, Expr jitter) {
        super(string, reference, event, period, jitter);
    }

}

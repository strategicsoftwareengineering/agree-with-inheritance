package edu.clemson.xagree.analysis.realtime;

import java.math.BigInteger;

import org.eclipse.emf.ecore.EObject;

import edu.clemson.xagree.xagree.TimeInterval;

import jkind.lustre.Expr;
import jkind.lustre.IdExpr;

public class XagreeTimesPattern extends XagreeCauseEffectPattern{

    public final BigInteger causeTimes;
    public final BigInteger effectTimes;

    public XagreeTimesPattern(String string, EObject reference, boolean effectIsExclusive, IdExpr cause, IdExpr effect,
            XagreePatternInterval causeInterval, XagreePatternInterval effectInterval, TriggerType causeType,
            TriggerType effectType, BigInteger causeTimes, BigInteger effectTimes) {
        super(string, reference, effectIsExclusive, cause, effect, causeInterval, effectInterval, causeType, effectType);
        this.causeTimes = causeTimes;
        this.effectTimes = effectTimes;
    }

}

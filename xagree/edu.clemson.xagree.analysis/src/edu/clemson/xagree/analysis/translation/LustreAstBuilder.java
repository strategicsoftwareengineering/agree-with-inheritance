package edu.clemson.xagree.analysis.translation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.xtext.util.Tuples;
import org.osate.aadl2.impl.DataPortImpl;
import org.osate.aadl2.impl.EventDataPortImpl;
import org.eclipse.xtext.util.Pair;

import edu.clemson.xagree.xagree.Arg;
import edu.clemson.xagree.xagree.AssertStatement;
import edu.clemson.xagree.xagree.AssumeStatement;
import edu.clemson.xagree.xagree.EqStatement;
import edu.clemson.xagree.xagree.InputStatement;
import edu.clemson.xagree.xagree.LemmaStatement;

import edu.clemson.xagree.analysis.Activator;
import edu.clemson.xagree.analysis.XagreeException;
import edu.clemson.xagree.analysis.XagreeUtils;
import edu.clemson.xagree.analysis.ast.XagreeAADLConnection;
import edu.clemson.xagree.analysis.ast.XagreeASTBuilder;
import edu.clemson.xagree.analysis.ast.XagreeConnection;
import edu.clemson.xagree.analysis.ast.XagreeEquation;
import edu.clemson.xagree.analysis.ast.XagreeNode;
import edu.clemson.xagree.analysis.ast.XagreeNodeBuilder;
import edu.clemson.xagree.analysis.ast.XagreeOverriddenConnection;
import edu.clemson.xagree.analysis.ast.XagreeProgram;
import edu.clemson.xagree.analysis.ast.XagreeStatement;
import edu.clemson.xagree.analysis.ast.XagreeVar;
import edu.clemson.xagree.analysis.ast.XagreeNode.TimingModel;
import edu.clemson.xagree.analysis.ast.visitors.XagreeInlineLatchedConnections;
import edu.clemson.xagree.analysis.lustre.visitors.IdRewriteVisitor;
import edu.clemson.xagree.analysis.lustre.visitors.IdRewriter;
import edu.clemson.xagree.analysis.lustre.visitors.LustreCondactNodeVisitor;
import edu.clemson.xagree.analysis.preferences.PreferenceConstants;
import edu.clemson.xagree.analysis.realtime.XagreePatternTranslator;
import edu.clemson.xagree.analysis.realtime.XagreeRealtimeCalendarBuilder;
import jkind.lustre.BinaryExpr;
import jkind.lustre.BinaryOp;
import jkind.lustre.BoolExpr;
import jkind.lustre.Equation;
import jkind.lustre.Expr;
import jkind.lustre.IdExpr;
import jkind.lustre.IntExpr;
import jkind.lustre.NamedType;
import jkind.lustre.Node;
import jkind.lustre.NodeCallExpr;
import jkind.lustre.Program;
import jkind.lustre.TypeDef;
import jkind.lustre.UnaryExpr;
import jkind.lustre.UnaryOp;
import jkind.lustre.VarDecl;
import jkind.lustre.builders.NodeBuilder;


public class LustreAstBuilder {

	protected static List<Node> nodes;
	protected static final String guarSuffix = "__GUARANTEE";
	public static final String assumeSuffix = "__ASSUME";
	protected static final String lemmaSuffix = "__LEMMA";
	protected static final String assertSuffix = "__ASSERT";
	protected static final String historyNodeName = "__HIST";
	public static final String assumeHistSufix = assumeSuffix + historyNodeName;
	protected static final String patternPropSuffix = "__PATTERN";

	// private static AgreeProgram translate(AgreeProgram program){
	// return AgreeInlineLatchedConnections.translate(program);
	// }

	public static Program getRealizabilityLustreProgram(XagreeProgram agreeProgram) {

        List<TypeDef> types = XagreeUtils.getLustreTypes(agreeProgram);

        List<Expr> assertions = new ArrayList<>();
        List<VarDecl> locals = new ArrayList<>();
        List<VarDecl> inputs = new ArrayList<>();
        List<Equation> equations = new ArrayList<>();
        List<String> properties = new ArrayList<>();

        XagreeNode topNode = agreeProgram.topNode;

        for (XagreeStatement assumption : topNode.assumptions) {
            assertions.add(assumption.expr);
        }

        int i = 0;

        for (XagreeStatement guarantee : topNode.guarantees) {
            String guarName = guarSuffix + i++;
            locals.add(new XagreeVar(guarName, NamedType.BOOL, guarantee.reference, topNode.compInst, null));
            equations.add(new Equation(new IdExpr(guarName), guarantee.expr));
            properties.add(guarName);
        }

        List<String> inputStrs = new ArrayList<>();
        for (XagreeVar var : topNode.inputs) {
            inputs.add(var);
            inputStrs.add(var.id);
        }

        for (XagreeVar var : topNode.outputs) {
            inputs.add(var);
        }

        // perhaps we should break out eq statements into implementation
        // equations
        // and type equations. This would clear this up
        for (XagreeStatement statement : topNode.assertions) {
            if (XagreeUtils.referenceIsInContract(statement, topNode.compInst)){

                // this is a strange hack we have to do. we have to make
                // equation and property
                // statements not assertions. They should all be binary
                // expressions with an
                // equals operator. We will need to removing their corresponding
                // variable
				// from the inputs and add them to the local variables
            	BinaryExpr binExpr;
            	IdExpr varId;
				try {
					binExpr = (BinaryExpr) statement.expr;
					varId = (IdExpr) binExpr.left;
				} catch (ClassCastException e) {
					//some equation variables are assertions for
					//subrange types. do not translate these to
					//local equations. Just add them to assertions
					assertions.add(statement.expr);
					continue;
				}

                boolean found = false;
                int index;
                for (index = 0; index < inputs.size(); index++) {
                    VarDecl var = inputs.get(index);
                    if (var.id.equals(varId.id)) {
                        found = true;
                        break;
                    }

                }
                if (!found || binExpr.op != BinaryOp.EQUAL) {
                    throw new XagreeException(
                            "Something went very wrong with the lustre generation in the realizability analysis");
                }
                locals.add(inputs.remove(index));
                equations.add(new Equation(varId, binExpr.right));
            }
        }

        NodeBuilder builder = new NodeBuilder("main");
        builder.addInputs(inputs);
        builder.addLocals(locals);
        builder.addEquations(equations);
        builder.addProperties(properties);
        builder.addAssertions(assertions);
        builder.setRealizabilityInputs(inputStrs);
        
        Node main = builder.build();
        List<Node> nodes = new ArrayList<>();
        nodes.add(main);
        nodes.addAll(agreeProgram.globalLustreNodes);
        Program program = new Program(types, null, nodes, main.id);

        return program;

    }

	public static Program getAssumeGuaranteeLustreProgram(XagreeProgram agreeProgram) {

		nodes = new ArrayList<>();

		XagreeNode flatNode = flattenAgreeNode(agreeProgram, agreeProgram.topNode, "_TOP__");
		List<Expr> assertions = new ArrayList<>();
		List<VarDecl> locals = new ArrayList<>();
		List<VarDecl> inputs = new ArrayList<>();
		List<Equation> equations = new ArrayList<>();
		List<String> properties = new ArrayList<>();
		List<String> ivcs = new ArrayList<>();

		int j = 0;
		for (XagreeStatement assumption : flatNode.assumptions) {
			String assumName = assumeSuffix + j++;
			locals.add(new XagreeVar(assumName, NamedType.BOOL, assumption.reference, flatNode.compInst, null));
			IdExpr assumId = new IdExpr(assumName);
			equations.add(new Equation(assumId, assumption.expr));
			assertions.add(assumId);
			ivcs.add(assumId.id);
		}

		for (XagreeStatement assertion : flatNode.assertions) {
			assertions.add(assertion.expr);
		}

		// add assumption and monolithic lemmas first (helps with proving)
		for (XagreeVar var : flatNode.outputs) {
			if (var.reference instanceof AssumeStatement || var.reference instanceof LemmaStatement) {
				properties.add(var.id);
			}
			inputs.add(var);
		}
		
		// add property that all assumption history is true
		Expr assumeConj = new BoolExpr(true);
		for (XagreeNode subNode : agreeProgram.topNode.subNodes) {
			assumeConj = new BinaryExpr(new IdExpr(subNode.id + "__" + assumeHistSufix), BinaryOp.AND, assumeConj);
		}
		XagreeVar assumeHistVar = new XagreeVar(assumeHistSufix, NamedType.BOOL,
				agreeProgram.topNode.compInst.getComponentClassifier(), agreeProgram.topNode.compInst, null);
		locals.add(assumeHistVar);
		equations.add(new Equation(new IdExpr(assumeHistVar.id), assumeConj));
		properties.add(assumeHistVar.id);

		int k = 0;
		for (XagreeStatement patternPropState : flatNode.patternProps) {
			String patternVarName = patternPropSuffix + k++;
			locals.add(new XagreeVar(patternVarName, NamedType.BOOL, patternPropState, flatNode.compInst, null));
			equations.add(new Equation(new IdExpr(patternVarName), patternPropState.expr));
			properties.add(patternVarName);
		}

		int i = 0;
		for (XagreeStatement guarantee : flatNode.lemmas) {
			String guarName = guarSuffix + i++;
			locals.add(new XagreeVar(guarName, NamedType.BOOL, guarantee.reference, flatNode.compInst, null));
			equations.add(new Equation(new IdExpr(guarName), guarantee.expr));
			properties.add(guarName);
		}

		for (XagreeStatement guarantee : flatNode.guarantees) {
			String guarName = guarSuffix + i++;
			locals.add(new XagreeVar(guarName, NamedType.BOOL, guarantee.reference, flatNode.compInst, null));
			equations.add(new Equation(new IdExpr(guarName), guarantee.expr));
			properties.add(guarName);
		}

		for (XagreeVar var : flatNode.inputs) {
			inputs.add(var);
		}

		for (XagreeVar var : flatNode.locals) {
			locals.add(var);
		}

		equations.addAll(flatNode.localEquations);
		assertions.add(XagreeRealtimeCalendarBuilder.getTimeConstraint(flatNode.eventTimes));
		
		NodeBuilder builder = new NodeBuilder("main");
		builder.addInputs(inputs);
		builder.addLocals(locals);
		builder.addEquations(equations);
		builder.addProperties(properties);
		builder.addAssertions(assertions);
		builder.addIvcs(ivcs);

		Node main = builder.build();

		nodes.add(main);
		nodes.addAll(agreeProgram.globalLustreNodes);
		nodes.add(getHistNode());

		// add realtime constraint nodes
		nodes.addAll(XagreeRealtimeCalendarBuilder.getRealTimeNodes());
		List<TypeDef> types = XagreeUtils.getLustreTypes(agreeProgram);
		Program program = new Program(types, null, nodes, main.id);

		return program;

	}

	public static List<Pair<String, Program>> getConsistencyChecks(XagreeProgram agreeProgram) {

		List<Pair<String, Program>> programs = new ArrayList<>();
		List<TypeDef> types = XagreeUtils.getLustreTypes(agreeProgram);

		nodes = new ArrayList<>();

		Node topConsist = getConsistencyLustreNode(agreeProgram.topNode, false);
		// we don't want node lemmas to show up in the consistency check
		for (Node node : agreeProgram.globalLustreNodes) {
			nodes.add(removeProperties(node));
		}
		nodes.add(topConsist);
		nodes.add(getHistNode());
		nodes.addAll(XagreeRealtimeCalendarBuilder.getRealTimeNodes());

		Program topConsistProg = new Program(types, null, nodes, topConsist.id);

		programs.add(Tuples.create("This component consistent", topConsistProg));

		for (XagreeNode subNode : agreeProgram.topNode.subNodes) {
			nodes = new ArrayList<>();
			subNode = flattenAgreeNode(agreeProgram, subNode, "_TOP__");
			Node subConsistNode = getConsistencyLustreNode(subNode, true);
			for (Node node : agreeProgram.globalLustreNodes) {
				nodes.add(removeProperties(node));
			}
			nodes.add(subConsistNode);
			nodes.add(getHistNode());
			nodes.addAll(XagreeRealtimeCalendarBuilder.getRealTimeNodes());

			Program subConsistProg = new Program(types, null, nodes, subConsistNode.id);

			programs.add(Tuples.create(subNode.id + " consistent", subConsistProg));
		}

		nodes = new ArrayList<>();
		// agreeProgram = translate(agreeProgram);
		XagreeNode compositionNode = flattenAgreeNode(agreeProgram, agreeProgram.topNode, "_TOP__");

		Node topCompositionConsist = getConsistencyLustreNode(compositionNode, true);
		for (Node node : agreeProgram.globalLustreNodes) {
			nodes.add(removeProperties(node));
		}
		// nodes.addAll(agreeProgram.globalLustreNodes);
		nodes.add(topCompositionConsist);
		nodes.add(getHistNode());
		nodes.addAll(XagreeRealtimeCalendarBuilder.getRealTimeNodes());

		Program topCompositConsistProg = new Program(types, null, nodes, topCompositionConsist.id);

		programs.add(Tuples.create("Component composition consistent", topCompositConsistProg));

		return programs;
	}

	protected static Node removeProperties(Node node) {
		NodeBuilder builder = new NodeBuilder(node.id);
		builder.addInputs(node.inputs);
		builder.addOutputs(node.outputs);
		builder.addLocals(node.locals);
		builder.addEquations(node.equations);

		return builder.build();
	}

	protected static Node getConsistencyLustreNode(XagreeNode agreeNode, boolean withAssertions) {
		final String stuffPrefix = "__STUFF";

		List<Expr> assertions = new ArrayList<>();
		List<VarDecl> locals = new ArrayList<>();
		List<VarDecl> inputs = new ArrayList<>();
		List<Equation> equations = new ArrayList<>();
		List<String> properties = new ArrayList<>();
		List<String> ivcs = new ArrayList<>();

		Expr stuffConj = new BoolExpr(true);

		int stuffAssumptionIndex = 0;
		for (XagreeStatement assumption : agreeNode.assumptions) {
			XagreeVar stuffAssumptionVar = new XagreeVar(stuffPrefix + assumeSuffix + stuffAssumptionIndex++,
					NamedType.BOOL, assumption.reference, agreeNode.compInst, null);
			locals.add(stuffAssumptionVar);
			ivcs.add(stuffAssumptionVar.id);
			IdExpr stuffAssumptionId = new IdExpr(stuffAssumptionVar.id);
			equations.add(new Equation(stuffAssumptionId, assumption.expr));

			stuffConj = new BinaryExpr(stuffConj, BinaryOp.AND, stuffAssumptionId);
		}

		int stuffGuaranteeIndex = 0;
		for (XagreeStatement guarantee : agreeNode.guarantees) {
			XagreeVar stuffGuaranteeVar = new XagreeVar(stuffPrefix + guarSuffix + stuffGuaranteeIndex++,
					NamedType.BOOL, guarantee.reference, agreeNode.compInst, null);
			locals.add(stuffGuaranteeVar);
			ivcs.add(stuffGuaranteeVar.id);
			IdExpr stuffGuaranteeId = new IdExpr(stuffGuaranteeVar.id);
			equations.add(new Equation(stuffGuaranteeId, guarantee.expr));

			stuffConj = new BinaryExpr(stuffConj, BinaryOp.AND, stuffGuaranteeId);
		}

		if (withAssertions) {
			equations.addAll(agreeNode.localEquations);
		} else {
			for (XagreeEquation eq : agreeNode.localEquations) {
				if (XagreeUtils.referenceIsInContract(eq.reference, agreeNode.compInst)) {
					equations.add(eq);
				}
			}
		}
		// TODO should we include lemmas in the consistency check?
		// for(AgreeStatement guarantee : agreeNode.lemmas){
		// histConj = new BinaryExpr(histConj, BinaryOp.AND, guarantee.expr);
		// }

		int stuffAssertionIndex = 0;
		if (withAssertions) {
			for (XagreeStatement assertion : agreeNode.assertions) {
				XagreeVar stuffAssertionVar = new XagreeVar(stuffPrefix + assertSuffix + stuffAssertionIndex++,
						NamedType.BOOL, assertion.reference, null, null);
				locals.add(stuffAssertionVar);
				IdExpr stuffAssertionId = new IdExpr(stuffAssertionVar.id);
				equations.add(new Equation(stuffAssertionId, assertion.expr));

				stuffConj = new BinaryExpr(stuffConj, BinaryOp.AND, stuffAssertionId);
			}
		} else {
			// perhaps we should break out eq statements into implementation
			// equations  and type equations. That would clear this up.
			for (XagreeStatement assertion : agreeNode.assertions) {
				if (XagreeUtils.referenceIsInContract(assertion.reference, agreeNode.compInst)) {
					XagreeVar stuffAssertionVar = new XagreeVar(stuffPrefix + assertSuffix + stuffAssertionIndex++,
							NamedType.BOOL, assertion.reference, null, null);
					locals.add(stuffAssertionVar);
					IdExpr stuffAssertionId = new IdExpr(stuffAssertionVar.id);
					equations.add(new Equation(stuffAssertionId, assertion.expr));

					stuffConj = new BinaryExpr(stuffConj, BinaryOp.AND, stuffAssertionId);
				}
			}
		}

		// add realtime constraints
		Set<XagreeVar> eventTimes = new HashSet<>();
		if (withAssertions) {
			eventTimes.addAll(agreeNode.eventTimes);
		} else {
			for (XagreeVar eventVar : agreeNode.eventTimes) {
				if (XagreeUtils.referenceIsInContract(eventVar.reference, agreeNode.compInst)) {
					eventTimes.add(eventVar);
				}
			}
		}
		assertions.add(XagreeRealtimeCalendarBuilder.getTimeConstraint(eventTimes));

		for (XagreeVar var : agreeNode.inputs) {
			inputs.add(var);
		}

		for (XagreeVar var : agreeNode.outputs) {
			inputs.add(var);
		}

		for (XagreeVar var : agreeNode.locals) {
			if (withAssertions) {
				locals.add(var);
			} else {
				if (XagreeUtils.referenceIsInContract(var.reference, agreeNode.compInst)) {
					locals.add(var);
				}
			}
		}

		EObject classifier = agreeNode.compInst.getComponentClassifier();

		XagreeVar countVar = new XagreeVar("__COUNT", NamedType.INT, null, null, null);
		XagreeVar stuffVar = new XagreeVar(stuffPrefix, NamedType.BOOL, null, null, null);
		XagreeVar histVar = new XagreeVar("__HIST", NamedType.BOOL, null, null, null);
		XagreeVar propVar = new XagreeVar("__PROP", NamedType.BOOL, classifier, agreeNode.compInst, null);

		locals.add(countVar);
		locals.add(stuffVar);
		locals.add(histVar);
		locals.add(propVar);

		IdExpr countId = new IdExpr(countVar.id);
		IdExpr stuffId = new IdExpr(stuffVar.id);
		IdExpr histId = new IdExpr(histVar.id);
		IdExpr propId = new IdExpr(propVar.id);

		equations.add(new Equation(stuffId, stuffConj));

		Expr histExpr = new UnaryExpr(UnaryOp.PRE, histId);
		histExpr = new BinaryExpr(histExpr, BinaryOp.AND, stuffId);
		histExpr = new BinaryExpr(stuffId, BinaryOp.ARROW, histExpr);
		equations.add(new Equation(histId, histExpr));

		Expr countExpr = new UnaryExpr(UnaryOp.PRE, countId);
		countExpr = new BinaryExpr(countExpr, BinaryOp.PLUS, new IntExpr(BigInteger.ONE));
		countExpr = new BinaryExpr(new IntExpr(BigInteger.ZERO), BinaryOp.ARROW, countExpr);
		equations.add(new Equation(countId, countExpr));

		IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
		int consistDetph = prefs.getInt(PreferenceConstants.PREF_CONSIST_DEPTH);

		Expr propExpr = new BinaryExpr(countId, BinaryOp.EQUAL, new IntExpr(BigInteger.valueOf(consistDetph)));
		propExpr = new BinaryExpr(propExpr, BinaryOp.AND, histId);
		equations.add(new Equation(propId, new UnaryExpr(UnaryOp.NOT, propExpr)));
		properties.add(propId.id);

		NodeBuilder builder = new NodeBuilder("consistency");
		builder.addInputs(inputs);
		builder.addLocals(locals);
		builder.addEquations(equations);
		builder.addProperties(properties);
		builder.addAssertions(assertions);
		builder.addIvcs(ivcs);

		Node node = builder.build();

		return node;

	}

	public static String getRelativeLocation(String location) {
		int dotIndex = location.indexOf(".");
		if (dotIndex < 0) {
			return "";
		}
		return location.substring(dotIndex + 1);
	}

	protected static Equation getHist(IdExpr histId, Expr expr) {
		Expr preHist = new UnaryExpr(UnaryOp.PRE, histId);
		Expr preAndNow = new BinaryExpr(preHist, BinaryOp.AND, expr);
		return new Equation(histId, new BinaryExpr(expr, BinaryOp.ARROW, preAndNow));
	}

	protected static Node getLustreNode(XagreeNode agreeNode, String nodePrefix) {

		List<VarDecl> inputs = new ArrayList<>();
		List<VarDecl> locals = new ArrayList<>();
		List<Equation> equations = new ArrayList<>();
		List<Expr> assertions = new ArrayList<>();
		List<String> ivcs = new ArrayList<>();

		// add assumption history variable
		IdExpr assumHist = new IdExpr(assumeHistSufix);
		inputs.add(new XagreeVar(assumHist.id, NamedType.BOOL, null, agreeNode.compInst, null));

		int i = 0;
		for (XagreeStatement statement : agreeNode.assumptions) {
			String inputName = assumeSuffix + i++;
			inputs.add(new XagreeVar(inputName, NamedType.BOOL, statement.reference, agreeNode.compInst, null));
			IdExpr assumeId = new IdExpr(inputName);
			assertions.add(new BinaryExpr(assumeId, BinaryOp.EQUAL, statement.expr));
		}

		int j = 0;
		for (XagreeStatement statement : agreeNode.lemmas) {
			String inputName = lemmaSuffix + j++;
			inputs.add(new XagreeVar(inputName, NamedType.BOOL, statement.reference, agreeNode.compInst, null));
			IdExpr lemmaId = new IdExpr(inputName);
			assertions.add(new BinaryExpr(lemmaId, BinaryOp.EQUAL, statement.expr));
		}

		int k = 0;
		Expr guarConjExpr = new BoolExpr(true);
		for (XagreeStatement statement : agreeNode.guarantees) {
			String inputName = guarSuffix + k++;
			locals.add(new XagreeVar(inputName, NamedType.BOOL, statement.reference, agreeNode.compInst, null));
			IdExpr guarId = new IdExpr(inputName);
			equations.add(new Equation(guarId, statement.expr));
			ivcs.add(guarId.id);
			guarConjExpr = new BinaryExpr(guarId, BinaryOp.AND, guarConjExpr);
		}
		for (XagreeStatement statement : agreeNode.lemmas) {
			guarConjExpr = new BinaryExpr(statement.expr, BinaryOp.AND, guarConjExpr);
		}

		// assert that if the assumptions have held historically, then the
		// gurantees hold
		assertions.add(new BinaryExpr(assumHist, BinaryOp.IMPLIES, guarConjExpr));

		for (XagreeStatement statement : agreeNode.assertions) {
			assertions.add(statement.expr);
		}

		// create properties for the patterns
		int l = 0;
		for (XagreeStatement patternPropState : agreeNode.patternProps) {
			String patternVarName = patternPropSuffix + l++;
			inputs.add(new XagreeVar(patternVarName, NamedType.BOOL, patternPropState, agreeNode.compInst, null));
			assertions.add(new BinaryExpr(new IdExpr(patternVarName), BinaryOp.EQUAL, patternPropState.expr));
		}

		Expr assertExpr = new BoolExpr(true);
		for (Expr expr : assertions) {
			assertExpr = new BinaryExpr(expr, BinaryOp.AND, assertExpr);
		}

		String outputName = "__ASSERT";
		List<VarDecl> outputs = new ArrayList<>();
		outputs.add(new VarDecl(outputName, NamedType.BOOL));
		equations.add(new Equation(new IdExpr(outputName), assertExpr));

		// gather the remaining inputs
		for (XagreeVar var : agreeNode.inputs) {
			inputs.add(var);
		}
		for (XagreeVar var : agreeNode.outputs) {
			inputs.add(var);
		}
		for (XagreeVar var : agreeNode.locals) {
			locals.add(var);
		}

		for (XagreeEquation equation : agreeNode.localEquations) {
			equations.add(equation);
		}

		NodeBuilder builder = new NodeBuilder(nodePrefix + agreeNode.id);
		builder.addInputs(inputs);
		builder.addOutputs(outputs);
		builder.addLocals(locals);
		builder.addEquations(equations);
		builder.addIvcs(ivcs);

		return builder.build();
	}

	protected static XagreeNode flattenAgreeNode(XagreeProgram agreeProgram, XagreeNode agreeNode, String nodePrefix) {

		List<XagreeVar> inputs = new ArrayList<>();
		List<XagreeVar> outputs = new ArrayList<>();
		List<XagreeVar> locals = new ArrayList<>();
		List<XagreeStatement> patternProps = new ArrayList<>();
		List<XagreeEquation> equations = new ArrayList<>();
		List<XagreeStatement> assertions = new ArrayList<>();
		Set<XagreeVar> timeEvents = new HashSet<>(agreeNode.eventTimes);

		Expr someoneTicks = null;
		for (XagreeNode subAgreeNode : agreeNode.subNodes) {
			String prefix = subAgreeNode.id + XagreeASTBuilder.dotChar;
			Expr clockExpr = getClockExpr(agreeNode, subAgreeNode);

			if (someoneTicks == null) {
				someoneTicks = clockExpr;
			} else {
				someoneTicks = new BinaryExpr(someoneTicks, BinaryOp.OR, clockExpr);
			}

			XagreeNode flatNode = flattenAgreeNode(agreeProgram, subAgreeNode, nodePrefix + subAgreeNode.id + XagreeASTBuilder.dotChar);

			Node lustreNode = addSubNodeLustre(agreeProgram, agreeNode, nodePrefix, flatNode);

			addInputsAndOutputs(agreeNode, inputs, outputs, patternProps, flatNode, lustreNode, prefix);

			addTimeEvents(timeEvents, flatNode, prefix, assertions);

			addNodeCall(agreeNode, assertions, prefix, clockExpr,
					lustreNode);

			addHistoricalAssumptionConstraint(agreeNode, prefix, clockExpr, assertions, lustreNode);

		}

		if (agreeNode.timing == TimingModel.ASYNC) {
			if (someoneTicks == null) {
				throw new XagreeException("Somehow we generated a clock constraint without any clocks."
						+ " Perhaps none of your subcomponents have an agree annex?");
			}
			assertions.add(new XagreeStatement("someone ticks", someoneTicks, null));
		}

		addConnectionConstraints(agreeNode, assertions);

		// add any clock constraints
		assertions.addAll(agreeNode.assertions);
		assertions.add(new XagreeStatement("", agreeNode.clockConstraint, null));
		inputs.addAll(agreeNode.inputs);
		outputs.addAll(agreeNode.outputs);
		locals.addAll(agreeNode.locals);
		equations.addAll(agreeNode.localEquations);
		patternProps.addAll(agreeNode.patternProps);
		
		XagreeNodeBuilder builder = new XagreeNodeBuilder(agreeNode.id);
		
		builder.addInput(inputs);
		builder.addOutput(outputs);
		builder.addLocal(locals);
		builder.addLocalEquation(equations);
		builder.addSubNode(agreeNode.subNodes);
		builder.addAssertion(assertions);
		builder.addAssumption(agreeNode.assumptions);
		builder.addGuarantee(agreeNode.guarantees);
		builder.addLemma(agreeNode.lemmas);
		builder.addPatternProp(patternProps);
		builder.setClockConstraint(new BoolExpr(true));
		builder.setInitialConstraint(agreeNode.initialConstraint);
		builder.setClockVar(agreeNode.clockVar);
		builder.setReference(agreeNode.reference);
		builder.setTiming(null);
		builder.addEventTime(timeEvents);
		builder.setCompInst(agreeNode.compInst);

		return builder.build();
	}

	private static void addHistoricalAssumptionConstraint(XagreeNode agreeNode, String prefix, Expr clockExpr,
			List<XagreeStatement> assertions, Node lustreNode) {
		Expr assumConj = new BoolExpr(true);
		for (VarDecl lustreVar : lustreNode.inputs) {
			XagreeVar var = (XagreeVar) lustreVar;
			if (var.reference instanceof AssumeStatement || var.reference instanceof LemmaStatement) {
				Expr varId = new IdExpr(prefix + var.id);
				assumConj = new BinaryExpr(varId, BinaryOp.AND, assumConj);
			}
		}
		// assumConj = new BinaryExpr(clockExpr, BinaryOp.IMPLIES, assumConj);
		Expr histCall = new NodeCallExpr(historyNodeName, assumConj);
		Expr assertExpr = new BinaryExpr(new IdExpr(prefix + assumeSuffix + "__HIST"), BinaryOp.EQUAL, histCall);
		assertions.add(new XagreeStatement("", assertExpr, null));

	}

	private static void addTimeEvents(Set<XagreeVar> timeEvents, XagreeNode flatNode, String prefix,
			List<XagreeStatement> assertions) {
		for (XagreeVar event : flatNode.eventTimes) {
			timeEvents.add(new XagreeVar(prefix + event.id, event.type, event.reference, event.compInst, null));
		}
		// set the time variable to be equal
		IdExpr timeId = XagreePatternTranslator.timeExpr;
		assertions.add(
				new XagreeStatement("", new BinaryExpr(timeId, BinaryOp.EQUAL, new IdExpr(prefix + timeId.id)), null));
	}

	protected static void addConnectionConstraints(XagreeNode agreeNode, List<XagreeStatement> assertions) {
		for (XagreeConnection conn : agreeNode.connections) {
			if (conn instanceof XagreeAADLConnection) {
				XagreeAADLConnection aadlConn = (XagreeAADLConnection)conn;
				String destName = aadlConn.destinationNode == null ? "" : aadlConn.destinationNode.id + XagreeASTBuilder.dotChar;
				destName = destName + aadlConn.destVar.id;

				String sourName = aadlConn.sourceNode == null ? "" : aadlConn.sourceNode.id + XagreeASTBuilder.dotChar;
				sourName = sourName + aadlConn.sourVar.id;

				Expr aadlConnExpr;

				if (!aadlConn.delayed) {
					aadlConnExpr = new BinaryExpr(new IdExpr(sourName), BinaryOp.EQUAL, new IdExpr(destName));
				} else {
					// we need to get the correct type for the aadlConnection
					// we can assume that the source and destination types are
					// the same at this point
					Expr initExpr = XagreeUtils.getInitValueFromType(aadlConn.sourVar.type);
					Expr preSource = new UnaryExpr(UnaryOp.PRE, new IdExpr(sourName));
					Expr sourExpr = new BinaryExpr(initExpr, BinaryOp.ARROW, preSource);
					aadlConnExpr = new BinaryExpr(sourExpr, BinaryOp.EQUAL, new IdExpr(destName));
				}

				assertions.add(new XagreeStatement("", aadlConnExpr, aadlConn.reference));
			}else{
				XagreeOverriddenConnection agreeConn = (XagreeOverriddenConnection)conn;
				assertions.add(agreeConn.statement);
			}
		}
	}


	protected static void addNodeCall(XagreeNode agreeNode, List<XagreeStatement> assertions, String prefix,
			Expr clockExpr, Node lustreNode) {

		List<Expr> inputIds = new ArrayList<>();
		for (VarDecl var : lustreNode.inputs) {
			String suffix = "";
			// this is gross, we need to add the latched variables to the node
			// call
			// if this component implementation is using the latched semantics
			if (agreeNode.timing == TimingModel.LATCHED) {
				EObject ref = ((XagreeVar) var).reference;
				if (ref instanceof DataPortImpl && ((DataPortImpl) ref).isIn()
						|| (ref instanceof EventDataPortImpl && ((EventDataPortImpl) ref).isIn()
								|| (ref instanceof Arg && ref.eContainer() instanceof InputStatement))) {
					suffix = XagreeInlineLatchedConnections.LATCHED_SUFFIX;
				}
			}
			inputIds.add(new IdExpr(prefix + var.id + suffix));
		}
		if (agreeNode.timing == TimingModel.ASYNC || agreeNode.timing == TimingModel.LATCHED) {
			// the clock expr should be the last input to the node
			inputIds.set(inputIds.size()-1,clockExpr);
		}

		Expr condactExpr = new NodeCallExpr(lustreNode.id, inputIds);

		XagreeStatement condactCall = new XagreeStatement("", condactExpr, null);
		assertions.add(condactCall);
	}

	protected static void addInputsAndOutputs(XagreeNode parentNode, List<XagreeVar> inputs, List<XagreeVar> outputs, List<XagreeStatement> patternProps, XagreeNode subAgreeNode,
			Node lustreNode, String prefix) {
		int varCount = 0;
		for (XagreeVar var : subAgreeNode.inputs) {
			varCount++;
			XagreeVar input = new XagreeVar(prefix + var.id, var.type, var.reference, var.compInst, var.featInst);
			inputs.add(input);
		}

		for (XagreeVar var : subAgreeNode.outputs) {
			varCount++;
			XagreeVar output = new XagreeVar(prefix + var.id, var.type, var.reference, var.compInst, var.featInst);
			outputs.add(output);
		}

//		for (AgreeVar var : subAgreeNode.locals) {
//			varCount++;
//			AgreeVar local = new AgreeVar(prefix + var.id, var.type, var.reference, var.compInst, var.featInst);
//			outputs.add(local);
//		}

		int i = 0;
		for (XagreeStatement statement : subAgreeNode.assumptions) {
			varCount++;
			XagreeVar output = new XagreeVar(prefix + assumeSuffix + i++, NamedType.BOOL, statement.reference,
					subAgreeNode.compInst, null);
			outputs.add(output);
		}

		int k = 0;
		for (XagreeStatement statement : subAgreeNode.patternProps) {
			varCount++;
			XagreeVar output = new XagreeVar(prefix + patternPropSuffix + k++, NamedType.BOOL, statement,
					subAgreeNode.compInst, null);
			patternProps.add(new XagreeStatement(prefix.replace("__", "") + ": " + statement.string,
					new IdExpr(output.id), statement.reference));
			outputs.add(output);
		}

		// add the assumption history var
		varCount++;
		outputs.add(new XagreeVar(prefix + assumeSuffix + "__HIST", NamedType.BOOL, null, subAgreeNode.compInst, null));

		int j = 0;
		for (XagreeStatement statement : subAgreeNode.lemmas) {
			varCount++;
			XagreeVar output = new XagreeVar(prefix + lemmaSuffix + j++, NamedType.BOOL, statement.reference,
					subAgreeNode.compInst, null);
			outputs.add(output);
		}

		if (parentNode.timing == TimingModel.ASYNC || parentNode.timing == TimingModel.LATCHED) {
			inputs.add(subAgreeNode.clockVar);
			varCount++;
		}

		if (lustreNode.inputs.size() != varCount) {
			throw new XagreeException("Something went wrong during node flattening");
		}
	} 

	protected static Node addSubNodeLustre(XagreeProgram agreeProgram, XagreeNode agreeNode, String nodePrefix, XagreeNode flatNode) {

		Node lustreNode = getLustreNode(flatNode, nodePrefix);
		if (agreeNode.timing == TimingModel.ASYNC || agreeNode.timing == TimingModel.LATCHED) {
			lustreNode = LustreCondactNodeVisitor.translate(agreeProgram, flatNode, lustreNode);
		}
		addToNodes(lustreNode);
		return lustreNode;
	}

	protected static Expr getClockExpr(XagreeNode agreeNode, XagreeNode subNode) {

		IdExpr clockId = new IdExpr(subNode.clockVar.id);
		switch (agreeNode.timing) {
		case SYNC:
			return new BoolExpr(true);
		case ASYNC:
			return clockId;
		case LATCHED:
			Expr preClock = new UnaryExpr(UnaryOp.PRE, clockId);
			Expr notClock = new UnaryExpr(UnaryOp.NOT, clockId);
			Expr andExpr = new BinaryExpr(preClock, BinaryOp.AND, notClock);
			Expr clockExpr = new BinaryExpr(new BoolExpr(false), BinaryOp.ARROW, andExpr);
			return clockExpr;
		default:
			throw new XagreeException("unhandled timing type: '" + agreeNode.timing + "");
		}

	}

	protected static void addToNodes(Node node) {
		for (Node inList : nodes) {
			if (node.id.equals(inList.id)) {
				throw new XagreeException(
						"AGREE Lustre AST Builder attempted to add multiple nodes of name '" + node.id + "'");
			}
		}
		nodes.add(node);
	}

	protected static Node getHistNode() {
		NodeBuilder builder = new NodeBuilder(historyNodeName);
		builder.addInput(new VarDecl("input", NamedType.BOOL));
		builder.addOutput(new VarDecl("hist", NamedType.BOOL));

		IdExpr histId = new IdExpr("hist");
		IdExpr inputId = new IdExpr("input");
		Expr preHist = new UnaryExpr(UnaryOp.PRE, histId);
		Expr histExpr = new BinaryExpr(preHist, BinaryOp.AND, inputId);
		histExpr = new BinaryExpr(inputId, BinaryOp.ARROW, histExpr);

		builder.addEquation(new Equation(histId, histExpr));
		return builder.build();
	}

}

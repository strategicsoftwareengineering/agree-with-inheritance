package edu.clemson.xagree.analysis.translation;

import java.util.ArrayList;
import java.util.List;

import jkind.lustre.BinaryExpr;
import jkind.lustre.BinaryOp;
import jkind.lustre.BoolExpr;
import jkind.lustre.CondactExpr;
import jkind.lustre.Contract;
import jkind.lustre.Equation;
import jkind.lustre.Expr;
import jkind.lustre.IdExpr;
import jkind.lustre.NamedType;
import jkind.lustre.Node;
import jkind.lustre.NodeCallExpr;
import jkind.lustre.Program;
import jkind.lustre.TupleExpr;
import jkind.lustre.TypeDef;
import jkind.lustre.UnaryExpr;
import jkind.lustre.UnaryOp;
import jkind.lustre.VarDecl;
import jkind.lustre.builders.NodeBuilder;

import edu.clemson.xagree.xagree.AssumeStatement;
import edu.clemson.xagree.xagree.LemmaStatement;

import edu.clemson.xagree.analysis.XagreeException;
import edu.clemson.xagree.analysis.XagreeUtils;
import edu.clemson.xagree.analysis.ast.XagreeASTBuilder;
import edu.clemson.xagree.analysis.ast.XagreeNode;
import edu.clemson.xagree.analysis.ast.XagreeNodeBuilder;
import edu.clemson.xagree.analysis.ast.XagreeProgram;
import edu.clemson.xagree.analysis.ast.XagreeStatement;
import edu.clemson.xagree.analysis.ast.XagreeVar;
import edu.clemson.xagree.analysis.ast.XagreeNode.TimingModel;
import edu.clemson.xagree.analysis.lustre.visitors.IdRewriteVisitor;
import edu.clemson.xagree.analysis.lustre.visitors.IdRewriter;

public class LustreContractAstBuilder extends LustreAstBuilder {

    public static Program getContractLustreProgram(XagreeProgram agreeProgram) {

        nodes = new ArrayList<>();
        List<TypeDef> types = XagreeUtils.getLustreTypes(agreeProgram);

        XagreeNode flatNode = flattenAgreeNodeKindContract(agreeProgram.topNode, "_TOP__");
        List<Expr> assertions = new ArrayList<>();
        List<VarDecl> locals = new ArrayList<>();
        List<VarDecl> inputs = new ArrayList<>();
        List<VarDecl> outputs = new ArrayList<>();
        List<Equation> equations = new ArrayList<>();
        List<String> properties = new ArrayList<>();
        List<Expr> requires = new ArrayList<>();
        List<Expr> ensures = new ArrayList<>();

        for (XagreeStatement assertion : flatNode.assertions) {
            assertions.add(assertion.expr);
        }
        
        for (XagreeStatement assumption : flatNode.assumptions) {
            requires.add(assumption.expr);
        }

        for (XagreeStatement guarantee : flatNode.lemmas) {
            ensures.add(guarantee.expr);
        }

        for (XagreeStatement guarantee : flatNode.guarantees) {
            ensures.add(guarantee.expr);
        }

        for (XagreeVar var : flatNode.inputs) {
            inputs.add(var);
        }

        for (XagreeVar var : flatNode.outputs) {
            outputs.add(var);
        }

        for (XagreeVar var : flatNode.outputs) {
            if (var.reference instanceof AssumeStatement || var.reference instanceof LemmaStatement) {
                throw new XagreeException("This shouldn't happen");
            }
        }

        Contract contract = new Contract(requires, ensures);

        NodeBuilder builder = new NodeBuilder("_TOP");
        builder.addInputs(inputs);
        builder.addOutputs(outputs);
        builder.addLocals(locals);
        builder.addEquations(equations);
        builder.addProperties(properties);
        builder.addAssertions(assertions);
        builder.setContract(contract);
        
        Node main = builder.build();
        
        nodes.addAll(agreeProgram.globalLustreNodes);
        nodes.add(main);
        
        Program program = new Program(types, null, nodes, main.id);

        return program;

    }

    protected static XagreeNode flattenAgreeNodeKindContract(XagreeNode agreeNode, String nodePrefix) {

        List<XagreeVar> inputs = new ArrayList<>();
        List<XagreeVar> outputs = new ArrayList<>();
        List<XagreeVar> locals = new ArrayList<>();
        List<XagreeStatement> assertions = new ArrayList<>();

        Expr someoneTicks = null;
        for (XagreeNode subAgreeNode : agreeNode.subNodes) {
            String prefix = subAgreeNode.id + XagreeASTBuilder.dotChar;
            Expr clockExpr = getClockExpr(agreeNode, subAgreeNode);

            if (someoneTicks == null) {
                someoneTicks = clockExpr;
            } else {
                someoneTicks = new BinaryExpr(someoneTicks, BinaryOp.OR, clockExpr);
            }

            XagreeNode flatNode = flattenAgreeNodeKindContract(subAgreeNode,
                    nodePrefix + subAgreeNode.id + XagreeASTBuilder.dotChar);

            Node lustreNode = addSubNodeLustre(agreeNode, nodePrefix, flatNode);

            addInputsAndOutputs(inputs, outputs, flatNode, lustreNode, prefix);

            addCondactCall(agreeNode, nodePrefix, inputs, assertions, flatNode, prefix, clockExpr,
                    lustreNode);

            // addClockHolds(agreeNode, assertions, flatNode, clockExpr, prefix,
            // lustreNode);

            addInitConstraint(agreeNode, outputs, assertions, flatNode, prefix, clockExpr, lustreNode);

        }

        if (agreeNode.timing == TimingModel.ASYNC) {
            if (someoneTicks == null) {
                throw new XagreeException("Somehow we generated a clock constraint without any clocks");
            }
            assertions.add(new XagreeStatement("someone ticks", someoneTicks, null));
        }

        addConnectionConstraints(agreeNode, assertions);

        // add any clock constraints
        assertions.addAll(agreeNode.assertions);
        assertions.add(new XagreeStatement("", agreeNode.clockConstraint, null));
        inputs.addAll(agreeNode.inputs);
        outputs.addAll(agreeNode.outputs);
        locals.addAll(agreeNode.locals);

        XagreeNodeBuilder builder = new XagreeNodeBuilder(agreeNode.id);
		builder.addInput(inputs);
		builder.addOutput(outputs);
		builder.addLocal(locals);
		builder.addLocalEquation(agreeNode.localEquations);
		builder.addSubNode(agreeNode.subNodes);
		builder.addAssertion(assertions);
		builder.addAssumption(agreeNode.assumptions);
		builder.addGuarantee(agreeNode.guarantees);
		builder.addLemma(agreeNode.lemmas);
		builder.addPatternProp(agreeNode.patternProps);
		builder.setClockConstraint(new BoolExpr(true));
		builder.setInitialConstraint(agreeNode.initialConstraint);
		builder.setClockVar(agreeNode.clockVar);
		builder.setReference(agreeNode.reference);
		builder.setTiming(null);
		builder.addEventTime(agreeNode.eventTimes);
		builder.setCompInst(agreeNode.compInst);

		return builder.build();

    }

    
    protected static void addInitConstraint(XagreeNode agreeNode, List<XagreeVar> outputs,
			List<XagreeStatement> assertions, XagreeNode subAgreeNode, String prefix, Expr clockExpr, Node lustreNode) {
		if (agreeNode.timing != TimingModel.SYNC) {
			String tickedName = subAgreeNode.id + "___TICKED";
			outputs.add(new XagreeVar(tickedName, NamedType.BOOL, null, agreeNode.compInst, null));
			Expr tickedId = new IdExpr(tickedName);
			Expr preTicked = new UnaryExpr(UnaryOp.PRE, tickedId);
			Expr tickedOrPre = new BinaryExpr(clockExpr, BinaryOp.OR, preTicked);
			Expr initOrTicked = new BinaryExpr(clockExpr, BinaryOp.ARROW, tickedOrPre);
			Expr tickedEq = new BinaryExpr(tickedId, BinaryOp.EQUAL, initOrTicked);

			assertions.add(new XagreeStatement("", tickedEq, null));

			// we have two re-write the ids in the initial expressions
			IdRewriter rewriter = new IdRewriter() {

				@Override
				public IdExpr rewrite(IdExpr id) {
					// TODO Auto-generated method stub
					return new IdExpr(prefix + id.id);
				}
			};
			Expr newInit = subAgreeNode.initialConstraint.accept(new IdRewriteVisitor(rewriter));

			Expr initConstr = new BinaryExpr(new UnaryExpr(UnaryOp.NOT, tickedId), BinaryOp.IMPLIES, newInit);
			assertions.add(new XagreeStatement("", initConstr, null));

			// we also need to add hold expressions for the assumptions and
			// lemmas
			Expr assumeLemmaTrue = new BoolExpr(true);
			for (VarDecl lustreVar : lustreNode.inputs) {
				XagreeVar var = (XagreeVar) lustreVar;
				if (var.reference instanceof AssumeStatement || var.reference instanceof LemmaStatement) {
					assumeLemmaTrue = new BinaryExpr(assumeLemmaTrue, BinaryOp.AND, new IdExpr(prefix + var.id));
				}
			}
			assumeLemmaTrue = new BinaryExpr(new UnaryExpr(UnaryOp.NOT, tickedId), BinaryOp.IMPLIES, assumeLemmaTrue);
			assertions.add(new XagreeStatement("", assumeLemmaTrue, null));

		}
	}
    
    protected static Node addSubNodeLustre(XagreeNode agreeNode, String nodePrefix, XagreeNode flatNode) {

        Node lustreNode = getLustreNode(flatNode, nodePrefix);
        addToNodes(lustreNode);
        return lustreNode;
    }

    protected static Node getLustreNode(XagreeNode agreeNode, String nodePrefix) {

        List<VarDecl> inputs = new ArrayList<>();
        List<VarDecl> outputs = new ArrayList<>();
        List<VarDecl> locals = new ArrayList<>();
        List<Equation> equations = new ArrayList<>();
        List<Expr> assertions = new ArrayList<>();
        List<Expr> requires = new ArrayList<>();
        List<Expr> ensures = new ArrayList<>();

        for (XagreeStatement statement : agreeNode.assumptions) {
            requires.add(statement.expr);
        }

        for (XagreeStatement statement : agreeNode.lemmas) {
            ensures.add(statement.expr);
        }

        for (XagreeStatement statement : agreeNode.guarantees) {
            ensures.add(statement.expr);
        }

        for (XagreeStatement statement : agreeNode.assertions) {
            assertions.add(statement.expr);
            if(XagreeUtils.referenceIsInContract(statement.reference, agreeNode.compInst)){
                ensures.add(statement.expr);
            }
        }

        // gather the remaining inputs
        for (XagreeVar var : agreeNode.inputs) {
            inputs.add(var);
        }
        for (XagreeVar var : agreeNode.outputs) {
            outputs.add(var);
        }
        //Contract contract = new Contract(nodePrefix + agreeNode.id, requires, ensures);
        Contract contract = new Contract(requires, ensures);

        
        NodeBuilder builder = new NodeBuilder(nodePrefix + agreeNode.id);
        builder.addInputs(inputs);
        builder.addOutputs(outputs);
        builder.addLocals(locals);
        builder.addEquations(equations);
        builder.addAssertions(assertions);
        builder.setContract(contract);
        
        return builder.build();
    }

    protected static void addInputsAndOutputs(List<XagreeVar> inputs, List<XagreeVar> outputs,
            XagreeNode subAgreeNode, Node lustreNode, String prefix) {
        for (XagreeVar var : subAgreeNode.inputs) {
            XagreeVar input = new XagreeVar(prefix + var.id, var.type, var.reference, var.compInst, var.featInst);
            inputs.add(input);
        }

        for (XagreeVar var : subAgreeNode.outputs) {
            XagreeVar output = new XagreeVar(prefix + var.id, var.type, var.reference, var.compInst, var.featInst);
            outputs.add(output);
        }

        // right now we do not support local variables in our translation
//        for (AgreeVar var : subAgreeNode.locals) {
//            throw new AgreeException("What is an example of this?");
//            // varCount++;
//            // AgreeVar local = new AgreeVar(prefix+var.id, var.type,
//            // var.reference, var.compInst);
//            // outputs.add(local);
//        }
        if (!subAgreeNode.locals.isEmpty()) {
            throw new XagreeException("What is an example of this?");
        }

        inputs.add(subAgreeNode.clockVar);

    }

    protected static void addCondactCall(XagreeNode agreeNode, String nodePrefix, List<XagreeVar> inputs,
            List<XagreeStatement> assertions, XagreeNode subAgreeNode, String prefix, Expr clockExpr,
            Node lustreNode) {
        List<Expr> inputIds = new ArrayList<>();
        List<Expr> initOutputsVals = new ArrayList<>();
        List<IdExpr> nodeOutputIds = new ArrayList<>();
        for (VarDecl var : lustreNode.inputs) {
            inputIds.add(new IdExpr(prefix + var.id));
        }

        for (VarDecl var : lustreNode.outputs) {
            XagreeVar outputVar = (XagreeVar) var;
            String dummyName = prefix + var.id + "__DUMMY";
            XagreeVar dummyVar = new XagreeVar(dummyName, outputVar.type, outputVar.reference, outputVar.compInst,
                    outputVar.featInst);
            
            if (!inputs.contains(dummyVar)) {
                inputs.add(dummyVar);
            }

            initOutputsVals.add(new IdExpr(dummyName));
            nodeOutputIds.add(new IdExpr(prefix + var.id));
        }

        if (agreeNode.timing == TimingModel.LATCHED) {
            throw new XagreeException("check how we do this in the generic lustre translation now"
            		+ " to make sure that it is correct");
        }

        Expr condactExpr =
                new CondactExpr(clockExpr, new NodeCallExpr(lustreNode.id, inputIds), initOutputsVals);

        Expr condactOutput;
        if(nodeOutputIds.size() > 1){
            condactOutput = new TupleExpr(nodeOutputIds);
        }else{
            condactOutput = nodeOutputIds.get(0);
        }
        
        Expr condactCall = new BinaryExpr(condactOutput, BinaryOp.EQUAL, condactExpr);
        assertions.add(new XagreeStatement("", condactCall, null));
    }

}

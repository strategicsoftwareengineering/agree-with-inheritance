package edu.clemson.xagree.analysis.handlers;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import jkind.JKindException;
import jkind.api.JKindApi;
import jkind.api.JRealizabilityApi;
import jkind.api.KindApi;
import jkind.api.results.AnalysisResult;
import jkind.api.results.CompositeAnalysisResult;
import jkind.api.results.JKindResult;
import jkind.api.results.JRealizabilityResult;
import jkind.lustre.Node;
import jkind.lustre.Program;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.IHandlerActivation;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.util.Pair;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.Element;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instantiation.InstantiateModel;
import org.osate.aadl2.modelsupport.util.AadlUtil;
import org.osate.annexsupport.AnnexUtil;
import org.osate.ui.dialogs.Dialog;

import edu.clemson.xagree.xagree.AgreeContractSubclause;
import edu.clemson.xagree.xagree.XagreePackage;
import edu.clemson.xagree.xagree.AgreeSubclause;

import edu.clemson.xagree.XagreeStatementsRepository;
import edu.clemson.xagree.analysis.Activator;
import edu.clemson.xagree.analysis.XagreeException;
import edu.clemson.xagree.analysis.XagreeLayout;
import edu.clemson.xagree.analysis.XagreeLogger;
import edu.clemson.xagree.analysis.XagreeRenaming;
import edu.clemson.xagree.analysis.XagreeUtils;
import edu.clemson.xagree.analysis.ConsistencyResult;
import edu.clemson.xagree.analysis.ast.XagreeASTBuilder;
import edu.clemson.xagree.analysis.ast.XagreeNode;
import edu.clemson.xagree.analysis.ast.XagreeProgram;
import edu.clemson.xagree.analysis.ast.XagreeStatement;
import edu.clemson.xagree.analysis.extentions.XagreeAutomater;
import edu.clemson.xagree.analysis.extentions.XagreeAutomaterRegistry;
import edu.clemson.xagree.analysis.extentions.ExtensionRegistry;
import edu.clemson.xagree.analysis.lustre.visitors.RenamingVisitor;
import edu.clemson.xagree.analysis.preferences.PreferencesUtil;
import edu.clemson.xagree.analysis.translation.LustreAstBuilder;
import edu.clemson.xagree.analysis.translation.LustreContractAstBuilder;
import edu.clemson.xagree.analysis.views.XagreeResultsLinker;
import edu.clemson.xagree.analysis.views.XagreeResultsView;

public abstract class VerifyHandler extends AadlHandler {
    protected XagreeResultsLinker linker = new XagreeResultsLinker();
    protected Queue<JKindResult> queue = new ArrayDeque<>();
    protected AtomicReference<IProgressMonitor> monitorRef = new AtomicReference<>();

    private static final String RERUN_ID = "edu.clemson.xagree.xagree.analysis.commands.rerunAgree";
    private IHandlerActivation rerunActivation;
    private IHandlerActivation terminateActivation;
    private IHandlerActivation terminateAllActivation;
    private IHandlerService handlerService;
    private Map<String, String> rerunAdviceMap = new HashMap<>();
    private int adviceCount = 0;
    private boolean calledFromRerun = false;

    private enum AnalysisType {
        AssumeGuarantee, Consistency, Realizability
    };

    protected abstract boolean isRecursive();

    protected abstract boolean isMonolithic();

    protected abstract boolean isRealizability();

	protected SystemInstance getSysInstance(Element root) {
		ComponentImplementation ci = getComponentImplementation(root);
		try {
			return InstantiateModel.buildInstanceModelFile(ci);
		} catch (Exception e) {
			Dialog.showError("Model Instantiate", "Error while re-instantiating the model: " + e.getMessage());
			throw new XagreeException("Error Instantiating model");
		}
	}

	private ComponentImplementation getComponentImplementation(Element root) {
		Classifier classifier = getOutermostClassifier(root);
		if (isRealizability()) {
			if (!(classifier instanceof ComponentType)) {
				throw new XagreeException("Must select an AADL Component Type");
			}
			return XagreeUtils.compImplFromType((ComponentType) classifier);
		} else {
			if (classifier instanceof ComponentImplementation) {
				return (ComponentImplementation) classifier;
			}
			if (!(classifier instanceof ComponentType)) {
				throw new XagreeException("Must select an AADL Component Type or Implementation");
			}
			ComponentType ct = (ComponentType) classifier;
			List<ComponentImplementation> cis = getComponentImplementations(ct);
			if (cis.size() == 0) {
				throw new XagreeException("AADL Component Type has no implementation to verify");
			} else if (cis.size() == 1) {
				ComponentImplementation ci = cis.get(0);
				Shell shell = getWindow().getShell();
				String message = "User selected " + ct.getFullName() + ".\nRunning analysis on " + ci.getFullName()	+ " instead.";
				shell.getDisplay().asyncExec(
						() -> MessageDialog.openInformation(shell, "Analysis information", message));
				return ci;
			} else {
				throw new XagreeException(
						"AADL Component Type has multiple implementation to verify: please select just one");
			}
		}
	}

	private List<ComponentImplementation> getComponentImplementations(ComponentType ct) {
		List<ComponentImplementation> result = new ArrayList<>();
		AadlPackage pkg = AadlUtil.getContainingPackage(ct);
		for (ComponentImplementation ci : EcoreUtil2.getAllContentsOfType(pkg, ComponentImplementation.class)) {
			if (ci.getType().equals(ct)) {
				result.add(ci);
			}
		}
		return result;
	}

    @Override
    protected IStatus runJob(Element root, IProgressMonitor monitor) {
	    	//this flag is set by the rerun handler to prevent clearing the advice map
	    	if(!calledFromRerun){
	    		rerunAdviceMap.clear();
	    	}
	    	calledFromRerun = false;
    	
        disableRerunHandler();
        handlerService = (IHandlerService) getWindow().getService(IHandlerService.class);

        try {
            return verifyComponent(root, monitor, false, true);
        } catch (Throwable e) {
            String messages = getNestedMessages(e);
            return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, messages, e);
        }
    }
    
    public IStatus verifyComponent(Element root, IProgressMonitor monitor, boolean waitForCompletion, boolean showView) throws Exception {
    		SystemInstance si = getSysInstance(root);

        AnalysisResult result;
        CompositeAnalysisResult wrapper = new CompositeAnalysisResult("");

        // SystemType sysType = si.getSystemImplementation().getType();
        ComponentType sysType = XagreeUtils.getInstanceType(si);
        EList<AnnexSubclause> annexSubClauses = AnnexUtil.getAllAnnexSubclauses(sysType,
                XagreePackage.eINSTANCE.getAgreeContractSubclause());

        if (annexSubClauses.size() == 0) {
            throw new XagreeException(
                    "There is not an AGREE annex in the '" + sysType.getName() + "' system type.");
        }

        if (isRecursive()) {
            if(XagreeUtils.usingKind2()){
                throw new XagreeException("Kind2 only supports monolithic verification");
            }
            result = buildAnalysisResult(((NamedElement)root).getName(), si);
            wrapper.addChild(result);
            result = wrapper;
        } else if (isRealizability()) {
            XagreeProgram agreeProgram = new XagreeASTBuilder().getAgreeProgram(si, false);
            Program program = LustreAstBuilder.getRealizabilityLustreProgram(agreeProgram);
            wrapper.addChild(
                    createVerification("Realizability Check", si, program, agreeProgram, AnalysisType.Realizability));
            result = wrapper;
        } else {
            wrapVerificationResult(si, wrapper);
            result = wrapper;
        }
        if(showView) {
        		showView(result, linker);
        }
        return doAnalysis(root, monitor, waitForCompletion, showView);
    }

    private void wrapVerificationResult(ComponentInstance si, CompositeAnalysisResult wrapper) {
        XagreeProgram agreeProgram = null;
	    	try {
	    		agreeProgram = new XagreeASTBuilder().getAgreeProgram(si, isMonolithic());
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    		throw new XagreeException(e.getMessage());
	    	}
 
        // generate different lustre depending on which model checker we are
        // using
      
        Program program;
        if (XagreeUtils.usingKind2()) {
            if(!isMonolithic()){
                throw new XagreeException("Kind2 now only supports monolithic verification");
            }
            program = LustreContractAstBuilder.getContractLustreProgram(agreeProgram);
        } else {
            program = LustreAstBuilder.getAssumeGuaranteeLustreProgram(agreeProgram);
        }
        List<Pair<String, Program>> consistencies =
                LustreAstBuilder.getConsistencyChecks(agreeProgram);

        wrapper.addChild(
                createVerification("Contract Guarantees", si, program, agreeProgram, AnalysisType.AssumeGuarantee));
        for (Pair<String, Program> consistencyAnalysis : consistencies) {
            wrapper.addChild(createVerification(consistencyAnalysis.getFirst(), si,
                    consistencyAnalysis.getSecond(), agreeProgram, AnalysisType.Consistency));
        }
    }

    protected String getNestedMessages(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        while (e != null) {
            if (e.getMessage() != null && !e.getMessage().isEmpty()) {
                pw.println(e.getMessage());
            }
            e = e.getCause();
        }
        pw.close();
        return sw.toString();
    }

    private AnalysisResult buildAnalysisResult(String name, ComponentInstance ci) {
        CompositeAnalysisResult result = new CompositeAnalysisResult("Verification for " + name);

        if (containsAGREEAnnex(ci)) {
            wrapVerificationResult(ci, result);
            ComponentImplementation compImpl = XagreeUtils.getInstanceImplementation(ci);
            for (ComponentInstance subInst : ci.getComponentInstances()) {
                if (XagreeUtils.getInstanceImplementation(subInst) != null) {
                    AnalysisResult buildAnalysisResult = buildAnalysisResult(subInst.getName(), subInst);
                    if (buildAnalysisResult != null) {
                        result.addChild(buildAnalysisResult);
                    }
                }
            }

            if (result.getChildren().size() != 0) {
                linker.setComponent(result, compImpl);
                return result;
            }
        }
        return null;
    }

    private boolean containsAGREEAnnex(ComponentInstance ci) {
        return getAGREEAnnex(ci) != null;
    }
    
    private AgreeContractSubclause getAGREEAnnex(ComponentInstance ci) {
    		ComponentClassifier compClass = ci.getComponentClassifier();
        if (compClass instanceof ComponentImplementation) {
            compClass = ((ComponentImplementation) compClass).getType();
        }
        for (AnnexSubclause annex : AnnexUtil.getAllAnnexSubclauses(compClass,
                XagreePackage.eINSTANCE.getAgreeContractSubclause())) {
            if (annex instanceof AgreeContractSubclause) {
                return (AgreeContractSubclause) annex;
            }
        }
        return null;
    }

    private AnalysisResult createVerification(String resultName, ComponentInstance compInst, Program lustreProgram, XagreeProgram agreeProgram,
            AnalysisType analysisType) {
		XagreeAutomaterRegistry aAReg = (XagreeAutomaterRegistry) ExtensionRegistry
				.getRegistry(ExtensionRegistry.AGREE_AUTOMATER_EXT_ID);
		List<XagreeAutomater> automaters = aAReg.getAgreeAutomaters();
		XagreeRenaming renaming = new XagreeRenaming();
		XagreeLayout layout = new XagreeLayout();
        Node mainNode = null;
        for (Node node : lustreProgram.nodes) {
            if (node.id.equals(lustreProgram.main)) {
                mainNode = node;
                break;
            }
        }
        if (mainNode == null) {
            throw new XagreeException("Could not find main lustre node after translation");
        }

        List<String> properties = new ArrayList<>();
        
        RenamingVisitor.addRenamings(lustreProgram, renaming, layout);
        addProperties(renaming, properties, mainNode, agreeProgram);

		for (XagreeAutomater aa : automaters) {
			renaming = aa.rename(renaming);
			layout = aa.transformLayout(layout);
		}
        
        JKindResult result;
        switch (analysisType) {
        case Consistency:
            result = new ConsistencyResult(resultName, mainNode.properties, Collections.singletonList(true),
                    renaming);
            break;
        case Realizability:
            result = new JRealizabilityResult(resultName, renaming);
            break;
        case AssumeGuarantee:
            result = new JKindResult(resultName, properties, renaming);
            break;
        default:
            throw new XagreeException("Unhandled Analysis Type");
        }
        queue.add(result);

        ComponentImplementation compImpl = XagreeUtils.getInstanceImplementation(compInst);
        linker.setProgram(result, lustreProgram);
        linker.setComponent(result, compImpl);
        linker.setContract(result, getContract(compImpl));
        linker.setLayout(result, layout);
        linker.setReferenceMap(result, renaming.getRefMap());
        linker.setLog(result, XagreeLogger.getLog());
        linker.setRenaming(result, renaming);

        // System.out.println(program);
        return result;

    }

    private void addProperties(XagreeRenaming renaming, List<String> properties, Node mainNode,
            XagreeProgram agreeProgram) {

        // there is a special case in the AgreeRenaming which handles this
        // translation
        if (XagreeUtils.usingKind2()) {
            addKind2Properties(agreeProgram.topNode, properties, renaming, "_TOP", "");
        } else {
            properties.addAll(mainNode.properties);
        }
        
        Set<String> strs = new HashSet<>();
        for(String prop : properties){
	        	String renamed = renaming.rename(prop);
	        	if(!strs.add(renamed)){
	        		throw new XagreeException("Multiple properties named \""+renamed+"\"");
	        	}
        }

    }
    
    void addKind2Properties(XagreeNode agreeNode, List<String> properties, XagreeRenaming renaming, String prefix, String userPropPrefix){
        int i = 0;
        
        String propPrefix = (userPropPrefix.equals("")) ? "" : userPropPrefix + ": ";
        for(XagreeStatement statement : agreeNode.lemmas){
            renaming.addExplicitRename(prefix+"["+(++i)+"]", propPrefix + statement.string);
            properties.add(prefix.replaceAll("\\.", XagreeASTBuilder.dotChar)+"["+i+"]");
        }
        for(XagreeStatement statement : agreeNode.guarantees){
            renaming.addExplicitRename(prefix+"["+(++i)+"]", propPrefix + statement.string);
            properties.add(prefix.replaceAll("\\.", XagreeASTBuilder.dotChar)+"["+i+"]");
        }
        
        userPropPrefix = userPropPrefix.equals("") ? "" : userPropPrefix + ".";
        for(XagreeNode subNode : agreeNode.subNodes){
            addKind2Properties(subNode, properties, renaming, prefix+"."+subNode.id, userPropPrefix + subNode.id);
        }
    }

    private AgreeSubclause getContract(ComponentImplementation ci) {
    		ComponentType ct = ci.getOwnedRealization().getImplemented();
        for (AnnexSubclause annex : ct.getOwnedAnnexSubclauses()) {
            if (annex instanceof AgreeSubclause) {
                return (AgreeSubclause) annex;
            }
        }
        return null;
    }

    protected void showView(final AnalysisResult result, final XagreeResultsLinker linker) {
		/*
		 * This command is executed while the xtext document is locked. Thus it must be async
		 * otherwise we can get a deadlock condition if the UI tries to lock the document,
		 * e.g., to pull up hover information.
		 */
        getWindow().getShell().getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    XagreeResultsView page =
                            (XagreeResultsView) getWindow().getActivePage().showView(XagreeResultsView.ID);
                    page.setInput(result, linker);
                } catch (PartInitException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    protected void clearView() {
        getWindow().getShell().getDisplay().syncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    XagreeResultsView page =
                            (XagreeResultsView) getWindow().getActivePage().showView(XagreeResultsView.ID);
                    page.setInput(new CompositeAnalysisResult("empty"), null);
                } catch (PartInitException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    private List<JKindResult> results = new ArrayList<JKindResult>();
    public List<JKindResult> getResults() {
    		return results;
    }
    
    public AgreeContractSubclause getContractForImpl(ComponentImplementation impl) {
		try {
			ComponentInstance inst = InstantiateModel.buildInstanceModelFile(impl);
			return getAGREEAnnex(inst);
		} catch (Exception e) {
			return null;
		}
    }

    private IStatus doAnalysis(final Element root, final IProgressMonitor globalMonitor, boolean waitForCompletion, boolean showView) {

        Thread analysisThread = new Thread() {
            public void run() {
	            	if(showView) {
	            		activateTerminateHandlers(globalMonitor);
	            	}
                KindApi api = PreferencesUtil.getKindApi();
                KindApi consistApi = PreferencesUtil.getConsistencyApi();
                JRealizabilityApi realApi = PreferencesUtil.getJRealizabilityApi();

                while (!queue.isEmpty() && !globalMonitor.isCanceled()) {
                    JKindResult result = queue.peek();
                    NullProgressMonitor subMonitor = new NullProgressMonitor();
                    monitorRef.set(subMonitor);

                    Program program = linker.getProgram(result);

					if (api instanceof JKindApi) {
						String resultName = result.getName();
						String adviceFileName = rerunAdviceMap.get(resultName);
						if(adviceFileName == null){
							adviceFileName = "agree_advice"+adviceCount++;
							rerunAdviceMap.put(resultName, adviceFileName);
						}else{
							((JKindApi) api).setReadAdviceFile(adviceFileName);
						}
						((JKindApi) api).setWriteAdviceFile(adviceFileName);
					}
					
                    try {
                        if (result instanceof ConsistencyResult) {
                            consistApi.execute(program, result, subMonitor);
                        } else if (result instanceof JRealizabilityResult) {
                            realApi.execute(program, (JRealizabilityResult) result, subMonitor);
                        } else {
                            api.execute(program, result, subMonitor);
                        }
                    } catch (JKindException e) {
                        System.out.println("******** JKindException Text ********");
                        e.printStackTrace(System.out);
                        System.out.println("******** JKind Output ********");
                        System.out.println(result.getText());
                        System.out.println("******** Agree Lustre ********");
                        System.out.println(program);
                        break;
                    }
                    results.add(result);
                    queue.remove();
                }

                while (!queue.isEmpty()) {
                    queue.remove().cancel();
                }

                if(showView) {
                	deactivateTerminateHandlers();
                	enableRerunHandler(root);
                }
            }
        };
        analysisThread.start();
        if(waitForCompletion) {
        	while(analysisThread.isAlive()) {
        		try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// do nothing
				}
        	}
        }
        return Status.OK_STATUS;
    }

    private void activateTerminateHandlers(final IProgressMonitor globalMonitor) {
        getWindow().getShell().getDisplay().syncExec(new Runnable() {
            @Override
            public void run() {
                terminateActivation =
                        handlerService.activateHandler(TERMINATE_ID, new TerminateHandler(monitorRef));
                terminateAllActivation = handlerService.activateHandler(TERMINATE_ALL_ID,
                        new TerminateHandler(monitorRef, globalMonitor));
            }
        });
    }

    private void deactivateTerminateHandlers() {
        getWindow().getShell().getDisplay().syncExec(new Runnable() {
            @Override
            public void run() {
                handlerService.deactivateHandler(terminateActivation);
                handlerService.deactivateHandler(terminateAllActivation);
            }
        });
    }

    private void enableRerunHandler(final Element root) {
        getWindow().getShell().getDisplay().syncExec(new Runnable() {
            @Override
            public void run() {
                IHandlerService handlerService = getHandlerService();
                rerunActivation =
                        handlerService.activateHandler(RERUN_ID, new RerunHandler(root, VerifyHandler.this));
            }
        });
    }

    protected void disableRerunHandler() {
        if (rerunActivation != null) {
            getWindow().getShell().getDisplay().syncExec(new Runnable() {
                @Override
                public void run() {
                    IHandlerService handlerService = getHandlerService();
                    handlerService.deactivateHandler(rerunActivation);
                    rerunActivation = null;
                }
            });
        }
    }

    private IHandlerService getHandlerService() {
        return (IHandlerService) getWindow().getService(IHandlerService.class);
    }
    
    public void setCalledFromRerun(){
    		calledFromRerun = true;
    }
    
}
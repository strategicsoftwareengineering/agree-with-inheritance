package edu.clemson.xagree.analysis.extentions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IConfigurationElement;

public class XagreeAutomaterRegistry extends ExtensionRegistry {

    public XagreeAutomaterRegistry() {
        initialize(AGREE_AUTOMATER_EXT_ID);
    }

    public List<XagreeAutomater> getAgreeAutomaters() {

        List<XagreeAutomater> cexExtractors = new ArrayList<>();
        for (Entry<String, ExtensionProxy> entry : extensions.entrySet()) {
            if (entry.getValue() instanceof XagreeAutomater) {
                cexExtractors.add((XagreeAutomater) entry.getValue());
            }
        }
        return cexExtractors;
    }

    @Override
    protected ExtensionProxy createProxy(IConfigurationElement configElem) {
        return new XagreeAutomaterProxy(configElem);
    }

}

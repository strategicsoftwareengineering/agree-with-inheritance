package edu.clemson.xagree.analysis.extentions;

import edu.clemson.xagree.analysis.XagreeLayout;
import edu.clemson.xagree.analysis.XagreeRenaming;
import edu.clemson.xagree.analysis.ast.XagreeProgram;
import jkind.api.results.AnalysisResult;

public interface XagreeAutomater {
    public XagreeProgram transform(XagreeProgram program);
    public XagreeRenaming rename(XagreeRenaming renaming);
    public AnalysisResult transformResult(AnalysisResult res);
    public XagreeLayout transformLayout(XagreeLayout layout);
}

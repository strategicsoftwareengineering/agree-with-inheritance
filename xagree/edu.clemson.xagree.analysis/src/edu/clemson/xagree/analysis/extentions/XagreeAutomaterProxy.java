package edu.clemson.xagree.analysis.extentions;

import java.util.Map;

import jkind.api.results.AnalysisResult;
import jkind.results.Counterexample;
import jkind.results.Property;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.annexsupport.AnnexHighlighter;
import org.osate.annexsupport.AnnexPlugin;

import edu.clemson.xagree.analysis.XagreeLayout;
import edu.clemson.xagree.analysis.XagreeRenaming;
import edu.clemson.xagree.analysis.ast.XagreeProgram;

public class XagreeAutomaterProxy extends ExtensionProxy implements XagreeAutomater {

    private XagreeAutomater extractor;

    protected XagreeAutomaterProxy(IConfigurationElement configElem) {
        super(configElem);
        // TODO Auto-generated constructor stub
    }

    @Override
    public XagreeProgram transform(XagreeProgram program) {
        XagreeAutomater extractor = getAgreeAutomater();

        if (extractor != null) {
            return extractor.transform(program);
        }
        return null;
    }

    private XagreeAutomater getAgreeAutomater() {
        if (extractor != null) {
            return extractor;
        }
        try {
            extractor = (XagreeAutomater) configElem.createExecutableExtension(ATT_CLASS);
        } catch (Exception e) {
            System.err.println("error instantiating agree automater in plugin "
                    + configElem.getDeclaringExtension().getContributor().getName());
        }
        return extractor;
    }

	@Override
	public XagreeRenaming rename(XagreeRenaming renaming) {
		XagreeAutomater extractor = getAgreeAutomater();

        if (extractor != null) {
            return extractor.rename(renaming);
        }
        return null;
	}

	@Override
	public AnalysisResult transformResult(AnalysisResult res) {
		XagreeAutomater extractor = getAgreeAutomater();

        if (extractor != null) {
            return extractor.transformResult(res);
        }
        return null;
	}

	@Override
	public XagreeLayout transformLayout(XagreeLayout layout) {
		XagreeAutomater extractor = getAgreeAutomater();

        if (extractor != null) {
            return extractor.transformLayout(layout);
        }
        return null;
	}

}

package edu.clemson.xagree.analysis.ast.visitors;

import static java.util.stream.Collectors.joining;

import java.util.Iterator;
import java.util.List;

import edu.clemson.xagree.analysis.ast.XagreeAADLConnection;
import edu.clemson.xagree.analysis.ast.XagreeConnection;
import edu.clemson.xagree.analysis.ast.XagreeEquation;
import edu.clemson.xagree.analysis.ast.XagreeNode;
import edu.clemson.xagree.analysis.ast.XagreeOverriddenConnection;
import edu.clemson.xagree.analysis.ast.XagreeProgram;
import edu.clemson.xagree.analysis.ast.XagreeStatement;
import edu.clemson.xagree.analysis.ast.XagreeVar;
import jkind.lustre.Equation;
import jkind.lustre.Expr;
import jkind.lustre.Location;
import jkind.lustre.Node;
import jkind.lustre.Type;
import jkind.lustre.TypeDef;
import jkind.lustre.VarDecl;
import jkind.lustre.visitors.AstVisitor;
import jkind.lustre.visitors.PrettyPrintVisitor;

public class XagreeASTPrettyprinter extends PrettyPrintVisitor implements XagreeASTVisitor<Void> {
	private StringBuilder sb = new StringBuilder();
	private String main;

	@Override
	public String toString() {
		return sb.toString();
	}

	protected void write(Object o) {
		sb.append(o);
	}

	private static final String seperator = System.getProperty("line.separator");

	private void newline() {
		write(seperator);
	}
	
	@Override
	public Void visit(XagreeProgram program) {
		if (program.containsRealTimePatterns) {
			write("-- Program contains real-time patterns");
			newline();
			newline();
		}
		
		write("-- Program top-level node is: " + program.topNode.id);
		newline();
		newline();
		
		if (!program.globalTypes.isEmpty()) {
			for (Type type : program.globalTypes) {
				TypeDef typeDef = new TypeDef(Location.NULL, "dummy", type);
				typeDef.accept(this);
				newline();
			}
			newline();
		}

		if (!program.globalLustreNodes.isEmpty()) {
			Iterator<Node> iterator = program.globalLustreNodes.iterator();
			while (iterator.hasNext()) {
				iterator.next().accept(this);
				newline();
				if (iterator.hasNext()) {
					newline();
				}
			}
			newline();
		}			

		Iterator<XagreeNode> iterator = program.agreeNodes.iterator();
		while (iterator.hasNext()) {
			iterator.next().accept(this);
			newline();
			if (iterator.hasNext()) {
				newline();
			}
		}
		newline();
		
		return null;
	}

	@Override
	public Void visit(XagreeConnection e) {
		connection(e);
		return null;
	}

	@Override
	public Void visit(XagreeNode node) {
		write("agree node ");
		write(node.id);
		write("(");
		newline();
		agreeVarDecls(node.inputs);
		newline();
		write(") returns (");
		newline();
		agreeVarDecls(node.outputs);
		newline();
		write(");");
		newline();

		if (!node.locals.isEmpty()) {
			write("var");
			newline();
			agreeVarDecls(node.locals);
			write(";");
			newline();
		}
		
		if (node.id.equals(main)) {
			write("  --%MAIN;");
			newline();
		}

		write("let");
		newline();
		
		if (!node.connections.isEmpty()) {
			for (XagreeConnection connection: node.connections) {
				connection(connection);
			}
			newline();
		}
		
		write("  children {");
		for (XagreeNode subNode: node.subNodes) {
			write(subNode.id);
			write(" ");
		}
		write("  }");
		newline();
		newline();

		statementList("assertions", node.assertions);
		statementList("assumptions", node.assumptions);
		statementList("guarantees", node.guarantees);
		statementList("lemmas", node.lemmas);
		statementList("pattern props", node.patternProps);
		
		for (XagreeEquation equation : node.localEquations) {
			write("  ");
			visit(equation);
			newline();
			newline();
		}

		if (node.clockConstraint != null) {
			write("  clock constraint: ");
			node.clockConstraint.accept(this);
			newline();
		}
		
		if (node.initialConstraint != null) {
			write("  initial constraint: ");
			node.initialConstraint.accept(this);
			newline();
		}
		
		if (node.clockVar != null) {
			write("  clock variable: ");
			visit(node.clockVar);
			newline();
		}
		
		write("  timing model: " + node.timing);
		newline();
		
		write("  -- TBD: event models to go along with timing model");
		newline();
		newline();
		
		write("tel;");
		return null;
	}

	private void statementList(String type, List<XagreeStatement> stmts) {
		if (!stmts.isEmpty()) {
			write("  " + type + " {");
			newline();
			Iterator<XagreeStatement> iterator = stmts.iterator();
			while (iterator.hasNext()) {
				iterator.next().accept(this);
				newline();
				if (iterator.hasNext()) {
					newline();
				}
			}
			write("  }");
			newline();
			newline();
		}
	}
	// Note: this method (and the fact that I'm using instanceof)
	// means that the AGREE Ast visitor is broken
	private void connection(XagreeConnection conn) {
		if (conn instanceof XagreeAADLConnection) {
			XagreeAADLConnection aadl = 
					(XagreeAADLConnection)conn;
			write("  ");
			write(aadl.type);
			write(aadl.delayed ? " delayed" : "");
			write(aadl.latched ? " latched" : "");
			write(" connection {");
			newline();
			write("    ");
			write(aadl.sourceNode == null ? "NULL NODE" : aadl.sourceNode.id);
			write("." + aadl.sourVar.id);
			write(" ->");
			write(aadl.destinationNode == null ? " NULL NODE" : " " + aadl.destinationNode.id);
			write("." + aadl.destVar.id);
			newline();
			write("  }");
			newline();
		} else if (conn instanceof XagreeOverriddenConnection) {
			XagreeOverriddenConnection aadl = 
					(XagreeOverriddenConnection)conn;
			write("  connection {");
			newline();
			write("    "); 
			aadl.statement.accept(this);
			newline();
			write("    aadl: " + aadl.aadlConn.toString());
			newline();
			write("  }");
			newline();
		}
	}
	
	private void agreeVarDecls(List<XagreeVar> varDecls) {
		Iterator<XagreeVar> iterator = varDecls.iterator();
		while (iterator.hasNext()) {
			write("  ");
			visit(iterator.next());
			if (iterator.hasNext()) {
				write(";");
				newline();
			}
		}
	}
	
	@Override
	public Void visit(XagreeStatement e) {
		write("    ");
		if (e.string != null && !e.string.equals("")) {
			write("\"" + e.string + "\" : ");
		}
		e.expr.accept(this);
		
		return null;
	}

	@Override
	public Void visit(XagreeVar e) {
		super.visit((VarDecl)e);
		return null;
	}

	@Override
	public Void visit(XagreeEquation agreeEquation) {
		super.visit((Equation)agreeEquation);
		return null;
	}

}

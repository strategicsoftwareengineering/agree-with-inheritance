package edu.clemson.xagree.analysis.ast;

import org.eclipse.emf.ecore.EObject;

import edu.clemson.xagree.analysis.ast.visitors.XagreeASTVisitor;

public class XagreeAADLConnection implements XagreeConnection{
	
	public enum ConnectionType {
        EVENT, DATA
    }
	public final XagreeNode sourceNode;
	public final XagreeNode destinationNode;
	public final XagreeVar sourVar;
	public final XagreeVar destVar;
	public final ConnectionType type;
	public final EObject reference;
	public final boolean latched;
	public final boolean delayed;

	public XagreeAADLConnection(XagreeNode sourceNode, XagreeNode destinationNode, XagreeVar sourceVarName,
			XagreeVar destinationVarName, ConnectionType type, boolean latched, boolean delayed, EObject reference) {
		this.sourceNode = sourceNode;
		this.destinationNode = destinationNode;
		this.sourVar = sourceVarName;
		this.destVar = destinationVarName;
		this.type = type;
        this.latched = latched;
        this.delayed = delayed;
        this.reference = reference;
    }

	@Override
	public <T> T accept(XagreeASTVisitor<T> visitor) {
		// TODO Auto-generated method stub
		return null;
	}

}
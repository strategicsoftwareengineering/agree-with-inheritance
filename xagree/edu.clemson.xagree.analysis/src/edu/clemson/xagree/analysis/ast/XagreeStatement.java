package edu.clemson.xagree.analysis.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;

import jkind.lustre.Expr;
import edu.clemson.xagree.analysis.XagreeException;
import edu.clemson.xagree.analysis.ast.visitors.XagreeASTVisitor;
import edu.clemson.xagree.analysis.realtime.XagreePattern;

public class XagreeStatement implements XagreeASTElement, EObject{
	public final String string;
	public final Expr expr;
	public final EObject reference;

	public XagreeStatement(String string, Expr expr, EObject reference) {
		this.string = string;
		this.expr = expr;
		this.reference = reference;
		if (expr == null && !(this instanceof XagreePattern)) {
			throw new XagreeException("AgreeStatement created with null expression");
		}
	}

	@Override
	public <T> T accept(XagreeASTVisitor<T> visitor) {
		return visitor.visit(this);
	}

    @Override
    public EList<Adapter> eAdapters() {
        return reference.eAdapters();
    }

    @Override
    public boolean eDeliver() {
        return reference.eDeliver();
    }

    @Override
    public void eSetDeliver(boolean deliver) {
       reference.eSetDeliver(deliver);
    }

    @Override
    public void eNotify(Notification notification) {
        reference.eNotify(notification);
    }

    @Override
    public EClass eClass() {
        return reference.eClass();
    }

    @Override
    public Resource eResource() {
        return reference.eResource();
    }

    @Override
    public EObject eContainer() {
		if (reference != null) {
			return reference.eContainer();
		} else {
			return null;
		}
    }

    @Override
    public EStructuralFeature eContainingFeature() {
        return reference.eContainingFeature();
    }

    @Override
    public EReference eContainmentFeature() {
       return reference.eContainmentFeature();
    }

    @Override
    public EList<EObject> eContents() {
        return reference.eContents();
    }

    @Override
    public TreeIterator<EObject> eAllContents() {
        return reference.eAllContents();
    }

    @Override
    public boolean eIsProxy() {
        return reference.eIsProxy();
    }

    @Override
    public EList<EObject> eCrossReferences() {
        return reference.eCrossReferences();
    }

    @Override
    public Object eGet(EStructuralFeature feature) {
        return reference.eGet(feature);
    }

    @Override
    public Object eGet(EStructuralFeature feature, boolean resolve) {
        return reference.eGet(feature, resolve);
    }

    @Override
    public void eSet(EStructuralFeature feature, Object newValue) {
        reference.eSet(feature, newValue);
    }

    @Override
    public boolean eIsSet(EStructuralFeature feature) {
        return reference.eIsSet(feature);
    }

    @Override
    public void eUnset(EStructuralFeature feature) {
        reference.eUnset(feature);
    }

    @Override
    public Object eInvoke(EOperation operation, EList<?> arguments) throws InvocationTargetException {
        return reference.eInvoke(operation, arguments);
    }
    
    @Override
    public String toString(){
        return string;
    }

}

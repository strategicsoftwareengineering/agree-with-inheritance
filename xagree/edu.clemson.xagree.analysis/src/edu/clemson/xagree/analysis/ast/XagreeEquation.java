package edu.clemson.xagree.analysis.ast;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import edu.clemson.xagree.analysis.ast.visitors.XagreeASTVisitor;
import jkind.lustre.Equation;
import jkind.lustre.Expr;
import jkind.lustre.IdExpr;
import jkind.lustre.Location;

public class XagreeEquation extends Equation implements XagreeASTElement{ 
    
    public final EObject reference;

    public XagreeEquation(List<IdExpr> lhs, Expr expr, EObject reference) {
        super(Location.NULL, lhs, expr);
        this.reference = reference;
    }

    public XagreeEquation(IdExpr id, Expr expr, EObject reference) {
        super(Location.NULL, id, expr);
        this.reference = reference;
    }
    
    public XagreeEquation(Equation eq, EObject reference){
        this(eq.lhs, eq.expr, reference);
    }

    @Override
    public <T> T accept(XagreeASTVisitor<T> visitor) {
        return visitor.visit(this);
    }

}

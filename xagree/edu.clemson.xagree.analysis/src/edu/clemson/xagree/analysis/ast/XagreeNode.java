package edu.clemson.xagree.analysis.ast;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.instance.ComponentInstance;

import edu.clemson.xagree.analysis.ast.visitors.XagreeASTVisitor;
import jkind.lustre.Expr;

public class XagreeNode implements XagreeASTElement{
    public enum TimingModel {
        SYNC, ASYNC, LATCHED
    };

    public final String id;
    public final List<XagreeVar> inputs;
    public final List<XagreeVar> outputs;
    public final List<XagreeVar> locals;
    public final List<XagreeConnection> connections;
    public final List<XagreeNode> subNodes;
    public final List<XagreeStatement> assertions;
    public final List<XagreeStatement> assumptions;
    public final List<XagreeStatement> guarantees;
    public final List<XagreeStatement> lemmas;
    public final List<XagreeStatement> patternProps;
    public final List<XagreeEquation> localEquations;
    public final Expr clockConstraint;
    public final Expr initialConstraint;
    public final XagreeVar clockVar;
    public final EObject reference;
    public final TimingModel timing;
    public final ComponentInstance compInst;
    public final Map<String, XagreeVar> timeOfMap;
    public final Map<String, XagreeVar> timeRiseMap;
    public final Map<String, XagreeVar> timeFallMap;
    
    public final Set<XagreeVar> eventTimes;

    public XagreeNode(String id, List<XagreeVar> inputs, List<XagreeVar> outputs, List<XagreeVar> locals,
            List<XagreeEquation> localEquations, List<XagreeConnection> connections, List<XagreeNode> subNodes, List<XagreeStatement> assertions,
            List<XagreeStatement> assumptions, List<XagreeStatement> guarantees, List<XagreeStatement> lemmas, List<XagreeStatement> patternProps,
            Expr clockConstraint, Expr initialConstraint, XagreeVar clockVar, EObject reference,
            TimingModel timing, Set<XagreeVar> eventTimes, Map<String, XagreeVar> timeOfMap, Map<String, XagreeVar> timeRiseMap, Map<String, XagreeVar> timeFallMap, ComponentInstance compinst) {
        this.id = id;
        this.inputs = jkind.util.Util.safeList(inputs);
        this.outputs = jkind.util.Util.safeList(outputs);
        this.locals = jkind.util.Util.safeList(locals);
        this.patternProps = jkind.util.Util.safeList(patternProps);
        this.localEquations = jkind.util.Util.safeList(localEquations);
        this.connections = jkind.util.Util.safeList(connections);
        this.subNodes = jkind.util.Util.safeList(subNodes);
        this.assertions = jkind.util.Util.safeList(assertions);
        this.assumptions = jkind.util.Util.safeList(assumptions);
        this.guarantees = jkind.util.Util.safeList(guarantees);
        this.lemmas = jkind.util.Util.safeList(lemmas);
        this.clockConstraint = clockConstraint;
        this.initialConstraint = initialConstraint;
        this.clockVar = clockVar;
        this.reference = reference;
        this.timing = timing;
        this.compInst = compinst;
        if (eventTimes == null) {
            this.eventTimes = Collections.emptySet();
        } else {
            this.eventTimes = Collections.unmodifiableSet(eventTimes);
        }
        this.timeOfMap = timeOfMap;
        this.timeRiseMap = timeRiseMap;
        this.timeFallMap = timeFallMap;
    }

    @Override
    public <T> T accept(XagreeASTVisitor<T> visitor) {
        return visitor.visit(this);
    }
    
    @Override
    public String toString(){
    	return id;
    }

}

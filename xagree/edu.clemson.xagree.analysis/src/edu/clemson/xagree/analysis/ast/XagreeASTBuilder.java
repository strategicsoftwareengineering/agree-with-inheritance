package edu.clemson.xagree.analysis.ast;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.AnnexSubclause;
import org.osate.aadl2.BooleanLiteral;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ConnectedElement;
import org.osate.aadl2.Connection;
import org.osate.aadl2.ConnectionEnd;
import org.osate.aadl2.Context;
import org.osate.aadl2.DataPort;
import org.osate.aadl2.DataSubcomponent;
import org.osate.aadl2.DataSubcomponentType;
import org.osate.aadl2.EnumerationLiteral;
import org.osate.aadl2.EventDataPort;
import org.osate.aadl2.EventPort;
import org.osate.aadl2.Feature;
import org.osate.aadl2.FeatureGroup;
import org.osate.aadl2.FeatureGroupType;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.NamedValue;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.RealLiteral;
import org.osate.aadl2.StringLiteral;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;
import org.osate.aadl2.properties.PropertyDoesNotApplyToHolderException;
import org.osate.annexsupport.AnnexUtil;
import org.osate.xtext.aadl2.properties.util.EMFIndexRetrieval;
import org.osate.xtext.aadl2.properties.util.PropertyUtils;

import edu.clemson.xagree.xagree.AADLEnumerator;
import edu.clemson.xagree.xagree.AgreeContract;
import edu.clemson.xagree.xagree.AgreeContractSubclause;
import edu.clemson.xagree.xagree.XagreePackage;
import edu.clemson.xagree.xagree.Arg;
import edu.clemson.xagree.xagree.AssertStatement;
import edu.clemson.xagree.xagree.AssignStatement;
import edu.clemson.xagree.xagree.AssumeStatement;
import edu.clemson.xagree.xagree.AsynchStatement;
import edu.clemson.xagree.xagree.BoolLitExpr;
import edu.clemson.xagree.xagree.CalenStatement;
import edu.clemson.xagree.xagree.ConnectionStatement;
import edu.clemson.xagree.xagree.ConstStatement;
import edu.clemson.xagree.xagree.EqStatement;
import edu.clemson.xagree.xagree.EventExpr;
import edu.clemson.xagree.xagree.FloorCast;
import edu.clemson.xagree.xagree.FnCallExpr;
import edu.clemson.xagree.xagree.FnDefExpr;
import edu.clemson.xagree.xagree.GetPropertyExpr;
import edu.clemson.xagree.xagree.GuaranteeStatement;
import edu.clemson.xagree.xagree.InitialStatement;
import edu.clemson.xagree.xagree.InputStatement;
import edu.clemson.xagree.xagree.IntLitExpr;
import edu.clemson.xagree.xagree.LatchedExpr;
import edu.clemson.xagree.xagree.LatchedStatement;
import edu.clemson.xagree.xagree.LemmaStatement;
import edu.clemson.xagree.xagree.LinearizationDefExpr;
import edu.clemson.xagree.xagree.MNSynchStatement;
import edu.clemson.xagree.xagree.NestedDotID;
import edu.clemson.xagree.xagree.NodeBodyExpr;
import edu.clemson.xagree.xagree.NodeDefExpr;
import edu.clemson.xagree.xagree.NodeEq;
import edu.clemson.xagree.xagree.NodeLemma;
import edu.clemson.xagree.xagree.NodeStmt;
import edu.clemson.xagree.xagree.PatternStatement;
import edu.clemson.xagree.xagree.PreExpr;
import edu.clemson.xagree.xagree.PrevExpr;
import edu.clemson.xagree.xagree.PrimType;
import edu.clemson.xagree.xagree.PropertyStatement;
import edu.clemson.xagree.xagree.RealCast;
import edu.clemson.xagree.xagree.RealLitExpr;
import edu.clemson.xagree.xagree.RecordDefExpr;
import edu.clemson.xagree.xagree.RecordExpr;
import edu.clemson.xagree.xagree.RecordUpdateExpr;
import edu.clemson.xagree.xagree.SpecStatement;
import edu.clemson.xagree.xagree.SynchStatement;
import edu.clemson.xagree.xagree.ThisExpr;
import edu.clemson.xagree.xagree.TimeExpr;
import edu.clemson.xagree.xagree.TimeFallExpr;
import edu.clemson.xagree.xagree.TimeOfExpr;
import edu.clemson.xagree.xagree.TimeRiseExpr;
import edu.clemson.xagree.xagree.util.XagreeSwitch;

import edu.clemson.xagree.XagreeStatementsRepository;
import edu.clemson.xagree.analysis.XagreeCalendarUtils;
import edu.clemson.xagree.analysis.XagreeException;
import edu.clemson.xagree.analysis.XagreeLogger;
import edu.clemson.xagree.analysis.XagreeTypeUtils;
import edu.clemson.xagree.analysis.XagreeUtils;
import edu.clemson.xagree.analysis.MNSynchronyElement;
import edu.clemson.xagree.analysis.ast.XagreeAADLConnection.ConnectionType;
import edu.clemson.xagree.analysis.ast.XagreeNode.TimingModel;
import edu.clemson.xagree.analysis.ast.visitors.XagreeInlineLatchedConnections;
import edu.clemson.xagree.analysis.ast.visitors.XagreeMakeClockedLustreNodes;
import edu.clemson.xagree.analysis.extentions.XagreeAutomater;
import edu.clemson.xagree.analysis.extentions.XagreeAutomaterRegistry;
import edu.clemson.xagree.analysis.extentions.ExtensionRegistry;
import edu.clemson.xagree.analysis.linearization.LinearizationRewriter;
import edu.clemson.xagree.analysis.lustre.visitors.IdGatherer;
import edu.clemson.xagree.analysis.realtime.XagreeCauseEffectPattern;
import edu.clemson.xagree.analysis.realtime.XagreePatternBuilder;
import edu.clemson.xagree.analysis.realtime.XagreePatternTranslator;
import edu.clemson.xagree.analysis.realtime.XagreePeriodicPattern;
import edu.clemson.xagree.analysis.realtime.XagreeSporadicPattern;
import jkind.lustre.BinaryExpr;
import jkind.lustre.BinaryOp;
import jkind.lustre.BoolExpr;
import jkind.lustre.CastExpr;
import jkind.lustre.Equation;
import jkind.lustre.Expr;
import jkind.lustre.IdExpr;
import jkind.lustre.IfThenElseExpr;
import jkind.lustre.IntExpr;
import jkind.lustre.NamedType;
import jkind.lustre.Node;
import jkind.lustre.NodeCallExpr;
import jkind.lustre.RealExpr;
import jkind.lustre.RecordAccessExpr;
import jkind.lustre.TupleExpr;
import jkind.lustre.Type;
import jkind.lustre.UnaryExpr;
import jkind.lustre.UnaryOp;
import jkind.lustre.VarDecl;
import jkind.lustre.builders.NodeBuilder;

public class XagreeASTBuilder extends XagreeSwitch<Expr> {

	public static final String clockIDSuffix = "___CLOCK_";
	public static final String eventSuffix = "___EVENT_";
	public static final String dotChar = "__";

	public static List<Node> globalNodes;
	private static Set<Type> globalTypes;
	private static Map<NamedElement, String> typeMap;
	private static Map<String, XagreeVar> timeOfVarMap;
	private static Map<String, XagreeVar> timeRiseVarMap;
	private static Map<String, XagreeVar> timeFallVarMap;

	// TODO: a number of the maps in this class are static but appear to more
	// appropriately and more safely be instance variables.
	private static Map<String, String> renamings;
	private static Map<String, EObject> refMap;

	private ComponentInstance curInst; // used for Get_Property Expressions
	private boolean isMonolithic = false;
	private LinearizationRewriter linearizationRewriter = new LinearizationRewriter();

	public XagreeProgram getAgreeProgram(ComponentInstance compInst, boolean isMonolithic) throws Exception {
		boolean containsRTPatterns = false;

		this.isMonolithic = isMonolithic;
		globalNodes = new ArrayList<>();
		globalTypes = new HashSet<>();
		typeMap = new HashMap<>();
		renamings = new HashMap<>();
		refMap = new HashMap<>();

		XagreeNode topNode = getAgreeNode(compInst, true);
		List<XagreeNode> agreeNodes = gatherNodes(topNode);

		// have to convert the types. The reason we use Record types in the
		// first place rather than the more general types is so we can check set
		// containment easily
		XagreeProgram program = new XagreeProgram(agreeNodes, new ArrayList<>(globalNodes), new ArrayList<>(globalTypes),
				topNode);

		// if there are any patterns in the AgreeProgram we need to inline them
		program = XagreePatternTranslator.translate(program);
		containsRTPatterns = program.containsRealTimePatterns;

		program = XagreeInlineLatchedConnections.translate(program);
		program = XagreeMakeClockedLustreNodes.translate(program);

		// go through the extension registries and transform the program
		XagreeAutomaterRegistry aAReg = (XagreeAutomaterRegistry) ExtensionRegistry
				.getRegistry(ExtensionRegistry.AGREE_AUTOMATER_EXT_ID);
		List<XagreeAutomater> automaters = aAReg.getAgreeAutomaters();

		for (XagreeAutomater aa : automaters) {
			program = aa.transform(program);
		}

		program.containsRealTimePatterns(containsRTPatterns);

		return program;
	}

	private List<XagreeNode> gatherNodes(XagreeNode node) {
		List<XagreeNode> nodes = new ArrayList<>();

		for (XagreeNode subNode : node.subNodes) {
			nodes.addAll(gatherNodes(subNode));
			nodes.add(subNode);
		}
		return nodes;
	}
	
	private XagreeNode buildNode(String id, List<XagreeVar> inputs, List<XagreeVar> outputs, List<XagreeVar> locals,
			List<XagreeEquation> localEquations, List<XagreeConnection> connections, List<XagreeNode> subNodes,
			List<XagreeStatement> assertions, List<XagreeStatement> assumptions, List<XagreeStatement> guarantees,
			List<XagreeStatement> lemmas, List<XagreeStatement> patternProps, Expr clockConstraint,
			Expr initialConstraint, XagreeVar clockVar, EObject reference, TimingModel timing,
			ComponentInstance compInst, Map<String, XagreeVar> timeOfVarMap2, Map<String, XagreeVar> timeRiseVarMap2,
			Map<String, XagreeVar> timeFallVarMap2) {
		XagreeNodeBuilder builder = new XagreeNodeBuilder(id);
		builder.addInput(inputs);
		builder.addOutput(outputs);
		builder.addLocal(locals);
		builder.addLocalEquation(localEquations);
		builder.addConnection(connections);
		builder.addSubNode(subNodes);
		builder.addAssertion(assertions);
		builder.addAssumption(assumptions);
		builder.addGuarantee(guarantees);
		builder.addLemma(lemmas);
		builder.addPatternProp(patternProps);
		builder.setClockConstraint(clockConstraint);
		builder.setInitialConstraint(initialConstraint);
		builder.setClockVar(clockVar);
		builder.setReference(reference);
		builder.setTiming(timing);
		builder.setCompInst(compInst);
		builder.addTimeOf(timeOfVarMap);
		builder.addTimeRise(timeRiseVarMap);
		builder.addTimeFall(timeFallVarMap);

		XagreeNode result = builder.build();
		return result;
	}

	private XagreeNode getAgreeNode(ComponentInstance compInst, boolean isTop) throws Exception {
		List<XagreeVar> inputs = new ArrayList<>();
		List<XagreeVar> outputs = new ArrayList<>();
		List<XagreeVar> locals = new ArrayList<>();
		List<XagreeAADLConnection> aadlConnections = new ArrayList<>();
		List<XagreeOverriddenConnection> userDefinedConections = new ArrayList<>();
		List<XagreeConnection> connections = new ArrayList<>();
		List<XagreeNode> subNodes = new ArrayList<>();
		List<XagreeStatement> assertions = new ArrayList<>();
		List<XagreeStatement> assumptions = new ArrayList<>();
		List<XagreeStatement> guarantees = new ArrayList<>();
		List<XagreeStatement> lemmas = new ArrayList<>();
		List<XagreeEquation> localEquations = new ArrayList<>();
		List<XagreeStatement> patternProps = Collections.emptyList();
		timeOfVarMap = new HashMap<>();
		timeRiseVarMap = new HashMap<>();
		timeFallVarMap = new HashMap<>();

		Expr clockConstraint = new BoolExpr(true);
		Expr initialConstraint = new BoolExpr(true);
		String id = compInst.getName();
		XagreeVar clockVar = new XagreeVar(id + clockIDSuffix, NamedType.BOOL, compInst.getSubcomponent(), compInst,
				null);
		EObject reference = isTop ? compInst.getComponentClassifier() : compInst.getSubcomponent();
		TimingModel timing = null;

		boolean foundSubNode = false;
		boolean hasDirectAnnex = false;
		boolean hasSubcomponents = false;
		ComponentClassifier compClass = compInst.getComponentClassifier();
		if (compClass instanceof ComponentImplementation && (isTop || isMonolithic)) {
			AgreeContractSubclause annex = getAgreeAnnex(compClass);

			for (ComponentInstance subInst : compInst.getComponentInstances()) {
				hasSubcomponents = true;
				curInst = subInst;
				XagreeNode subNode = getAgreeNode(subInst, false);
				if (subNode != null) {
					foundSubNode = true;
					subNodes.add(subNode);
				}
			}
			if(annex != null && XagreeStatementsRepository.getRepistory().isAbstract(annex.getContract().getContainingClassifier().getQualifiedName())) {
				throw new XagreeException("cannot run verification on annex with abstract or unbound inherited abstract clauses");
			}
			boolean latched = false;
			if (annex != null) {
				hasDirectAnnex = true;
				AgreeContract contract = (AgreeContract) annex.getContract();

				List<SpecStatement> specs = null;
				try {
					specs = XagreeStatementsRepository.getRepistory().getStatements(contract.getContainingClassifier().getQualifiedName());
				} catch (Exception e) {
					e.printStackTrace();
				}
				if(XagreeStatementsRepository.getRepistory().isAbstract(contract.getContainingClassifier().getQualifiedName())) {
					return buildNode(id, inputs, outputs, locals, localEquations, connections, subNodes,
							assertions, assumptions, guarantees, lemmas, patternProps, clockConstraint,
							initialConstraint, clockVar, reference, timing, compInst, timeOfVarMap, timeRiseVarMap,
							timeFallVarMap);
				}
				
				curInst = compInst;
				assertions.addAll(getAssertionStatements(specs));
				assertions.addAll(getEquationStatements(specs));
				assertions.addAll(getPropertyStatements(specs));
				assertions.addAll(getAssignmentStatements(specs));
				userDefinedConections.addAll(getConnectionStatements(specs));

				lemmas.addAll(getLemmaStatements(specs));
				addLustreNodes(specs);
				gatherLustreTypes(specs);
				// the clock constraints contain other nodes that we add
				clockConstraint = getClockConstraint(specs, subNodes);
				timing = getTimingModel(specs);

				outputs.addAll(getEquationVars(specs, compInst));

				for (SpecStatement spec : specs) {
					if (spec instanceof LatchedStatement) {
						latched = true;
						break;
					}
				}

			}
			aadlConnections.addAll(getConnections(((ComponentImplementation) compClass).getAllConnections(), compInst,
					subNodes, latched));

			connections.addAll(filterConnections(aadlConnections, userDefinedConections));

			// make compClass the type so we can get it's other contract
			// elements
			compClass = ((ComponentImplementation) compClass).getType();
		} else if (compClass instanceof ComponentImplementation) {
			compClass = ((ComponentImplementation) compClass).getType();
		}
		curInst = compInst;

		if (timing == null) {
			timing = TimingModel.SYNC;
		}

		AgreeContractSubclause annex = getAgreeAnnex(compClass);
		if (annex != null) {
			hasDirectAnnex = true;
			AgreeContract contract = (AgreeContract) annex.getContract();
			//this makes files for monolithic verification a bit smaller
			List<SpecStatement> specs = null;
			try {
				specs = XagreeStatementsRepository.getRepistory().getStatements(contract.getContainingClassifier().getQualifiedName());
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(XagreeStatementsRepository.getRepistory().isAbstract(contract.getContainingClassifier().getQualifiedName())) {
				return buildNode(id, inputs, outputs, locals, localEquations, connections, subNodes,
						assertions, assumptions, guarantees, lemmas, patternProps, clockConstraint,
						initialConstraint, clockVar, reference, timing, compInst, timeOfVarMap, timeRiseVarMap,
						timeFallVarMap);
			}
			if (isTop || !hasSubcomponents) {
				assumptions.addAll(getAssumptionStatements(specs));
				guarantees.addAll(getGuaranteeStatements(specs));
			}
			// we count eqstatements with expressions as assertions
			assertions.addAll(getEquationStatements(specs));
			assertions.addAll(getPropertyStatements(specs));
			outputs.addAll(getEquationVars(specs, compInst));
			inputs.addAll(getAgreeInputVars(specs, compInst));
			initialConstraint = getInitialConstraint(specs);

			addLustreNodes(specs);
			gatherLustreTypes(specs);
		}

		if (!(foundSubNode || hasDirectAnnex)) {
			return null;
		}
		gatherOutputsInputsTypes(outputs, inputs, compInst.getFeatureInstances(), typeMap, globalTypes);

		// verify that every variable that is reasoned about is
		// in a component containing an annex
		assertReferencedSubcomponentHasAnnex(compInst, inputs, outputs, subNodes, assertions, lemmas);

		XagreeNode result = buildNode(id, inputs, outputs, locals, localEquations, connections, subNodes,
				assertions, assumptions, guarantees, lemmas, patternProps, clockConstraint,
				initialConstraint, clockVar, reference, timing, compInst, timeOfVarMap, timeRiseVarMap,
				timeFallVarMap);

		renamings.put(id, compInst.getName());
		refMap.put(id, compInst);

		return linearizationRewriter.visit(result);
	}

	private List<XagreeConnection> filterConnections(List<XagreeAADLConnection> aadlConnections,
			List<XagreeOverriddenConnection> userDefinedConections) {
		List<XagreeConnection> conns = new ArrayList<>();
		// TODO right now for event ports this will copy an overridden
		// connection twice
		for (XagreeAADLConnection aadlConn : aadlConnections) {
			EObject aadlRef = aadlConn.reference;
			XagreeConnection replacementConn = aadlConn;
			for (XagreeOverriddenConnection agreeConn : userDefinedConections) {
				EObject agreeRef = agreeConn.aadlConn;
				if (aadlRef == agreeRef) {
					replacementConn = agreeConn;
					break;
				}
			}
			conns.add(replacementConn);
		}

		//throw errors for non-override connections with multiple fanin
		Set<XagreeVar> destinations = new HashSet<>();

		for(XagreeConnection conn : conns){
			if(conn instanceof XagreeAADLConnection){
				XagreeAADLConnection aadlConn = (XagreeAADLConnection)conn;
				if(!destinations.add(aadlConn.destVar)){
					String message = "Multiple connections to feature '" + aadlConn.destVar + "'. Remove "
							+ "the additional AADL connections or override them with a connection statement "
							+ "in the AGREE annex.";
					throw new XagreeException(message);
				}
			}

		}

		return conns;
	}

	private List<XagreeVar> getAgreeInputVars(List<SpecStatement> specs, ComponentInstance compInst) {
		List<XagreeVar> agreeVars = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof InputStatement) {
				EList<Arg> args = ((InputStatement) spec).getLhs();
				List<VarDecl> vars = agreeVarsFromArgs(args, compInst);
				for (VarDecl var : vars) {
					agreeVars.add((XagreeVar) var);
				}
			}
		}
		return agreeVars;
	}

	private void assertReferencedSubcomponentHasAnnex(ComponentInstance compInst, List<XagreeVar> inputs,
			List<XagreeVar> outputs, List<XagreeNode> subNodes, List<XagreeStatement> assertions,
			List<XagreeStatement> lemmas) {
		Set<String> allExprIds = new HashSet<>();
		for (XagreeStatement statement : assertions) {
			allExprIds.addAll(gatherStatementIds(statement));
		}
		for (XagreeStatement statement : lemmas) {
			allExprIds.addAll(gatherStatementIds(statement));
		}
		for (String idStr : allExprIds) {
			if (idStr.contains(dotChar) && !(idStr.endsWith(XagreePatternTranslator.FALL_SUFFIX)
					|| idStr.endsWith(XagreePatternTranslator.RISE_SUFFIX)
					|| idStr.endsWith(XagreePatternTranslator.TIME_SUFFIX))) {
				String prefix = idStr.substring(0, idStr.indexOf(dotChar));
				boolean found = false;
				for (XagreeVar var : inputs) {
					if (var.id.startsWith(prefix + dotChar)) {
						found = true;
						break;
					}
				}
				for (XagreeVar var : outputs) {
					if (var.id.startsWith(prefix + dotChar)) {
						found = true;
						break;
					}
				}
				for (XagreeNode subNode : subNodes) {
					if (subNode.id.equals(prefix)) {
						found = true;
						break;
					}
				}
				if (!found) {
					throw new XagreeException(
							"Variable '" + idStr.replace(dotChar, ".") + "' appears in an assertion, lemma "
									+ "or equation statement in component '" + compInst.getInstanceObjectPath()
									+ "' but subcomponent '" + prefix + "' does " + "not contain an AGREE annex");
				}
			}
		}
	}

	private Set<String> gatherStatementIds(XagreeStatement statement) {
		IdGatherer visitor = new IdGatherer();
		Set<String> ids = new HashSet<>();
		if (statement instanceof XagreeCauseEffectPattern) {
			XagreeCauseEffectPattern pattern = (XagreeCauseEffectPattern) statement;
			ids.addAll(pattern.cause.accept(visitor));
			ids.addAll(pattern.effect.accept(visitor));
		} else if (statement instanceof XagreePeriodicPattern) {
			XagreePeriodicPattern pattern = (XagreePeriodicPattern) statement;
			ids.addAll(pattern.event.accept(visitor));
		} else if (statement instanceof XagreeSporadicPattern){
			XagreeSporadicPattern pattern = (XagreeSporadicPattern) statement;
			ids.addAll(pattern.event.accept(visitor));
		} else {
			ids.addAll(statement.expr.accept(visitor));
		}
		return ids;
	}

	private List<XagreeStatement> getLemmaStatements(List<SpecStatement> specs) {
		List<XagreeStatement> lemmas = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof LemmaStatement) {
				LemmaStatement lemma = (LemmaStatement) spec;
				if (lemma.getExpr() != null) {
					lemmas.add(new XagreeStatement(lemma.getStr(), doSwitch(lemma.getExpr()), spec));
				} else {
					PatternStatement pattern = lemma.getPattern();
					lemmas.add(new XagreePatternBuilder(lemma.getStr(), lemma, this).doSwitch(pattern));
				}
			}
		}
		return lemmas;
	}

	private TimingModel getTimingModel(List<SpecStatement> specs) {
		for (SpecStatement spec : specs) {
			if (spec instanceof MNSynchStatement) {
				return TimingModel.ASYNC;
			}
			if (spec instanceof CalenStatement) {
				throw new XagreeException("The use of calendar statements has been depricated");
			}
			if (spec instanceof AsynchStatement) {
				return TimingModel.ASYNC;
			}
			if (spec instanceof LatchedStatement) {
				return TimingModel.LATCHED;
			}
			if (spec instanceof SynchStatement) {
//				int val = Integer.valueOf(((SynchStatement) spec).getVal());
//				if (val != 0) {
//					return TimingModel.ASYNC;
//				}
				return TimingModel.ASYNC;
			}
		}
		return TimingModel.SYNC;
	}

	private List<XagreeVar> getEquationVars(List<SpecStatement> specs, ComponentInstance compInst) {
		List<XagreeVar> agreeVars = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof EqStatement) {
				Arg arg = ((EqStatement) spec).getLhs();
				List<Arg> args = new ArrayList<Arg>();
				args.add(arg);
				List<VarDecl> vars = agreeVarsFromArgs(args, compInst);
				for (VarDecl var : vars) {
					agreeVars.add((XagreeVar) var);
				}
			} else if (spec instanceof PropertyStatement) {
				agreeVars.add(new XagreeVar(((PropertyStatement) spec).getName(), NamedType.BOOL, spec, compInst, null));
			}
		}
		return agreeVars;
	}

	private void gatherOutputsInputsTypes(List<XagreeVar> outputs, List<XagreeVar> inputs,
			EList<FeatureInstance> features, Map<NamedElement, String> typeMap, Set<Type> typeExpressions) {
		for (FeatureInstance feature : features) {
			featureToAgreeVars(outputs, inputs, feature, typeMap, typeExpressions);
		}

	}

	private void featureToAgreeVars(List<XagreeVar> outputs, List<XagreeVar> inputs, FeatureInstance feature,
			Map<NamedElement, String> typeMap, Set<Type> typeExpressions) {

		switch (feature.getCategory()) {
		case FEATURE_GROUP:
			List<XagreeVar> newInputs = new ArrayList<>();
			List<XagreeVar> newOutputs = new ArrayList<>();
			gatherOutputsInputsTypes(newOutputs, newInputs, feature.getFeatureInstances(), typeMap, typeExpressions);
			for (XagreeVar agreeVar : newInputs) {
				String newName = feature.getName() + dotChar + agreeVar.id;
				inputs.add(new XagreeVar(newName, agreeVar.type, feature.getFeature(), feature.getComponentInstance(),
						feature));
			}
			for (XagreeVar agreeVar : newOutputs) {
				String newName = feature.getName() + dotChar + agreeVar.id;
				outputs.add(new XagreeVar(newName, agreeVar.type, feature.getFeature(), feature.getComponentInstance(),
						feature));
			}
			return;
		case DATA_PORT:
		case EVENT_DATA_PORT:
			portToAgreeVar(outputs, inputs, feature, typeMap, typeExpressions);
			return;
		case DATA_ACCESS:
			break;
		default:
			break; // TODO: handle other types
		}

		return;
	}

	private void portToAgreeVar(List<XagreeVar> outputs, List<XagreeVar> inputs, FeatureInstance feature,
			Map<NamedElement, String> typeMap, Set<Type> typeExpressions) {

		DataSubcomponentType dataClass;
		Feature dataFeature = feature.getFeature();
		if (dataFeature instanceof DataPort) {
			DataPort dataPort = (DataPort) dataFeature;
			dataClass = dataPort.getDataFeatureClassifier();
		} else {
			EventDataPort eventDataPort = (EventDataPort) dataFeature;
			dataClass = eventDataPort.getDataFeatureClassifier();
		}

		String name = feature.getName();
		boolean isEvent = feature.getCategory() == FeatureCategory.EVENT_DATA_PORT;
		if (isEvent) {
			XagreeVar var = new XagreeVar(name + eventSuffix, NamedType.BOOL, feature.getFeature(),
					feature.getComponentInstance(), feature);
			switch (feature.getDirection()) {
			case IN:
				inputs.add(var);
				break;
			case OUT:
				outputs.add(var);
				break;
			default:
				throw new XagreeException(
						"Unable to reason about bi-directional event port: " + dataFeature.getQualifiedName());
			}
		}

		if (dataClass == null) {
			// we do not reason about this type
			return;
		}

		Type type = XagreeTypeUtils.getType(dataClass, typeMap, typeExpressions);
		if(type == null){
			//we do not reason about this type
			return;
		}

		XagreeVar agreeVar = new XagreeVar(name, type, feature.getFeature(), feature.getComponentInstance(), feature);

		switch (feature.getDirection()) {
		case IN:
			inputs.add(agreeVar);
			break;
		case OUT:
			outputs.add(agreeVar);
			break;
		default:
			throw new XagreeException(
					"Unable to reason about bi-directional event port: " + dataFeature.getQualifiedName());
		}
	}

	private List<XagreeAADLConnection> getConnections(EList<Connection> connections, ComponentInstance compInst,
			List<XagreeNode> subNodes, boolean latched) {

//		Set<AgreeVar> destinationSet = new HashSet<>();

		Property commTimingProp = EMFIndexRetrieval.getPropertyDefinitionInWorkspace(OsateResourceUtil.getResourceSet(),
				"Communication_Properties::Timing");
		List<XagreeAADLConnection> agreeConns = new ArrayList<>();
		for (Connection conn : connections) {

			ConnectedElement absConnDest = conn.getDestination();
			ConnectedElement absConnSour = conn.getSource();
			boolean delayed = false;
			try {
				EnumerationLiteral lit = PropertyUtils.getEnumLiteral(conn, commTimingProp);
				delayed = lit.getName().equals("delayed");
			} catch (PropertyDoesNotApplyToHolderException e) {
				delayed = false;
			}
			Context destContext = absConnDest.getContext();
			Context sourContext = absConnSour.getContext();
			// only make connections to things that have annexs
			if (destContext != null && destContext instanceof Subcomponent) {
				ComponentInstance subInst = compInst.findSubcomponentInstance((Subcomponent) destContext);
				if (!XagreeUtils.containsTransitiveAgreeAnnex(subInst, isMonolithic)) {
					continue;
				}
			}
			if (sourContext != null && sourContext instanceof Subcomponent) {
				ComponentInstance subInst = compInst.findSubcomponentInstance((Subcomponent) sourContext);
				if (!XagreeUtils.containsTransitiveAgreeAnnex(subInst, isMonolithic)) {
					continue;
				}
			}

			XagreeNode sourceNode = agreeNodeFromNamedEl(subNodes, sourContext);
			XagreeNode destNode = agreeNodeFromNamedEl(subNodes, destContext);

			ConnectionEnd destPort = absConnDest.getConnectionEnd();
			ConnectionEnd sourPort = absConnSour.getConnectionEnd();

			if(sourPort instanceof DataSubcomponent || destPort instanceof DataSubcomponent){
				XagreeLogger.logWarning("unable to reason about connection '"+ conn.getName() +"' because it connects to a data subcomponent");
				continue;
			}

			//weirdness with feature groups
			String sourPrefix = null;
			if(sourContext instanceof FeatureGroup){
				sourPrefix = sourContext.getName();
			}
			String destPrefix = null;
			if(destContext instanceof FeatureGroup){
				destPrefix = destContext.getName();
			}

			List<XagreeVar> sourVars = getAgreePortNames(sourPort, sourPrefix,  sourceNode == null ? null : sourceNode.compInst);
			List<XagreeVar> destVars = getAgreePortNames(destPort, destPrefix, destNode == null ? null : destNode.compInst);

			if(sourVars.size() != destVars.size()){
				throw new XagreeException("The number of AGREE variables differ for connection '" + conn.getQualifiedName() +
						"'. Do the types of the source and destination differ? Perhaps one is an implementation and the other is a type?");
			}

			for(int i = 0; i < sourVars.size(); i++){
				XagreeVar sourVar = sourVars.get(i);
				XagreeVar destVar = destVars.get(i);

				if (!matches((ConnectionEnd)sourVar.reference, (ConnectionEnd)destVar.reference)) {
					XagreeLogger.logWarning("Connection '" + conn.getName() + "' has ports '" + sourVar.id.replace(dotChar, ".")
					+ "' and '" + destVar.id.replace(dotChar, ".") + "' of differing type");
					continue;
				}

				if(!sourVar.type.equals(destVar.type)){
					throw new XagreeException("Type mismatch during connection generation");
				}

				ConnectionType connType;

				if(sourVar.id.endsWith(eventSuffix)){
					connType = ConnectionType.EVENT;
				}else{
					connType = ConnectionType.DATA;
				}

				XagreeAADLConnection agreeConnection = new XagreeAADLConnection(sourceNode, destNode, sourVar, destVar,
						connType, latched, delayed, conn);

				agreeConns.add(agreeConnection);
			}


		}
		return agreeConns;
	}

	private Type getConnectionEndType(ConnectionEnd port){
		DataSubcomponentType dataClass = getConnectionEndDataClass(port);
		if(dataClass == null){
			return null;
		}
		return XagreeTypeUtils.getType(dataClass, typeMap, globalTypes);
	}

	private List<XagreeVar> getAgreePortNames(ConnectionEnd port, String prefix, ComponentInstance compInst) {
		String portName = port.getName();
		List<XagreeVar> subVars = new ArrayList<>();

		//if the port is a datasubcomponent then it is a member
		//of a record type. Otherwise it is the first member of a feature group
		if(prefix == null){
			prefix = "";
		}else if(port instanceof DataSubcomponent){
			prefix = prefix + ".";
		}else{
			prefix = prefix + dotChar;
		}

		if(port instanceof FeatureGroup){
			FeatureGroup featGroup = (FeatureGroup)port;
			FeatureGroupType featType = featGroup.getFeatureGroupType();
			for(FeatureGroup subFeatGroup : featType.getOwnedFeatureGroups()){
				subVars.addAll(getAgreePortNames(subFeatGroup, null, compInst));
			}
			for(DataPort subPort : featType.getOwnedDataPorts()){
				subVars.addAll(getAgreePortNames(subPort, null, compInst));
			}
			for(EventDataPort subPort : featType.getOwnedEventDataPorts()){
				subVars.addAll(getAgreePortNames(subPort, null, compInst));
			}
			for(EventPort subPort : featType.getOwnedEventPorts()){
				subVars.addAll(getAgreePortNames(subPort, null, compInst));
			}

			List<XagreeVar> prefixedStrs = new ArrayList<>();
			for(XagreeVar subVar : subVars){
				prefixedStrs.add(new XagreeVar(prefix + portName + dotChar + subVar.id, subVar.type, subVar.reference, compInst));
			}
			subVars = prefixedStrs;
		}
		if(port instanceof DataPort ||
				port instanceof EventDataPort ||
				port instanceof DataSubcomponent){
			Type type = getConnectionEndType(port);
			if (type != null) {
				subVars.add(new XagreeVar(prefix + portName, type, port, compInst));
			}
		}
		if(port instanceof EventDataPort || port instanceof EventPort){
			subVars.add(new XagreeVar(prefix + portName + eventSuffix, NamedType.BOOL, port, compInst));
		}

		return subVars;

	}

	private DataSubcomponentType getConnectionEndDataClass(ConnectionEnd port) {
		DataSubcomponentType dataClass = null;
		if (port instanceof DataPort) {
			DataPort dataPort = (DataPort) port;
			dataClass = dataPort.getDataFeatureClassifier();
		} else if (port instanceof EventDataPort) {
			EventDataPort eventDataPort = (EventDataPort) port;
			dataClass = eventDataPort.getDataFeatureClassifier();
		} else if (port instanceof DataSubcomponent) {
			dataClass = ((DataSubcomponent) port).getDataSubcomponentType();
		}
		if (dataClass == null) {
			XagreeLogger.logWarning("Unable to determine the type of port '" + port + "'");
		}
		return dataClass;
	}

	private boolean matches(ConnectionEnd a, ConnectionEnd b) {
		if (a instanceof EventDataPort ^ b instanceof EventDataPort) {
			return false;
		}
		return true;
	}

	private XagreeNode agreeNodeFromNamedEl(List<XagreeNode> nodes, NamedElement comp) {
		if (comp == null) {
			return null;
		}
		for (XagreeNode node : nodes) {
			if (comp.getName().equals(node.id)) {
				return node;
			}
		}
		return null;
	}

	private Expr getInitialConstraint(List<SpecStatement> specs) {
		for (SpecStatement spec : specs) {
			if (spec instanceof InitialStatement) {
				return doSwitch(((InitialStatement) spec).getExpr());
			}
		}
		return new BoolExpr(true);
	}

	private Expr getClockConstraint(List<SpecStatement> specs, List<XagreeNode> subNodes) {
		// NOTE: we generate the constraint that "someone ticks" during the
		// lustre generation
		for (SpecStatement spec : specs) {
			if (spec instanceof MNSynchStatement) {
				return getMNSynchConstraint((MNSynchStatement) spec);
			}
			if (spec instanceof CalenStatement) {
				throw new XagreeException("The use of calendar statements has been depricated");
			}
			if (spec instanceof AsynchStatement) {
				return new BoolExpr(true);
			}
			if (spec instanceof LatchedStatement) {
				return new BoolExpr(true);
			}
			if (spec instanceof SynchStatement) {
				return getSynchConstraint((SynchStatement) spec, subNodes);
			}
		}

		return new BoolExpr(true);
	}

	private Expr getSynchConstraint(SynchStatement spec, List<XagreeNode> subNodes) {

		int val1 = Integer.decode(spec.getVal());
		if (val1 == 0) {
			return new BoolExpr(true);
		}

		List<Expr> clockIds = new ArrayList<>();
		Expr clockAssertion;
		for (XagreeNode subNode : subNodes) {
			clockIds.add(new IdExpr(subNode.clockVar.id));
		}

		String dfaPrefix = XagreeTypeUtils.getObjectLocationPrefix(spec);

		if (spec.getVal2() == null) {
			Node dfaNode = XagreeCalendarUtils.getDFANode(dfaPrefix + "__DFA_NODE", val1);
			Node calNode = XagreeCalendarUtils.getCalendarNode(dfaPrefix + "__CALENDAR_NODE", dfaNode.id,
					clockIds.size());

			// we do not need to make copies of the nodes if they exist
			if (!nodeNameExists(dfaNode.id)) {
				if (nodeNameExists(calNode.id)) {
					throw new XagreeException("The calander node should not exist if the dfa node does not exist");
				}
				addToNodeList(dfaNode);
				addToNodeList(calNode);
			}

			clockAssertion = new NodeCallExpr(calNode.id, clockIds);
		} else {
			int val2 = Integer.decode(spec.getVal2());

			String nodeName = "__calendar_node_" + val1 + "_" + val2;
			nodeName = dfaPrefix + nodeName;
			Node calNode = XagreeCalendarUtils.getMNCalendar(nodeName, val1, val2);

			if (!nodeNameExists(calNode.id)) {
				addToNodeList(calNode);
			}

			clockAssertion = new BoolExpr(true);
			int i, j;
			for (i = 0; i < clockIds.size(); i++) {
				Expr clock1 = clockIds.get(i);
				for (j = i + 1; j < clockIds.size(); j++) {
					Expr clock2 = clockIds.get(j);
					NodeCallExpr nodeCall = new NodeCallExpr(nodeName, clock1, clock2);
					clockAssertion = new BinaryExpr(clockAssertion, BinaryOp.AND, nodeCall);
					nodeCall = new NodeCallExpr(nodeName, clock2, clock1);
					clockAssertion = new BinaryExpr(clockAssertion, BinaryOp.AND, nodeCall);
				}
			}
		}
		return clockAssertion;
	}

	private Expr getMNSynchConstraint(MNSynchStatement sync) {

		Set<String> nodeNames = new HashSet<>();
		Expr clockAssertion = new BoolExpr(true);
		for (int i = 0; i < sync.getComp1().size(); i++) {
			Subcomponent maxComp = (Subcomponent) sync.getComp1().get(i);
			Subcomponent minComp = (Subcomponent) sync.getComp2().get(i);

			Expr maxClock = new IdExpr(maxComp.getName() + clockIDSuffix);
			Expr minClock = new IdExpr(minComp.getName() + clockIDSuffix);
			int max = Integer.valueOf(sync.getMax().get(i));
			int min = Integer.valueOf(sync.getMin().get(i));

			MNSynchronyElement elem = new MNSynchronyElement(maxClock, minClock, max, min);

			String nodeName = "__calendar_node_" + elem.max + "_" + elem.min;
			nodeName = XagreeTypeUtils.getObjectLocationPrefix(sync) + nodeName;
			if (!nodeNames.contains(nodeName)) {
				nodeNames.add(nodeName);
				Node calNode = XagreeCalendarUtils.getMNCalendar(nodeName, elem.max, elem.min);
				addToNodeList(calNode);
			}

			NodeCallExpr nodeCall = new NodeCallExpr(nodeName, elem.maxClock, elem.minClock);
			clockAssertion = new BinaryExpr(clockAssertion, BinaryOp.AND, nodeCall);
			nodeCall = new NodeCallExpr(nodeName, elem.minClock, elem.maxClock);
			clockAssertion = new BinaryExpr(clockAssertion, BinaryOp.AND, nodeCall);
		}

		return clockAssertion;
	}

	private List<Type> gatherLustreTypes(List<SpecStatement> specs) {
		List<Type> types = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof RecordDefExpr) {
				// this will record them to the global types
				XagreeTypeUtils.getType((NamedElement) spec, typeMap, globalTypes);
			}
		}
		return types;
	}

	private List<Node> addLustreNodes(List<SpecStatement> specs) {
		List<Node> nodes = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof NodeDefExpr || spec instanceof FnDefExpr) {
				doSwitch(spec);
			}
		}
		return nodes;
	}

	public VarDecl agreeVarFromArg(Arg arg, ComponentInstance compInst) {
		NamedType type = getNamedType(XagreeTypeUtils.getTypeName(arg.getType(), typeMap, globalTypes));
		return new XagreeVar(arg.getName(), type, arg, compInst, null);
	}
	// MWW: made this public.
	public List<VarDecl> agreeVarsFromArgs(List<Arg> args, ComponentInstance compInst) {
		List<VarDecl> agreeVars = new ArrayList<>();
		for (Arg arg : args) {
			agreeVars.add(agreeVarFromArg(arg, compInst));
		}
		return agreeVars;
	}

	private List<XagreeStatement> getAssertionStatements(List<SpecStatement> specs) {
		List<XagreeStatement> asserts = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof AssertStatement) {
				AssertStatement assertState = (AssertStatement) spec;
				String str = assertState.getStr();

				if (assertState.getExpr() != null) {
					asserts.add(new XagreeStatement(str, doSwitch(assertState.getExpr()), assertState));
				} else {
					PatternStatement pattern = assertState.getPattern();
					asserts.add(new XagreePatternBuilder(str, assertState, this).doSwitch(pattern));
				}
			}
		}
		return asserts;
	}

	private List<XagreeOverriddenConnection> getConnectionStatements(List<SpecStatement> specs) {
		List<XagreeOverriddenConnection> conns = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof ConnectionStatement) {
				Expr expr = doSwitch(((ConnectionStatement) spec).getExpr());
				Connection conn = (Connection) ((ConnectionStatement) spec).getConn();
				XagreeOverriddenConnection agreeConn = new XagreeOverriddenConnection(new XagreeStatement("", expr, spec),
						conn);
				conns.add(agreeConn);
			}
		}
		return conns;
	}

	private List<XagreeStatement> getAssignmentStatements(List<SpecStatement> specs) {
		List<XagreeStatement> assigns = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof AssignStatement) {
				Expr expr = doSwitch(((AssignStatement) spec).getExpr());
				expr = new BinaryExpr(new IdExpr(((AssignStatement) spec).getId().getBase().getName()), BinaryOp.EQUAL,
						expr);
				assigns.add(new XagreeStatement("", expr, spec));
			}
		}
		return assigns;
	}

	private List<XagreeStatement> getPropertyStatements(List<SpecStatement> specs) {
		List<XagreeStatement> props = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof PropertyStatement) {
				Expr expr = doSwitch(((PropertyStatement) spec).getExpr());
				expr = new BinaryExpr(new IdExpr(((PropertyStatement) spec).getName()), BinaryOp.EQUAL, expr);
				props.add(new XagreeStatement("", expr, spec));
			}
		}
		return props;
	}

	private List<XagreeStatement> getEquationStatements(List<SpecStatement> specs) {
		List<XagreeStatement> eqs = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof EqStatement) {
				EqStatement eq = (EqStatement) spec;
				Arg lhs = eq.getLhs();
				if (eq.getExpr() != null) {
					Expr expr = doSwitch(eq.getExpr());
					List<Expr> ids = new ArrayList<>();
					ids.add(new IdExpr(lhs.getName()));
					expr = new BinaryExpr(new IdExpr(lhs.getName()), BinaryOp.EQUAL, expr);
					eqs.add(new XagreeStatement("", expr, spec));
				}
				List<Arg> args = new ArrayList<Arg>();
				args.add(lhs);
				eqs.addAll(getVariableRangeConstraints(args, eq));
			}
		}
		return eqs;
	}

	private List<XagreeStatement> getVariableRangeConstraints(List<Arg> args, EqStatement eq) {
		List<XagreeStatement> constraints = new ArrayList<>();
		for (Arg arg : args) {
			if (arg.getType() instanceof PrimType) {
				PrimType primType = (PrimType) arg.getType();
				String lowStr = primType.getRangeLow();
				String highStr = primType.getRangeHigh();

				if (lowStr != null && highStr != null) {
					IdExpr id = new IdExpr(arg.getName());
					int lowSign = primType.getLowNeg() == null ? 1 : -1;
					int highSign = primType.getHighNeg() == null ? 1 : -1;
					Expr lowVal = null;
					Expr highVal = null;

					switch (primType.getString()) {
					case "int":
						long lowl = Long.valueOf(lowStr) * lowSign;
						long highl = Long.valueOf(highStr) * highSign;
						lowVal = new IntExpr(BigInteger.valueOf(lowl));
						highVal = new IntExpr(BigInteger.valueOf(highl));
						break;
					case "real":
						double lowd = Double.valueOf(lowStr) * lowSign;
						double highd = Double.valueOf(highStr) * highSign;
						lowVal = new RealExpr(BigDecimal.valueOf(lowd));
						highVal = new RealExpr(BigDecimal.valueOf(highd));
						break;
					default:
						throw new XagreeException("Unhandled type '" + primType.getString() + "' in ranged type");
					}
					Expr lowBound = new BinaryExpr(lowVal, BinaryOp.LESSEQUAL, id);
					Expr highBound = new BinaryExpr(id, BinaryOp.LESSEQUAL, highVal);
					Expr bound = new BinaryExpr(lowBound, BinaryOp.AND, highBound);
					// must have reference to eq statement so we don't throw
					// them away later
					constraints.add(new XagreeStatement("", bound, eq));
				}
			}
		}

		return constraints;
	}

	private List<XagreeStatement> getAssumptionStatements(List<SpecStatement> specs) {
		List<XagreeStatement> assumptions = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof AssumeStatement) {
				AssumeStatement assumption = (AssumeStatement) spec;
				String str = assumption.getStr();
				if (assumption.getExpr() != null) {
					assumptions.add(new XagreeStatement(str, doSwitch(assumption.getExpr()), assumption));
				} else {
					PatternStatement pattern = assumption.getPattern();
					assumptions.add(new XagreePatternBuilder(str, assumption, this).doSwitch(pattern));
				}
			}
		}
		return assumptions;
	}

	private List<XagreeStatement> getGuaranteeStatements(List<SpecStatement> specs) {
		List<XagreeStatement> guarantees = new ArrayList<>();
		for (SpecStatement spec : specs) {
			if (spec instanceof GuaranteeStatement) {
				GuaranteeStatement guarantee = (GuaranteeStatement) spec;
				String str = guarantee.getStr();
				if (guarantee.getExpr() != null) {
					guarantees.add(new XagreeStatement(str, doSwitch(guarantee.getExpr()), guarantee));
				} else {
					PatternStatement pattern = guarantee.getPattern();
					guarantees.add(new XagreePatternBuilder(str, guarantee, this).doSwitch(pattern));
				}
			}
		}
		return guarantees;
	}

	private AgreeContractSubclause getAgreeAnnex(ComponentClassifier comp) {
		for (AnnexSubclause annex : AnnexUtil.getAllAnnexSubclauses(comp,
				XagreePackage.eINSTANCE.getAgreeContractSubclause())) {
			if (annex instanceof AgreeContractSubclause) {
				// in newer versions of osate the annex this returns annexes in
				// the type
				// as well as the implementation. We want the annex in the
				// specific component
				EObject container = annex.eContainer();
				while (!(container instanceof ComponentClassifier)) {
					container = container.eContainer();
				}
				if (container == comp) {
					return (AgreeContractSubclause) annex;
				}
			}
		}
		return null;
	}

	private static NamedType getNamedType(String name) {
		if (name == null) {
			return null;
		}
		switch (name) {
		case "bool":
			return NamedType.BOOL;
		case "real":
			return NamedType.REAL;
		case "int":
			return NamedType.INT;
		default:
			return new NamedType(name);
		}
	}

	// BEGIN CASE EXPRESSION STATEMENTS
	@Override
	public Expr caseRecordUpdateExpr(RecordUpdateExpr upExpr) {
		EList<NamedElement> args = upExpr.getArgs();
		EList<edu.clemson.xagree.xagree.Expr> argExprs = upExpr.getArgExpr();

		Expr lustreExpr = doSwitch(upExpr.getRecord());
		for (int i = 0; i < args.size(); i++) {
			edu.clemson.xagree.xagree.Expr argExpr = argExprs.get(i);
			NamedElement arg = args.get(i);
			Expr lustreArgExpr = doSwitch(argExpr);
			lustreExpr = new jkind.lustre.RecordUpdateExpr(lustreExpr, arg.getName(), lustreArgExpr);
		}

		return lustreExpr;
	}

	@Override
	public Expr caseTimeOfExpr(TimeOfExpr timeExpr) {
		NestedDotID nestId = timeExpr.getId();
		NamedElement namedEl = nestId.getBase();
		String idStr = namedEl.getName();

		XagreeVar var = timeOfVarMap.get(idStr);
		if (var == null) {
			String varStr = idStr + XagreePatternTranslator.TIME_SUFFIX;
			var = new XagreeVar(varStr, NamedType.REAL, namedEl);
			timeOfVarMap.put(idStr, var);
		}

		return new IdExpr(var.id);
	}

	@Override
	public Expr caseTimeRiseExpr(TimeRiseExpr timeExpr) {
		NestedDotID nestId = timeExpr.getId();
		NamedElement namedEl = nestId.getBase();
		String idStr = namedEl.getName();

		XagreeVar var = timeRiseVarMap.get(idStr);
		if (var == null) {
			String varStr = idStr + XagreePatternTranslator.RISE_SUFFIX;
			var = new XagreeVar(varStr, NamedType.REAL, namedEl);
			timeRiseVarMap.put(idStr, var);
		}

		return new IdExpr(var.id);
	}

	@Override
	public Expr caseTimeFallExpr(TimeFallExpr timeExpr) {
		NestedDotID nestId = timeExpr.getId();
		NamedElement namedEl = nestId.getBase();
		String idStr = namedEl.getName();

		XagreeVar var = timeFallVarMap.get(idStr);
		if (var == null) {
			String varStr = idStr + XagreePatternTranslator.FALL_SUFFIX;
			var = new XagreeVar(varStr, NamedType.REAL, namedEl);
			timeFallVarMap.put(idStr, var);
		}

		return new IdExpr(var.id);
	}

	@Override
	public Expr caseRecordExpr(RecordExpr recExpr) {

		EList<NamedElement> agreeArgs = recExpr.getArgs();
		EList<edu.clemson.xagree.xagree.Expr> agreeArgExprs = recExpr.getArgExpr();
		Map<String, Expr> argExprMap = new HashMap<String, Expr>();

		for (int i = 0; i < agreeArgs.size(); i++) {
			NamedElement agreeArg = agreeArgs.get(i);
			edu.clemson.xagree.xagree.Expr agreeExpr = agreeArgExprs.get(i);

			Expr lustreExpr = doSwitch(agreeExpr);
			String argName = agreeArg.getName();

			argExprMap.put(argName, lustreExpr);

		}

		NestedDotID recId = recExpr.getRecord();
		while (recId.getSub() != null) {
			recId = recId.getSub();
		}
		String recName = XagreeTypeUtils.getIDTypeStr(recId.getBase());
		return new jkind.lustre.RecordExpr(recName, argExprMap);

	}

	@Override
	public Expr caseFloorCast(FloorCast floor) {
		Expr expr = doSwitch(floor.getExpr());
		Expr castExpr = new CastExpr(NamedType.INT, expr);
		return castExpr;
	}

	@Override
	public Expr caseRealCast(RealCast real) {
		Expr expr = doSwitch(real.getExpr());
		Expr castExpr = new CastExpr(NamedType.REAL, expr);
		return castExpr;
	}

	/*	@Override
	public Expr caseBinaryNonLinearExpr(edu.clemson.xagree.xagree.BinaryNonLinearExpr expr) {
		Expr leftExpr = doSwitch(expr.getLeft());
		Expr rightExpr = doSwitch(expr.getRight());
		String op = expr.getOp();
		BinaryOp binOp = null;
		switch (op) {
		case "pow": binOp = BinaryOp.POW; break;
		case "arctan2" : binOp = BinaryOp.ARCTAN2; break;
		}
		assert (binOp != null);
		BinaryExpr binExpr = new BinaryExpr(leftExpr, binOp, rightExpr);

		return binExpr;
	}

	@Override
	public Expr caseUnaryNonLinearExpr(edu.clemson.xagree.xagree.UnaryNonLinearExpr expr) {
		Expr sub = doSwitch(expr.getExpr());
		String op = expr.getOp();
		UnaryOp unyOp = null;
		switch (op) {
		case "exp": unyOp = UnaryOp.EXP; break;
		case "log": unyOp = UnaryOp.LOG; break;
		case "sqrt": unyOp = UnaryOp.SQRT; break;
		case "sin": unyOp = UnaryOp.SIN; break;
		case "cos": unyOp = UnaryOp.COS; break;
		case "tan": unyOp = UnaryOp.TAN; break;
		case "asin":
		case "arcsin": unyOp = UnaryOp.ARCSIN; break;
		case "acos":
		case "arccos": unyOp = UnaryOp.ARCCOS; break;
		case "atan":
		case "arctan": unyOp = UnaryOp.ARCTAN; break;
		case "sinh" : unyOp = UnaryOp.SINH; break;
		case "cosh" : unyOp = UnaryOp.COSH; break;
		case "tanh" : unyOp = UnaryOp.TANH; break;
		case "matan":
		case "marctan": unyOp = UnaryOp.MATAN; break;
		}
		assert (unyOp != null);
		UnaryExpr unyExpr = new UnaryExpr(unyOp, sub);

		return unyExpr;

	}
	 */

	@Override
	public Expr caseBinaryExpr(edu.clemson.xagree.xagree.BinaryExpr expr) {

		Expr leftExpr = doSwitch(expr.getLeft());
		Expr rightExpr = doSwitch(expr.getRight());

		String op = expr.getOp();

		BinaryOp binOp = null;
		switch (op) {
		case "+":
			binOp = BinaryOp.PLUS;
			break;
		case "-":
			binOp = BinaryOp.MINUS;
			break;
		case "*":
			binOp = BinaryOp.MULTIPLY;
			break;
		case "/":
			binOp = BinaryOp.DIVIDE;
			break;
		case "mod":
			binOp = BinaryOp.MODULUS;
			break;
		case "div":
			binOp = BinaryOp.INT_DIVIDE;
			break;
		case "<=>":
		case "=":
			binOp = BinaryOp.EQUAL;
			break;
		case "!=":
		case "<>":
			binOp = BinaryOp.NOTEQUAL;
			break;
		case ">":
			binOp = BinaryOp.GREATER;
			break;
		case "<":
			binOp = BinaryOp.LESS;
			break;
		case ">=":
			binOp = BinaryOp.GREATEREQUAL;
			break;
		case "<=":
			binOp = BinaryOp.LESSEQUAL;
			break;
		case "or":
			binOp = BinaryOp.OR;
			break;
		case "and":
			binOp = BinaryOp.AND;
			break;
		case "xor":
			binOp = BinaryOp.XOR;
			break;
		case "=>":
			binOp = BinaryOp.IMPLIES;
			break;
		case "->":
			binOp = BinaryOp.ARROW;
			break;
		}

		assert (binOp != null);
		BinaryExpr binExpr = new BinaryExpr(leftExpr, binOp, rightExpr);

		return binExpr;
	}

	@Override
	public Expr caseBoolLitExpr(BoolLitExpr expr) {
		return new BoolExpr(expr.getVal().getValue());
	}

	@Override
	public Expr caseFnCallExpr(FnCallExpr expr) {
		NestedDotID dotId = expr.getFn();
		NamedElement namedEl = XagreeUtils.getFinalNestId(dotId);

		String fnName = XagreeTypeUtils.getNodeName(namedEl);
		boolean found = false;
		for (Node node : globalNodes) {
			if (node.id.equals(fnName)) {
				found = true;
				break;
			}
		}

		if (!found) {
			NestedDotID fn = expr.getFn();
			doSwitch(XagreeUtils.getFinalNestId(fn));
			// for dReal integration
			if (fnName.substring(0,7).equalsIgnoreCase("dreal__")) {
				fnName = namedEl.getName();
			}
		}

		List<Expr> argResults = new ArrayList<Expr>();

		for (edu.clemson.xagree.xagree.Expr argExpr : expr.getArgs()) {
			argResults.add(doSwitch(argExpr));
		}

		NodeCallExpr nodeCall = new NodeCallExpr(fnName, argResults);
		return nodeCall;
	}

	@Override
	public Expr caseFnDefExpr(FnDefExpr expr) {
		String nodeName = XagreeTypeUtils.getNodeName(expr);
		for (Node node : globalNodes) {
			if (node.id.equals(nodeName)) {
				return null;
			}
		}
		List<VarDecl> inputs = agreeVarsFromArgs(expr.getArgs(), null);
		Expr bodyExpr = doSwitch(expr.getExpr());
		NamedType outType = getNamedType(XagreeTypeUtils.getTypeName(expr.getType(), typeMap, globalTypes));
		VarDecl outVar = new VarDecl("_outvar", outType);
		List<VarDecl> outputs = Collections.singletonList(outVar);
		Equation eq = new Equation(new IdExpr("_outvar"), bodyExpr);
		List<Equation> eqs = Collections.singletonList(eq);

		NodeBuilder builder = new NodeBuilder(nodeName);
		builder.addInputs(inputs);
		builder.addOutputs(outputs);
		builder.addEquations(eqs);

		Node node = builder.build();
		addToNodeList(node);
		return null;
	}

	@Override
	public Expr caseLinearizationDefExpr(LinearizationDefExpr expr) {
		NodeDefExpr linearization = linearizationRewriter.addLinearization(expr);
		caseNodeDefExpr(linearization);
		return null;
	}

	@Override
	public Expr caseNodeDefExpr(NodeDefExpr expr) {

		String nodeName = XagreeTypeUtils.getNodeName(expr);

		for (Node node : globalNodes) {
			if (node.id.equals(nodeName)) {
				return null;
			}
		}

		List<VarDecl> inputs = agreeVarsFromArgs(expr.getArgs(), null);
		List<VarDecl> outputs = agreeVarsFromArgs(expr.getRets(), null);
		NodeBodyExpr body = expr.getNodeBody();
		List<VarDecl> internals = agreeVarsFromArgs(body.getLocs(), null);
		List<Equation> eqs = new ArrayList<Equation>();
		List<String> props = new ArrayList<String>();

		// TODO are node lemmas depricated?
		String lemmaName = "__nodeLemma";
		int lemmaIndex = 0;
		for (NodeStmt stmt : body.getStmts()) {
			if (stmt instanceof NodeLemma) {
				NodeLemma nodeLemma = (NodeLemma) stmt;
				String propName = lemmaName + lemmaIndex++;
				IdExpr eqId = new IdExpr(propName);
				internals.add(new VarDecl(eqId.id, NamedType.BOOL));
				Expr eqExpr = doSwitch(nodeLemma.getExpr());
				Equation eq = new Equation(eqId, eqExpr);
				eqs.add(eq);
				props.add(eqId.id);
			} else if (stmt instanceof NodeEq) {
				eqs.add(nodeEqToEq((NodeEq) stmt));
			}
		}

		// nodeLemmaNames.put(nodeName, lemmaNames);

		NodeBuilder builder = new NodeBuilder(nodeName);
		builder.addInputs(inputs);
		builder.addOutputs(outputs);
		builder.addLocals(internals);
		builder.addEquations(eqs);
		builder.addProperties(props);

		Node n = builder.build();
		addToNodeList(n);
		return null;
	}

	// helper method for above
	private Equation nodeEqToEq(NodeEq nodeEq) {
		Expr expr = doSwitch(nodeEq.getExpr());
		List<IdExpr> ids = new ArrayList<IdExpr>();
		for (Arg arg : nodeEq.getLhs()) {
			ids.add(new IdExpr(arg.getName()));
		}
		Equation eq = new Equation(ids, expr);
		return eq;
	}

	@Override
	public Expr caseGetPropertyExpr(GetPropertyExpr expr) {

		NamedElement propName = expr.getProp();
		NamedElement compName = XagreeTypeUtils.namedElFromId(expr.getComponent(), curInst);

		Property prop = (Property) propName;

		PropertyExpression propVal = XagreeUtils.getPropExpression(compName, prop);

		if (propVal == null) {
			throw new XagreeException("Could not locate property value '" + prop.getFullName() + "' in component '"
					+ compName.getName() + "'.  Is it possible "
					+ "that a 'this' statement is used in a context in which it wasn't supposed to?");
		}
		Expr res = null;
		if (propVal != null) {
			if (propVal instanceof StringLiteral) {
				// StringLiteral value = (StringLiteral) propVal;
				// nodeStr += value.getValue() + ")";
				throw new XagreeException("Property value for '" + prop.getFullName() + "' cannot be of string type");
			} else if (propVal instanceof NamedValue) {
				// NamedValue namedVal = (NamedValue) propVal;
				// AbstractNamedValue absVal = namedVal.getNamedValue();
				// assert (absVal instanceof EnumerationLiteral);
				// EnumerationLiteral enVal = (EnumerationLiteral) absVal;
				throw new XagreeException(
						"Property value for '" + prop.getFullName() + "' cannot be of enumeration type");
			} else if (propVal instanceof BooleanLiteral) {
				BooleanLiteral value = (BooleanLiteral) propVal;
				res = new BoolExpr(value.getValue());
			} else if (propVal instanceof IntegerLiteral) {
				IntegerLiteral value = (IntegerLiteral) propVal;
				res = new IntExpr(BigInteger.valueOf((long) value.getScaledValue()));
			} else {
				assert (propVal instanceof RealLiteral);
				RealLiteral value = (RealLiteral) propVal;
				res = new RealExpr(BigDecimal.valueOf(value.getValue()));
			}
		}
		assert (res != null);

		return res;
	}

	@Override
	public Expr caseThisExpr(ThisExpr expr) {
		throw new XagreeException("A 'this' expression should never be called in a case statement");
	}

	@Override
	public Expr caseIfThenElseExpr(edu.clemson.xagree.xagree.IfThenElseExpr expr) {
		Expr condExpr = doSwitch(expr.getA());
		Expr thenExpr = doSwitch(expr.getB());
		Expr elseExpr = doSwitch(expr.getC());

		Expr result = new IfThenElseExpr(condExpr, thenExpr, elseExpr);

		return result;
	}

	@Override
	public Expr caseIntLitExpr(IntLitExpr expr) {
		return new IntExpr(BigInteger.valueOf(Integer.parseInt(expr.getVal())));
	}

	@Override
	public Expr caseNestedDotID(NestedDotID id) {

		String jKindVar = "";
		String aadlVar = "";
		NamedElement base = id.getBase();

		while (id.getSub() != null
				&& (base instanceof FeatureGroup || base instanceof AadlPackage || base instanceof Subcomponent)) {
			jKindVar += base.getName() + dotChar;
			aadlVar += base.getName() + ".";
			id = id.getSub();
			base = id.getBase();
		}

		NamedElement namedEl = id.getBase();

		String tag = id.getTag();
		if (tag != null) {

			switch (tag) {
			case "_CLK":
				IdExpr clockId = new IdExpr(namedEl.getName() + clockIDSuffix);
				return clockId;
			default:
				throw new XagreeException("use of uknown tag: '" + tag + "' in expression: '" + aadlVar + tag + "'");
			}
		}

		Expr result;
		if (namedEl instanceof ConstStatement) {
			// evaluate the constant
			result = doSwitch(((ConstStatement) namedEl).getExpr());
		} else {
			jKindVar = jKindVar + namedEl.getName();
			result = new IdExpr(jKindVar);
		}

		// this is a record accessrecord
		while (id.getSub() != null) {
			id = id.getSub();
			namedEl = id.getBase();
			result = new RecordAccessExpr(result, namedEl.getName());
		}

		return result;
	}

	@Override
	public Expr caseAADLEnumerator(AADLEnumerator aadlEnum) {
		String typeStr = XagreeTypeUtils.getIDTypeStr(
				edu.clemson.xagree.validation.XagreeJavaValidator.getFinalNestId(aadlEnum.getEnumType()));
		return new IdExpr(typeStr.replace("__", "_") + "_" + aadlEnum.getValue());
	}

	@Override
	public Expr casePreExpr(PreExpr expr) {
		Expr res = doSwitch(expr.getExpr());

		return new UnaryExpr(UnaryOp.PRE, res);
	}

	@Override
	public Expr caseEventExpr(EventExpr expr) {

		IdExpr nestIdExpr = (IdExpr) doSwitch(expr.getId());
		String eventStr = nestIdExpr.id + eventSuffix;
		return new IdExpr(eventStr);
	}

	@Override
	public Expr caseLatchedExpr(LatchedExpr expr) {

		IdExpr nestIdExpr = (IdExpr) doSwitch(expr.getExpr());
		String latchedStr = nestIdExpr.id + XagreeInlineLatchedConnections.LATCHED_SUFFIX;
		return new IdExpr(latchedStr);
	}

	@Override
	public Expr casePrevExpr(PrevExpr expr) {
		Expr delayExpr = doSwitch(expr.getDelay());
		Expr initExpr = doSwitch(expr.getInit());

		Expr preExpr = new UnaryExpr(UnaryOp.PRE, delayExpr);

		Expr res = new BinaryExpr(initExpr, BinaryOp.ARROW, preExpr);

		return res;
	}

	@Override
	public Expr caseRealLitExpr(RealLitExpr expr) {
		return new RealExpr(BigDecimal.valueOf(Double.parseDouble(expr.getVal())));
	}

	@Override
	public Expr caseTimeExpr(TimeExpr time) {
		return XagreePatternTranslator.timeExpr;
	}

	@Override
	public Expr caseUnaryExpr(edu.clemson.xagree.xagree.UnaryExpr expr) {
		Expr subExpr = doSwitch(expr.getExpr());

		Expr res = null;
		switch (expr.getOp()) {
		case "-":
			res = new UnaryExpr(UnaryOp.NEGATIVE, subExpr);
			break;
		case "not":
			res = new UnaryExpr(UnaryOp.NOT, subExpr);
			break;
		default:
			assert (false);
		}

		return res;
	}

	private static void addToNodeList(Node node) {
		for (Node inList : globalNodes) {
			if (inList.id.equals(node.id)) {
				throw new XagreeException("AGREE AST generator tried adding multiple nodes of name '" + node.id + "'");
			}
		}
		globalNodes.add(node);
	}

	private static boolean nodeNameExists(String name) {
		for (Node inList : globalNodes) {
			if (inList.id.equals(name)) {
				return true;
			}
		}
		return false;
	}

}

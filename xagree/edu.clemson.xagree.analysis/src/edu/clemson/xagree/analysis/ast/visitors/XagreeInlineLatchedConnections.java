package edu.clemson.xagree.analysis.ast.visitors;

import static jkind.lustre.parsing.LustreParseUtil.equation;
import static jkind.lustre.parsing.LustreParseUtil.to;

import java.util.ArrayList;
import java.util.List;

import edu.clemson.xagree.analysis.XagreeException;
import edu.clemson.xagree.analysis.ast.XagreeASTBuilder;
import edu.clemson.xagree.analysis.ast.XagreeASTElement;
import edu.clemson.xagree.analysis.ast.XagreeConnection;
import edu.clemson.xagree.analysis.ast.XagreeEquation;
import edu.clemson.xagree.analysis.ast.XagreeNode;
import edu.clemson.xagree.analysis.ast.XagreeNodeBuilder;
import edu.clemson.xagree.analysis.ast.XagreeProgram;
import edu.clemson.xagree.analysis.ast.XagreeStatement;
import edu.clemson.xagree.analysis.ast.XagreeVar;
import jkind.lustre.Equation;
import jkind.lustre.Expr;
import jkind.lustre.IdExpr;
import jkind.lustre.visitors.ExprMapVisitor;

public class XagreeInlineLatchedConnections extends ExprMapVisitor implements XagreeASTVisitor<XagreeASTElement> {

    private final List<XagreeNode> nodes = new ArrayList<>();
    public static final String LATCHED_SUFFIX = "__LATCHED_";

    public static XagreeProgram translate(XagreeProgram program) {
        return (XagreeProgram) program.accept(new XagreeInlineLatchedConnections());
    }

    @Override
    public XagreeProgram visit(XagreeProgram program) {
        XagreeNode topNode = (XagreeNode) program.topNode.accept(this);
        return new XagreeProgram(nodes, program.globalLustreNodes, program.globalTypes, topNode);
    }

    @Override
    public XagreeNode visit(XagreeNode node) {
        XagreeNodeBuilder builder = new XagreeNodeBuilder(node);
        builder.clearSubNodes();

        for (XagreeNode subNode : node.subNodes) {
            XagreeNode newSubNode = (XagreeNode) subNode.accept(this);
            builder.addSubNode(newSubNode);
        }

        if (node.timing == XagreeNode.TimingModel.LATCHED) {
            for (XagreeNode subNode : builder.build().subNodes) {
                addLatchedInputEqs(builder, subNode);
            }
        }

        XagreeNode finalNode = builder.build();
        nodes.add(finalNode);
        return finalNode;
    }

    private void addLatchedInputEqs(XagreeNodeBuilder builder, XagreeNode subNode) {
        for (XagreeVar var : subNode.inputs) {
            XagreeVar latchVar = new XagreeVar(subNode.id + "__" + var.id + LATCHED_SUFFIX, var.type, var.reference,
                    var.compInst, var.featInst);
            builder.addLocal(latchVar);

            Expr clockExpr = new IdExpr(subNode.id + XagreeASTBuilder.clockIDSuffix);
            String sourceVarName = subNode.id + "__" + var.id;

            Equation latchEq = equation("latchVar = " + sourceVarName
                    + " -> if (pre (not clockVar)) and clockVar then " + sourceVarName + " else pre latchVar;",
                    to("latchVar", latchVar), to("clockVar", clockExpr));

            builder.addLocalEquation(new XagreeEquation(latchEq, null));
        }
    }

    @Override
    public XagreeVar visit(XagreeVar var) {
        throw new XagreeException("Should not visit here");
    }

    @Override
    public XagreeEquation visit(XagreeEquation eq) {
        throw new XagreeException("Should not visit here");
    }

    @Override
    public XagreeConnection visit(XagreeConnection conn) {
        throw new XagreeException("Should not visit here");
    }

	@Override
	public XagreeASTElement visit(XagreeStatement e) {
        throw new XagreeException("Should not visit here");
	}

}

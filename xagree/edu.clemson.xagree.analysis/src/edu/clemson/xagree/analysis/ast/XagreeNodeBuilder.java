package edu.clemson.xagree.analysis.ast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.instance.ComponentInstance;

import edu.clemson.xagree.analysis.ast.XagreeNode.TimingModel;
import jkind.lustre.Equation;
import jkind.lustre.Expr;
import jkind.lustre.IdExpr;

public class XagreeNodeBuilder {
    
    private String id;
    private List<XagreeVar> inputs = new ArrayList<>();
    private List<XagreeVar> outputs = new ArrayList<>();
    private List<XagreeVar> locals = new ArrayList<>();
    private List<XagreeEquation> equations = new ArrayList<>();
    private List<XagreeConnection> connections = new ArrayList<>();
    private List<XagreeNode> subNodes = new ArrayList<>();
    private List<XagreeStatement> assertions = new ArrayList<>();
    private List<XagreeStatement> assumptions = new ArrayList<>();
    private List<XagreeStatement> guarantees = new ArrayList<>();
    private List<XagreeStatement> lemmas = new ArrayList<>();
    private List<XagreeEquation> localEquations = new ArrayList<>();
    private List<XagreeStatement> patternProps = new ArrayList<>();
    private Expr clockConstraint;
    private Expr initialConstraint;
    private XagreeVar clockVar;
    private EObject reference;
    private TimingModel timing;
    private ComponentInstance compInst;
    private Set<XagreeVar> eventTimes = new HashSet<>();
    private Map<String, XagreeVar> timeOfMap = new HashMap<>();
    private Map<String, XagreeVar> timeRiseMap = new HashMap<>();
    private Map<String, XagreeVar> timeFallMap = new HashMap<>();
  
    public XagreeNodeBuilder(String id){
        this.id = id;
    }
    
    public XagreeNodeBuilder(XagreeNode node){
        this.id = node.id;
        this.inputs = new ArrayList<>(node.inputs);
        this.outputs = new ArrayList<>(node.outputs);
        this.locals = new ArrayList<>(node.locals);
        this.equations = new ArrayList<>(node.localEquations);
        this.connections = new ArrayList<>(node.connections);
        this.subNodes = new ArrayList<>(node.subNodes);
        this.assertions = new ArrayList<>(node.assertions);
        this.assumptions = new ArrayList<>(node.assumptions);
        this.guarantees = new ArrayList<>(node.guarantees);
        this.lemmas = new ArrayList<>(node.lemmas);
        this.localEquations = new ArrayList<>(node.localEquations);
        this.patternProps = new ArrayList<>(node.patternProps);
        this.eventTimes = new HashSet<>(node.eventTimes);
        this.timeOfMap = new HashMap<>(node.timeOfMap);
        this.timeRiseMap = new HashMap<>(node.timeRiseMap);
        this.timeFallMap = new HashMap<>(node.timeFallMap);
        this.clockConstraint = node.clockConstraint;
        this.initialConstraint = node.initialConstraint;
        this.clockVar = node.clockVar;
        this.reference = node.reference;
        this.timing = node.timing;
        this.compInst = node.compInst;
        
    }
    
    public void setId(String id){
        this.id = id;
    }
    
    public String getId(){
        return this.id;
    }
        
    public void addPatternProp(XagreeStatement statement){
        this.patternProps.add(statement);
    }
    
    public void addPatternProp(List<XagreeStatement> statements){
        this.patternProps.addAll(statements);
    }
    
    public void clearPatternProps(){
        this.patternProps.clear();
    }
    
    public void addEventTime(XagreeVar event){
        this.eventTimes.add(event);
    }
    
    public void addEventTime(Set<XagreeVar> events){
        this.eventTimes.addAll(events);
    }
    
    public void clearEventTimes(){
        this.eventTimes.clear();
    }
    
    public void addInput(XagreeVar var){
        this.inputs.add(var);
    }
    
    public void addInput(List<XagreeVar> vars){
        this.inputs.addAll(vars);
    }

    public void clearInputs() {
        this.inputs.clear();
    }
    
    public void addOutput(XagreeVar var){
        this.outputs.add(var);
    }
    
    public void addOutput(List<XagreeVar> vars){
        this.outputs.addAll(vars);
    }
    
    public void clearOutputs(){
        this.outputs.clear();
    }
    
    public void addLocal(XagreeVar var){
        this.locals.add(var);
    }
    
    public void addLocal(List<XagreeVar> var){
        this.locals.addAll(var);
    }
    
    public void clearLocals(){
        this.locals.clear();
    }
    
    public void addLocalEquation(XagreeEquation eq){
        this.equations.add(eq);
    }
    
    public void addLocalEquation(List<XagreeEquation> eq){
        this.equations.addAll(eq);
    }
    
    public void clearLocalEquations(){
        this.equations.clear();
    }
    
    public void addConnection(XagreeConnection conn){
        this.connections.add(conn);
    }
    
    public void addConnection(List<XagreeConnection> conn){
        this.connections.addAll(conn);
    }
    
    public void clearConnections(){
        this.connections.clear();
    }
    
    public void addSubNode(XagreeNode node){
        this.subNodes.add(node);
    }
    
    public void addSubNode(List<XagreeNode> node){
        this.subNodes.addAll(node);
    }
    
    public void clearSubNodes(){
        this.subNodes.clear();
    }
    
    public void addAssertion(XagreeStatement statement){
        this.assertions.add(statement);
    }
    
    public void addAssertion(List<XagreeStatement> statement){
        this.assertions.addAll(statement);
    }
    
    public void clearAssertions(){
        this.assertions.clear();
    }
    
    public void addAssumption(XagreeStatement statement){
        this.assumptions.add(statement);
    }
    
    public void addAssumption(List<XagreeStatement> statement){
        this.assumptions.addAll(statement);
    }
    
    public void clearAssumptions(){
        this.assumptions.clear();
    }
    
    public void addGuarantee(XagreeStatement statement){
        this.guarantees.add(statement);
    }
    
    public void addGuarantee(List<XagreeStatement> statement){
        this.guarantees.addAll(statement);
    }
    
    public void clearGuarantees(){
        this.guarantees.clear();
    }
    
    public void addLemma(XagreeStatement statement){
        this.lemmas.add(statement);
    }
    
    public void addLemma(List<XagreeStatement> statement){
        this.lemmas.addAll(statement);
    }
    
    public void addTimeOf(String name, XagreeVar var){
    	timeOfMap.put(name, var);
    }
    
    public void addTimeOf(Map<String, XagreeVar> map){
    	timeOfMap.putAll(map);
    }
    
    public void addTimeRise(String name, XagreeVar var){
    	timeRiseMap.put(name, var);
    }
    
    public void addTimeRise(Map<String, XagreeVar> map){
    	timeRiseMap.putAll(map);
    }
    
    public void addTimeFall(String name, XagreeVar var){
    	timeFallMap.put(name, var);
    }
    
    public void addTimeFall(Map<String, XagreeVar> map){
    	timeFallMap.putAll(map);
    }
    
    public void clearTimeOfs(){
    	timeOfMap.clear();
    }
    
    public void clearTimeRises(){
    	timeRiseMap.clear();
    }
    
    public void clearTimeFalls(){
    	timeFallMap.clear();
    }
    
    public void clearLemmas(){
        this.lemmas.clear();
    }
    
    public void setClockConstraint(Expr clock){
        this.clockConstraint = clock;
    }
    
    public void setInitialConstraint(Expr init){
        this.initialConstraint = init;
    }
    
    public void setClockVar(XagreeVar var){
        this.clockVar = var;
    }
    
    public void setReference(EObject ref){
        this.reference = ref;
    }
    
    public void setTiming(TimingModel time){
        this.timing = time;
    }

    public void setCompInst(ComponentInstance compInst) {
        this.compInst = compInst;
    }
    
    public XagreeNode build() {
        return new XagreeNode(id, inputs, outputs, locals, equations, connections, subNodes, assertions,
                assumptions, guarantees, lemmas, patternProps, clockConstraint, initialConstraint, clockVar, reference,
                timing, eventTimes, timeOfMap, timeRiseMap, timeFallMap, compInst);
    }

}

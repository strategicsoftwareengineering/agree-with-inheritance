package edu.clemson.xagree.analysis.ast;

import org.osate.aadl2.Connection;

import edu.clemson.xagree.analysis.ast.visitors.XagreeASTVisitor;
import jkind.lustre.Expr;

public class XagreeOverriddenConnection implements XagreeConnection{

	public final XagreeStatement statement;
    public final Connection aadlConn;
	
	public XagreeOverriddenConnection(XagreeStatement statement, Connection aadlConn){
		this.statement = statement;
		this.aadlConn = aadlConn;
	}
	
	
	@Override
	public <T> T accept(XagreeASTVisitor<T> visitor) {
		return visitor.visit(this);
	}

}

package edu.clemson.xagree.analysis.ast;

import jkind.lustre.Expr;

public class XagreeInterval {
    public final Expr low;
    public final Expr high;
    
    public XagreeInterval(Expr low, Expr high){
        this.low = low;
        this.high = high;
    }
}

/**
 * Copyright (c) 2016, Rockwell Collins.
 * 
 * Developed with the sponsorship of Defense Advanced Research Projects Agency
 * (DARPA).
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this data, including any software or models in source or binary form, as
 * well as any drawings, specifications, and documentation (collectively
 * "the Data"), to deal in the Data without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Data, and to permit persons to whom the
 * Data is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Data.
 *
 * THE DATA IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS, SPONSORS, DEVELOPERS, CONTRIBUTORS, OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE DATA OR THE
 * USE OR OTHER DEALINGS IN THE DATA.
 */

/*
 * MWW (9/17/2017)
 * NOTE: this pass only works correctly for *complete* traversals!
 * If you short-circuit a node traversal, the pass behaves incorrectly.
 *  
 */

package edu.clemson.xagree.analysis.ast.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;

import jkind.lustre.Expr;
import jkind.lustre.IdExpr;
import jkind.lustre.Node;
import jkind.lustre.Type;
import edu.clemson.xagree.analysis.XagreeException;
import edu.clemson.xagree.analysis.ast.XagreeAADLConnection;
import edu.clemson.xagree.analysis.ast.XagreeASTElement;
import edu.clemson.xagree.analysis.ast.XagreeConnection;
import edu.clemson.xagree.analysis.ast.XagreeEquation;
import edu.clemson.xagree.analysis.ast.XagreeNode;
import edu.clemson.xagree.analysis.ast.XagreeNodeBuilder;
import edu.clemson.xagree.analysis.ast.XagreeOverriddenConnection;
import edu.clemson.xagree.analysis.ast.XagreeProgram;
import edu.clemson.xagree.analysis.ast.XagreeStatement;
import edu.clemson.xagree.analysis.ast.XagreeVar;
import edu.clemson.xagree.analysis.ast.XagreeAADLConnection.ConnectionType;
import edu.clemson.xagree.analysis.ast.XagreeNode.TimingModel;

public class XagreeASTMapVisitor extends jkind.lustre.visitors.AstMapVisitor
		implements XagreeASTVisitor<XagreeASTElement> {

	protected jkind.lustre.visitors.TypeMapVisitor lustreTypeMapVisitor;

//	protected Map<AgreeNode, AgreeNode> visitedNodes;
	protected Map<ComponentInstance, XagreeNode> visitedNodes;

	public XagreeASTMapVisitor(jkind.lustre.visitors.TypeMapVisitor lustreTypeMapVisitor) {
		this.visitedNodes = new HashMap<>();
		this.lustreTypeMapVisitor = lustreTypeMapVisitor;
	}


	@Override
	public XagreeProgram visit(XagreeProgram e) {
		List<XagreeNode> agreeNodes = new ArrayList<>();
		for (XagreeNode node : e.agreeNodes) {
			XagreeNode visitedNode = visitedNodes.get(node.compInst);
			if (visitedNode == null) {
				visitedNode = this.visit(node);
			}
			agreeNodes.add(visitedNode);
		}

		List<Node> globalLustreNodes = new ArrayList<>();
		for (Node node : e.globalLustreNodes) {
			globalLustreNodes.add(this.visit(node));
		}

		List<jkind.lustre.Type> globalTypes = new ArrayList<>();
		for (Type ty : e.globalTypes) {
			globalTypes.add(ty.accept(lustreTypeMapVisitor));
		}

		XagreeNode topNode = this.visit(e.topNode);

		return new XagreeProgram(agreeNodes, globalLustreNodes, globalTypes, topNode);
	};

	@Override
	public XagreeConnection visit(XagreeConnection e) {
		if (e instanceof XagreeAADLConnection) {
			XagreeAADLConnection aadlConn = (XagreeAADLConnection)e;
			XagreeNode sourceNode = null; 
			if (aadlConn.sourceNode != null) { 
				sourceNode = visitedNodes.get(aadlConn.sourceNode.compInst);
			}
			XagreeNode destinationNode = null;
			if (aadlConn.destinationNode != null) {
				destinationNode = visitedNodes.get(aadlConn.destinationNode.compInst);
			}
			
			XagreeVar sourVar = visit(aadlConn.sourVar);
			XagreeVar destVar = visit(aadlConn.destVar);
			ConnectionType type = aadlConn.type;
			boolean latched = aadlConn.latched;
			boolean delayed = aadlConn.delayed;
			EObject reference = aadlConn.reference;

			return new XagreeAADLConnection(sourceNode, destinationNode, sourVar, destVar, type, latched,
					delayed, reference);
		}else if(e instanceof XagreeOverriddenConnection){
			XagreeOverriddenConnection overriddenCon = (XagreeOverriddenConnection)e;
			XagreeStatement statement = visit(overriddenCon.statement);
			return new XagreeOverriddenConnection(statement, overriddenCon.aadlConn);
		}
		throw new XagreeException("Unhandled Agree connection type "+e.getClass());
	}

	@Override
	public XagreeNode visit(XagreeNode e) {
		String id = e.id;

		List<XagreeVar> inputs = new ArrayList<>();
		for (XagreeVar input : e.inputs) {
			inputs.add(this.visit(input));
		}

		List<XagreeVar> outputs = new ArrayList<>();
		for (XagreeVar output : e.outputs) {
			outputs.add(this.visit(output));
		}

		List<XagreeVar> locals = new ArrayList<>();
		for (XagreeVar local : e.locals) {
			locals.add(this.visit(local));
		}

		// Note that nodes and connections contain cross-references to each
		// other. But, valid model structure requires that connections
		// refer only to features on the this node and the sub-nodes. Thus,
		// we may visit the sub-nodes first, and then use the result of that
		// in visiting the connections.
		//
		List<XagreeNode> subNodes = new ArrayList<>();
		for (XagreeNode subnode : e.subNodes) {
			subNodes.add(this.visit(subnode));
		}

		List<XagreeConnection> connections = new ArrayList<>();
		for (XagreeConnection conn : e.connections) {
			connections.add(this.visit(conn));
		}

		List<XagreeStatement> assertions = new ArrayList<>();
		for (XagreeStatement stmt : e.assertions) {
			assertions.add(this.visit(stmt));
		}

		List<XagreeStatement> assumptions = new ArrayList<>();
		for (XagreeStatement stmt : e.assumptions) {
			assumptions.add(this.visit(stmt));
		}

		List<XagreeStatement> guarantees = new ArrayList<>();
		for (XagreeStatement stmt : e.guarantees) {
			guarantees.add(this.visit(stmt));
		}

		List<XagreeStatement> lemmas = new ArrayList<>();
		for (XagreeStatement stmt : e.lemmas) {
			lemmas.add(this.visit(stmt));
		}
		
		List<XagreeStatement> patternProps = new ArrayList<>();
		for(XagreeStatement stmt : e.patternProps){
			patternProps.add(this.visit(stmt));
		}
		
		List<XagreeEquation> localEqs = new ArrayList<>();
		for(XagreeEquation eq : e.localEquations){
			localEqs.add(this.visit(eq));
		}

		Expr clockConstraint = e.clockConstraint.accept(this);
		Expr initialConstraint = e.initialConstraint.accept(this);
		XagreeVar clockVar = this.visit(e.clockVar);
		EObject reference = e.reference;
		TimingModel timing = e.timing;
		// ComponentInstance compinst = e.compInst;
		
		XagreeNodeBuilder builder = new XagreeNodeBuilder(id);
		builder.addInput(inputs);
		builder.addOutput(outputs);
		builder.addLocal(locals);
		builder.addConnection(connections);
		builder.addSubNode(subNodes);
		builder.addAssertion(assertions);
		builder.addAssumption(assumptions);
		builder.addGuarantee(guarantees);
		builder.addLemma(lemmas);
		builder.addLocalEquation(localEqs);
		builder.addPatternProp(patternProps);
		builder.setClockConstraint(clockConstraint);
		builder.setInitialConstraint(initialConstraint);
		builder.setClockVar(clockVar);
		builder.setReference(reference);
		builder.setTiming(timing);
		builder.setCompInst(e.compInst);
		builder.addTimeFall(e.timeFallMap);
		builder.addTimeRise(e.timeRiseMap);
		builder.addTimeOf(e.timeOfMap);

		XagreeNode result = builder.build();
		visitedNodes.put(e.compInst, result);

		return result;
	}

	@Override
	public XagreeStatement visit(XagreeStatement e) {
		String string = e.string;
		Expr expr = e.expr.accept(this);
		EObject reference = e.reference;

		return new XagreeStatement(string, expr, reference);
	}

	@Override
	public XagreeVar visit(XagreeVar e) {
		String name = e.id;
		Type type = e.type.accept(lustreTypeMapVisitor);
		EObject reference = e.reference;
		ComponentInstance compInst = e.compInst;
        FeatureInstance featInst = e.featInst;

        return new XagreeVar(name, type, reference, compInst, featInst);
	}

	/*
	// MWW: WTF?
	@Override
	public AgreeEquation visit(AgreeEquation agreeEquation) {
		
		// TODO Auto-generated method stub
		return null;
	}
	*/
	
	@Override
	public XagreeEquation visit(XagreeEquation agreeEquation) {
		EObject reference = agreeEquation.reference;
		List<IdExpr> lhs = new ArrayList<>(); 
		for (IdExpr e: agreeEquation.lhs) {
			lhs.add((IdExpr)e.accept(this)); 
		}
		Expr rhs = agreeEquation.expr.accept(this);
		return new XagreeEquation(lhs, rhs, reference);
	}
}

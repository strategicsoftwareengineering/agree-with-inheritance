package edu.clemson.xagree.analysis.ast;

import java.util.List;

import edu.clemson.xagree.analysis.ast.visitors.XagreeASTVisitor;
import jkind.lustre.Node;
import jkind.lustre.Type;

public class XagreeProgram implements XagreeASTElement {
	public final List<XagreeNode> agreeNodes;
	public final List<Node> globalLustreNodes;
	public final List<Type> globalTypes;
	public final XagreeNode topNode;
	public boolean containsRealTimePatterns;

	public XagreeProgram(List<XagreeNode> agreeNodes, List<Node> globalLustreNodes, List<Type> globalTypes,
			XagreeNode topNode) {
		this.agreeNodes = jkind.util.Util.safeList(agreeNodes);
		this.globalLustreNodes = jkind.util.Util.safeList(globalLustreNodes);
		this.globalTypes = jkind.util.Util.safeList(globalTypes);
		this.topNode = topNode;
		this.containsRealTimePatterns = false;
	}
	
	public XagreeProgram(List<XagreeNode> agreeNodes, List<Node> globalLustreNodes, List<Type> globalTypes,
			XagreeNode topNode, boolean containsRealTimePatterns) {
		this.agreeNodes = jkind.util.Util.safeList(agreeNodes);
		this.globalLustreNodes = jkind.util.Util.safeList(globalLustreNodes);
		this.globalTypes = jkind.util.Util.safeList(globalTypes);
		this.topNode = topNode;
		this.containsRealTimePatterns = containsRealTimePatterns;
	}
	
	@Override
	public <T> T accept(XagreeASTVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public void containsRealTimePatterns(boolean containsRTPatterns){
		containsRealTimePatterns = containsRTPatterns;
	}

}

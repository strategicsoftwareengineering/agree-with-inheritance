package edu.clemson.xagree.analysis.lustre.visitors;

import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.DataPort;
import org.osate.aadl2.EventDataPort;
import org.osate.aadl2.FeatureGroup;
import org.osate.aadl2.SystemImplementation;

import edu.clemson.xagree.xagree.Arg;
import edu.clemson.xagree.xagree.AssertStatement;
import edu.clemson.xagree.xagree.AssumeStatement;
import edu.clemson.xagree.xagree.GuaranteeStatement;
import edu.clemson.xagree.xagree.LemmaStatement;
import edu.clemson.xagree.xagree.PropertyStatement;

import edu.clemson.xagree.analysis.XagreeException;
import edu.clemson.xagree.analysis.XagreeLayout;
import edu.clemson.xagree.analysis.XagreeRenaming;
import edu.clemson.xagree.analysis.XagreeLayout.SigType;
import edu.clemson.xagree.analysis.ast.XagreeASTBuilder;
import edu.clemson.xagree.analysis.ast.XagreeStatement;
import edu.clemson.xagree.analysis.ast.XagreeVar;
import edu.clemson.xagree.analysis.ast.visitors.XagreeInlineLatchedConnections;
import edu.clemson.xagree.analysis.translation.LustreAstBuilder;
import jkind.lustre.Node;
import jkind.lustre.Program;
import jkind.lustre.VarDecl;
import jkind.lustre.visitors.AstIterVisitor;

public class RenamingVisitor extends AstIterVisitor{

    private final XagreeRenaming renaming;
    private final XagreeLayout layout;
    private final Program program;
    private boolean isMainNode;
    private String nodeName;
    
    public static void addRenamings(Program program, XagreeRenaming renaming, XagreeLayout layout){
        RenamingVisitor visitor = new RenamingVisitor(program, renaming, layout);
        visitor.visit(program);
    }
    
    private RenamingVisitor(Program program, XagreeRenaming renaming, XagreeLayout layout){
        this.renaming = renaming;
        this.layout = layout;
        this.program = program;
    }

    @Override
    public Void visit(Program program) {
        super.visit(program);
		for (jkind.lustre.TypeDef typeDef : program.types) {
			if (typeDef.type instanceof jkind.lustre.EnumType) {
				jkind.lustre.EnumType enumType = (jkind.lustre.EnumType) typeDef.type;
				for (String enumerator : enumType.values) {
					renaming.addExplicitRename(enumerator, enumerator);
				}
			}
		}
        return null;
    }

    @Override
    public Void visit(Node node){
        isMainNode = node.id.equals(program.main);
        nodeName = node.id;
        visitVarDecls(node.inputs);
        visitVarDecls(node.outputs);
        visitVarDecls(node.locals);
        return null;
    }

    @Override
    public Void visit(VarDecl e) {
        if (e instanceof XagreeVar) {
            XagreeVar var = (XagreeVar) e;
            String category = getCategory(var);
            String refStr = getReferenceStr(var);

            if (isMainNode && var.reference != null) {
                // TODO: the means of detecting whether this is a consistency analysis is a hack. Fix it.
                if ((var.reference instanceof AssumeStatement
                        || (nodeName.contains("consistency") && var.reference instanceof GuaranteeStatement))
                        && category != null && category.equals("")) {
                    renaming.addSupportRename(var.id, var.id);
                    renaming.addSupportRefString(var.id, refStr);
                    renaming.getRefMap().put(refStr, var.reference);
                } else {
                    renaming.addExplicitRename(var.id, refStr);
                    renaming.addToRefMap(var.id, var.reference);
                }
            } else if (var.reference instanceof GuaranteeStatement) {
                renaming.addSupportRename(nodeName + "." + var.id, category + "." + var.id);
                renaming.addSupportRefString(nodeName + "." + var.id, refStr);
                renaming.getRefMap().put(refStr, var.reference);
            } else {
                return null;
            }

            if (category != null && !layout.getCategories().contains(category)) {
                layout.addCategory(category);
            }
            layout.addElement(category, refStr, SigType.INPUT);

        }
        return null;
    }

    private String getReferenceStr(XagreeVar var) {

        String prefix = getCategory(var);
        if (prefix == null) {
            return null;
        }
        if (var.id.endsWith(XagreeASTBuilder.clockIDSuffix)) {
            return null;
        }

        String seperator = (prefix == "" ? "" : ".");
        EObject reference = var.reference;
        String suffix = "";
        
        if(var.id.endsWith(XagreeASTBuilder.eventSuffix + XagreeInlineLatchedConnections.LATCHED_SUFFIX)){
        	suffix = "._EVENT_._LATCHED_";
        }else if(var.id.endsWith(XagreeASTBuilder.eventSuffix)){
        	suffix = "._EVENT_";
        }else if(var.id.endsWith(XagreeInlineLatchedConnections.LATCHED_SUFFIX)){
        	suffix = "._LATCHED_";
        }

		if (reference instanceof GuaranteeStatement) {
			return ((GuaranteeStatement) reference).getStr();
		} else if (reference instanceof AssumeStatement) {
			return prefix + " assume: " + ((AssumeStatement) reference).getStr();
		} else if (reference instanceof LemmaStatement) {
			return prefix + " lemma: " + ((LemmaStatement) reference).getStr();
		} else if (reference instanceof AssertStatement) {
			throw new XagreeException("We really didn't expect to see an assert statement here");
		} else if (reference instanceof Arg) {
			return prefix + seperator + ((Arg) reference).getName() + suffix;
		} else if (reference instanceof DataPort) {
			return prefix + seperator + ((DataPort) reference).getName() + suffix;
		} else if (reference instanceof EventDataPort) {
			return prefix + seperator + ((EventDataPort) reference).getName() + suffix;
		} else if (reference instanceof FeatureGroup) {
			String featName = ((FeatureGroup) reference).getName();
			String varName = var.toString();
			featName = varName.substring(varName.indexOf(featName)).replace("__", ".");
			return prefix + seperator + featName;
		} else if (reference instanceof PropertyStatement) {
			return prefix + seperator + ((PropertyStatement) reference).getName();
		} else if (reference instanceof ComponentType || reference instanceof ComponentImplementation
				|| reference instanceof SystemImplementation) {
        	if(var.id.equals(LustreAstBuilder.assumeHistSufix)){
        		return "Subcomponent Assumptions";
        	}
            return "Result";
        } else if(reference instanceof XagreeStatement){
            return prefix + reference.toString();
        }
        throw new XagreeException("Unhandled reference type: '" + reference.getClass().getName() + "'");
    }
    
    public static String getCategory(XagreeVar var) {
        if (var.compInst == null || var.reference == null) {
            return null;
        }
        return LustreAstBuilder.getRelativeLocation(var.compInst.getInstanceObjectPath());
    }
    
}

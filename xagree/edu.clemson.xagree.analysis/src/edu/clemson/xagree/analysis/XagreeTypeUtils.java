package edu.clemson.xagree.analysis;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.DataPort;
import org.osate.aadl2.Feature;
import org.osate.aadl2.FeatureGroup;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.StringLiteral;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;

import edu.clemson.xagree.xagree.Arg;
import edu.clemson.xagree.xagree.EnumStatement;
import edu.clemson.xagree.xagree.NamedID;
import edu.clemson.xagree.xagree.NestedDotID;
import edu.clemson.xagree.xagree.PrimType;
import edu.clemson.xagree.xagree.RecordDefExpr;
import edu.clemson.xagree.xagree.RecordType;
import edu.clemson.xagree.xagree.ThisExpr;

import edu.clemson.xagree.XagreeAADLEnumerationUtils;
import edu.clemson.xagree.analysis.ast.XagreeASTBuilder;
import jkind.lustre.BoolExpr;
import jkind.lustre.EnumType;
import jkind.lustre.Expr;
import jkind.lustre.IntExpr;
import jkind.lustre.NamedType;
import jkind.lustre.RealExpr;
import jkind.lustre.RecordExpr;
import jkind.lustre.Type;
import jkind.lustre.VarDecl;

public class XagreeTypeUtils {

	private static final String dotChar = "__";
	private static final Expr initBool = new BoolExpr(false);
	private static final Expr initReal = new RealExpr(BigDecimal.ZERO);
	private static final Expr initInt = new IntExpr(BigInteger.ZERO);

	public static String getTypeName(Type type) {
		if (type instanceof PrimType) {
			return ((PrimType) type).getString();
		} else {
			return getIDTypeStr((XagreeUtils.getFinalNestId(((RecordType) type).getRecord())));
		}
	}

	public static String getTypeName(edu.clemson.xagree.xagree.Type type, Map<NamedElement, String> typeMap,
			Set<Type> typeExpressions) {
		if (type instanceof PrimType) {
			return ((PrimType) type).getString();
		} else {
			return getTypeName(((RecordType) type).getRecord(), typeMap, typeExpressions);
		}
	}

	public static String getTypeName(NestedDotID recId, Map<NamedElement, String> typeMap, Set<Type> typeExpressions) {
		NamedElement finalId = XagreeUtils.getFinalNestId(recId);
		return getTypeName(finalId, typeMap, typeExpressions);
	}

	private static String getTypeName(NamedElement finalId, Map<NamedElement, String> typeMap,
			Set<Type> typeExpressions) {

		if (finalId == null) {
			return null;
		}

		if (typeMap.containsKey(finalId)) {
			return typeMap.get(finalId);
		}
		recordType(finalId, typeMap, typeExpressions);
		return typeMap.get(finalId);
	}

	public static Type getType(NamedElement finalId, Map<NamedElement, String> typeMap, Set<Type> typeExpressions) {
		String name = getTypeName(finalId, typeMap, typeExpressions);
		if (name == null) {
			return null;
		}
		for (Type type : typeExpressions) {
			if (type.toString().equals(name)) {
				return type;
			}
		}
		return new NamedType(name);
	}

	private static void recordType(NamedElement el, Map<NamedElement, String> typeMap, Set<Type> typeExpressions) {
		Map<String, Type> subTypeMap = new HashMap<String, Type>();
		if (el instanceof ComponentImplementation) {
			ComponentImplementation compImpl = (ComponentImplementation) el;
			String typeStr = null;
			Type subType = null;
			if (compImpl.getAllSubcomponents().size() == 0) {
				typeStr = getIDTypeStr(compImpl.getType());
				typeMap.put(el, typeStr);
				return;
			}
			for (Subcomponent subComp : compImpl.getAllSubcomponents()) {
				ComponentImplementation subCompImpl = subComp.getComponentImplementation();
				if (subCompImpl == null) {
					ComponentType subCompType = subComp.getComponentType();
					subType = getType(subCompType, typeMap, typeExpressions);
				} else {
					subType = getType(subCompImpl, typeMap, typeExpressions);
				}
				if (subType != null) {
					subTypeMap.put(subComp.getName(), subType);
				}
			}
		} else if (el instanceof RecordDefExpr) {
			RecordDefExpr agreeRecDef = (RecordDefExpr) el;
			for (Arg arg : agreeRecDef.getArgs()) {

				edu.clemson.xagree.xagree.Type argType = arg.getType();
				String typeStr = null;
				Type subType = null;
				if (argType instanceof PrimType) {
					typeStr = ((PrimType) argType).getString();
				} else {
					NestedDotID nestId = ((RecordType) argType).getRecord();
					NamedElement namedEl = XagreeUtils.getFinalNestId(nestId);
					subType = getType(namedEl, typeMap, typeExpressions);
				}
				if (typeStr != null) {
					subType = getNamedType(typeStr);
				}
				subTypeMap.put(arg.getName(), subType);
			}

		} else if (el instanceof EnumStatement) {
			List<String> vals = new ArrayList<>();
			EnumStatement enumStatement = (EnumStatement) el;
			for (NamedID id : enumStatement.getEnums()) {
				vals.add(id.getName());
			}
			String typeStr = getIDTypeStr(enumStatement);
			typeMap.put(el, typeStr);
			typeExpressions.add(new EnumType(typeStr, vals));
			return;
		} else if (XagreeAADLEnumerationUtils.isAADLEnumeration(el)) {
			String typeStr = getIDTypeStr(el);
			List<String> vals = XagreeAADLEnumerationUtils.getEnumerators((ComponentClassifier) el).stream()
					.map(val -> typeStr.replace("__", "_") + "_" + ((StringLiteral) val).getValue())
					.collect(Collectors.toList());
			typeMap.put(el, typeStr);
			typeExpressions.add(new EnumType(typeStr, vals));
		} else if (el instanceof ComponentType) {
			String typeStr = getIDTypeStr(el);
			typeMap.put(el, typeStr);
			return;
		}

		if (subTypeMap.size() > 0) {
			String typeStr = getIDTypeStr(el);
			typeMap.put(el, typeStr);
			jkind.lustre.RecordType lustreRecord = new jkind.lustre.RecordType(typeStr, subTypeMap);
			typeExpressions.add(lustreRecord);
		}

	}

	public static Expr getInitialType(String typeStr, Set<jkind.lustre.RecordType> typeExpressions) {

		switch (typeStr) {
		case "bool":
			return initBool;
		case "real":
			return initReal;
		case "int":
			return initInt;
		default:
		}

		boolean foundType = false;
		Map<String, Expr> fieldExprs = new HashMap<>();
		for (jkind.lustre.RecordType type : typeExpressions) {
			if (type.id.equals(typeStr)) {
				foundType = true;
				for (Entry<String, Type> field : type.fields.entrySet()) {
					Type fieldType = field.getValue();
					if (!(fieldType instanceof NamedType) && !(fieldType instanceof RecordType)) {
						throw new XagreeException("Unhandled type: '" + fieldType.getClass().getTypeName() + "'");
					}
					Expr fieldExpr = getInitialType(fieldType.toString(), typeExpressions);
					fieldExprs.put(field.getKey(), fieldExpr);
				}
			}
		}
		if (!foundType) {
			throw new XagreeException("Could not find type: '" + typeStr + "'");
		}

		return new RecordExpr(typeStr, fieldExprs);
	}

	public static String getIDTypeStr(NamedElement record) {
		String typeStr = null;
		EObject container = record.eContainer();

		if (record instanceof ComponentType && !XagreeAADLEnumerationUtils.isAADLEnumeration(record)) {
			ComponentType type = (ComponentType) record;
			do {
				String name = type.getQualifiedName();
				switch (name) {
				case "Base_Types::Boolean":
				case "Base_Types::Integer":
				case "Base_Types::Unsigned":
				case "Base_Types::Unsigned_64":
				case "Base_Types::Unsigned_32":
				case "Base_Types::Unsigned_16":
				case "Base_Types::Unsigned_8":
				case "Base_Types::Integer_64":
				case "Base_Types::Integer_32":
				case "Base_Types::Integer_16":
				case "Base_Types::Integer_8":
				case "Base_Types::Float":
				case "Base_Types::Float_32":
				case "Base_Types::Float_64":
					return name.replace("::", "__");
				}
				type = type.getExtended();

			} while (type != null);
			XagreeLogger.logWarning("Reference to component type '" + record.getName()
					+ "' is not among the types reasoned about by AGREE");
			return null;
		} else if (record instanceof ComponentImplementation) {
			typeStr = record.getName();
		} else {
			while (!(container instanceof ComponentClassifier) && !(container instanceof AadlPackage)) {
				container = container.eContainer();
			}
			if (container instanceof ComponentClassifier) {
				ComponentClassifier compClass = (ComponentClassifier) container;
				typeStr = compClass.getName() + XagreeASTBuilder.dotChar + record.getName();
			} else {
				typeStr = record.getName();
			}
		}
		// get the name of the containing package
		while (!(container instanceof AadlPackage)) {
			container = container.eContainer();
		}
		typeStr = ((AadlPackage) container).getName() + XagreeASTBuilder.dotChar + typeStr;
		typeStr = typeStr.replace(".", "__");
		typeStr = typeStr.replace("::", "____");

		return typeStr;
	}

	public static NamedType getNamedType(String name) {
		switch (name) {
		case "bool":
			return NamedType.BOOL;
		case "real":
			return NamedType.REAL;
		case "int":
			return NamedType.INT;
		default:
			return new NamedType(name);
		}
	}

	public static String getNodeName(NamedElement nodeDef) {
		EObject container = nodeDef.eContainer();
		List<String> segments = new ArrayList<>();

		segments.add(nodeDef.getName());
		while (container != null) {
			if (container instanceof ComponentClassifier || container instanceof AadlPackage) {
				segments.add(0, ((NamedElement) container).getName().replace(".", XagreeASTBuilder.dotChar));
			}
			container = container.eContainer();
		}

		return String.join(XagreeASTBuilder.dotChar, segments);
	}

	public static String getObjectLocationPrefix(EObject obj) {
		String objPrefix = "";
		EObject container = obj.eContainer();

		while (!(container instanceof AadlPackage)) {
			if (container instanceof ComponentClassifier) {
				objPrefix = ((ComponentClassifier) container).getName();
				objPrefix = dotChar + objPrefix;
				objPrefix = objPrefix.replace(".", dotChar);
			}
			container = container.eContainer();
		}
		objPrefix = ((AadlPackage) container).getName() + objPrefix + dotChar;

		return objPrefix;
	}

	public static List<VarDecl> argsToVarDeclList(EList<Arg> args, Map<NamedElement, String> typeMap,
			Set<Type> typeExpressions) {
		List<VarDecl> varList = new ArrayList<VarDecl>();
		for (Arg arg : args) {
			Type type = getNamedType(getTypeName(arg.getType(), typeMap, typeExpressions));
			VarDecl varDecl = new VarDecl(arg.getName(), type);
			varList.add(varDecl);
		}

		return varList;
	}

	public static NamedElement namedElFromId(EObject obj, ComponentInstance compInst) {
		if (obj instanceof NestedDotID) {
			return XagreeUtils.getFinalNestId((NestedDotID) obj);
		} else {
			assert (obj instanceof ThisExpr);

			ThisExpr thisExpr = (ThisExpr) obj;

			NestedDotID nestId = thisExpr.getSubThis();

			while (nestId != null) {
				NamedElement base = nestId.getBase();

				if (base instanceof Subcomponent) {
					compInst = compInst.findSubcomponentInstance((Subcomponent) base);
					nestId = nestId.getSub();
				} else if (base instanceof FeatureGroup) {
					assert (base instanceof FeatureGroup);
					FeatureInstance featInst = compInst.findFeatureInstance((Feature) base);

					while (nestId.getSub() != null) {
						nestId = nestId.getSub();
						assert (nestId.getBase() instanceof Feature);
						Feature subFeat = (Feature) nestId.getBase();
						FeatureInstance eqFeatInst = null;
						for (FeatureInstance subFeatInst : featInst.getFeatureInstances()) {
							if (subFeatInst.getFeature().equals(subFeat)) {
								eqFeatInst = subFeatInst;
								break;
							}
						}
						featInst = eqFeatInst;
					}

					return featInst;
				} else {
					assert (base instanceof DataPort);
					return compInst.findFeatureInstance((DataPort) base);
				}

			}
			return compInst;
		}
	}

	public static void addToRenamingAll(Map<String, String> renamings, Map<String, String> varRenaming) {
		for (Entry<String, String> entry : renamings.entrySet()) {
			addToRenaming(entry.getKey(), entry.getValue(), varRenaming);
		}
	}

	public static void addToRenaming(String key, String value, Map<String, String> varRenaming) {
		if (varRenaming.containsValue(value)) {
			// TODO: this could be done another way if its to slow
			for (Entry<String, String> entry : varRenaming.entrySet()) {
				if (entry.getValue().equals(value) && !entry.getKey().equals(key)) {
					throw new XagreeException("There is a name collision with a multiple variables"
							+ ", assumptions, or guarantees named '" + value + "' which will cause "
							+ "issues with reporting results");
				}
			}
		}
		varRenaming.put(key, value);
	}

}

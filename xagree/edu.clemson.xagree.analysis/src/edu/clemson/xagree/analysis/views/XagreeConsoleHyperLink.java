package edu.clemson.xagree.analysis.views;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ui.console.IHyperlink;
import org.eclipse.xtext.ui.editor.GlobalURIEditorOpener;

import edu.clemson.xagree.analysis.XagreeUtils;

public class XagreeConsoleHyperLink implements IHyperlink {
    private static GlobalURIEditorOpener globalURIEditorOpener = XagreeUtils.getGlobalURIEditorOpener();
    private EObject ref;

    public XagreeConsoleHyperLink(EObject ref) {
        this.ref = ref;
    }

    @Override
    public void linkEntered() {
    }

    @Override
    public void linkExited() {
    }

    @Override
    public void linkActivated() {
        globalURIEditorOpener.open(EcoreUtil.getURI(ref), true);
    }
}

package edu.clemson.xagree.analysis.views;

import jkind.api.results.AnalysisResult;
import jkind.api.ui.results.AnalysisResultTree;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

public class XagreeResultsView extends ViewPart {
    public static final String ID = "edu.clemson.xagree.analysis.views.xagreeResultsView";

    private AnalysisResultTree tree;
    private XagreeMenuListener menuListener;

    @Override
    public void createPartControl(Composite parent) {
        tree = new AnalysisResultTree(parent);
        tree.getViewer().setAutoExpandLevel(2);

        menuListener = new XagreeMenuListener(getViewSite().getWorkbenchWindow(), tree);
        MenuManager manager = new MenuManager();
        manager.setRemoveAllWhenShown(true);
        manager.addMenuListener(menuListener);
        tree.getControl().setMenu(manager.createContextMenu(tree.getViewer().getTree()));
    }

    @Override
    public void setFocus() {
        tree.getControl().setFocus();
    }

    public void setInput(AnalysisResult result, XagreeResultsLinker linker) {
        tree.setInput(result);
        menuListener.setLinker(linker);
    }
}

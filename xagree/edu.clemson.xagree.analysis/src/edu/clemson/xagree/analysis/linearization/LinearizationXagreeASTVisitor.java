/**
 * Copyright (c) 2016, Rockwell Collins.
 * 
 * Developed with the sponsorship of Defense Advanced Research Projects Agency
 * (DARPA).
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this data, including any software or models in source or binary form, as
 * well as any drawings, specifications, and documentation (collectively
 * "the Data"), to deal in the Data without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Data, and to permit persons to whom the
 * Data is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Data.
 *
 * THE DATA IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS, SPONSORS, DEVELOPERS, CONTRIBUTORS, OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE DATA OR THE
 * USE OR OTHER DEALINGS IN THE DATA.
 */

package edu.clemson.xagree.analysis.linearization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.instance.ComponentInstance;

import jkind.lustre.Expr;
import jkind.lustre.NodeCallExpr;
import edu.clemson.xagree.analysis.ast.XagreeConnection;
import edu.clemson.xagree.analysis.ast.XagreeNode;
import edu.clemson.xagree.analysis.ast.XagreeNodeBuilder;
import edu.clemson.xagree.analysis.ast.XagreeStatement;
import edu.clemson.xagree.analysis.ast.XagreeVar;
import edu.clemson.xagree.analysis.ast.XagreeNode.TimingModel;
import edu.clemson.xagree.analysis.ast.visitors.XagreeASTMapVisitor;

public class LinearizationXagreeASTVisitor extends XagreeASTMapVisitor {

	private static final String ephemeralBaseName = "__EPHEMERAL__";

	private int ephemeralIndex = 0;

	private Map<String, String> linearizationMap;

	private class LinearizationContext {
		public List<XagreeVar> addedVars = new ArrayList<>();
		public List<NodeCallExpr> liftedConstraintExprs = new ArrayList<>();
		public final ComponentInstance componentInstance;

		public LinearizationContext(ComponentInstance componentInstance) {
			this.componentInstance = componentInstance;
		}
	}

	private Stack<LinearizationContext> contextStack = new Stack<>();

	public LinearizationXagreeASTVisitor(Map<String, String> linearizationMap) {
		super(new jkind.lustre.visitors.TypeMapVisitor());
		this.linearizationMap = linearizationMap;
	}

	private class LinearizationResult {
		public final List<XagreeVar> addedLocals;
		public final List<XagreeStatement> liftedConstraints;
		public final XagreeStatement linearizedStatement;

		public LinearizationResult(List<XagreeVar> addedLocals, List<XagreeStatement> liftedConstraints,
				XagreeStatement linearizedStatement) {
			this.addedLocals = Collections.unmodifiableList(addedLocals);
			this.liftedConstraints = Collections.unmodifiableList(liftedConstraints);
			this.linearizedStatement = linearizedStatement;
		}
	}

	private LinearizationResult linearizeStatement(XagreeNode parent, XagreeStatement stmt,
			jkind.lustre.BinaryOp connective) {
		contextStack.push(new LinearizationContext(parent.compInst));
		List<XagreeVar> addedLocals = new ArrayList<>();
		List<XagreeStatement> addedAssertions = new ArrayList<>();

		XagreeStatement linearizedStatement = this.visit(stmt);

		// Incorporate the additions resulting from the linearization
		LinearizationContext ctx = contextStack.peek();
		addedLocals.addAll(contextStack.peek().addedVars);
		for (int index = 0; index < ctx.liftedConstraintExprs.size(); ++index) {
			XagreeVar ephemeral = ctx.addedVars.get(index);
			String statementName = stmt.string + " (linearize constraint " + ephemeral.id + ")";
			addedAssertions.add(new XagreeStatement(statementName, ctx.liftedConstraintExprs.get(index), null));
		}

		contextStack.pop();

		return new LinearizationResult(addedLocals, addedAssertions, linearizedStatement);
	}

	@Override
	public XagreeNode visit(XagreeNode e) {
		List<XagreeVar> inputs = new ArrayList<>();
		for (XagreeVar input : e.inputs) {
			inputs.add(this.visit(input));
		}

		List<XagreeVar> outputs = new ArrayList<>();
		for (XagreeVar output : e.outputs) {
			outputs.add(this.visit(output));
		}

		List<XagreeVar> locals = new ArrayList<>();
		for (XagreeVar local : e.locals) {
			locals.add(this.visit(local));
		}

		// Note that nodes and connections contain cross-references to each
		// other. But, valid model structure requires that connections
		// refer only to features on the this node and the sub-nodes. Thus,
		// we may visit the sub-nodes first, and then use the result of that
		// in visiting the connections.
		//
		List<XagreeNode> subNodes = new ArrayList<>();
		for (XagreeNode subnode : e.subNodes) {
			subNodes.add(this.visit(subnode));
		}

		List<XagreeConnection> connections = new ArrayList<>();
		for (XagreeConnection conn : e.connections) {
			connections.add(this.visit(conn));
		}

		List<XagreeStatement> assertions = new ArrayList<>();
		for (XagreeStatement stmt : e.assertions) {
			if (stmt.expr != null) {
				LinearizationResult linResult = linearizeStatement(e, stmt, jkind.lustre.BinaryOp.AND);
				outputs.addAll(linResult.addedLocals);
				assertions.addAll(linResult.liftedConstraints);
				assertions.add(linResult.linearizedStatement);
			} else {
				assertions.add(stmt);
			}
		}

		List<XagreeStatement> assumptions = new ArrayList<>();
		for (XagreeStatement stmt : e.assumptions) {
			if (stmt.expr != null) {
				LinearizationResult linResult = linearizeStatement(e, stmt, jkind.lustre.BinaryOp.AND);
				outputs.addAll(linResult.addedLocals);
				assertions.addAll(linResult.liftedConstraints);
				assumptions.add(linResult.linearizedStatement);
			} else {
				assumptions.add(stmt);
			}
		}

		List<XagreeStatement> guarantees = new ArrayList<>();
		for (XagreeStatement stmt : e.guarantees) {
			if (stmt.expr != null) {
				LinearizationResult linResult = linearizeStatement(e, stmt, jkind.lustre.BinaryOp.IMPLIES);
				outputs.addAll(linResult.addedLocals);
				guarantees.addAll(linResult.liftedConstraints);
				guarantees.add(linResult.linearizedStatement);
			} else {
				guarantees.add(stmt);
			}
		}

		List<XagreeStatement> lemmas = new ArrayList<>();
		for (XagreeStatement stmt : e.lemmas) {
			if (stmt.expr != null) {
				LinearizationResult linResult = linearizeStatement(e, stmt, jkind.lustre.BinaryOp.IMPLIES);
				outputs.addAll(linResult.addedLocals);
				assertions.addAll(linResult.liftedConstraints);
				lemmas.add(linResult.linearizedStatement);
			} else {
				lemmas.add(stmt);
			}
		}

		Expr clockConstraint = e.clockConstraint.accept(this);
		Expr initialConstraint = e.initialConstraint.accept(this);
		XagreeVar clockVar = this.visit(e.clockVar);

		XagreeNodeBuilder builder = new XagreeNodeBuilder(e);
		builder.clearInputs();
		builder.addInput(inputs);
		builder.clearOutputs();
		builder.addOutput(outputs);
		builder.clearLocals();
		builder.addLocal(locals);
		builder.clearConnections();
		builder.addConnection(connections);
		builder.clearSubNodes();
		builder.addSubNode(subNodes);
		builder.clearAssertions();
		builder.addAssertion(assertions);
		builder.clearAssumptions();
		builder.addAssumption(assumptions);
		builder.clearGuarantees();
		builder.addGuarantee(guarantees);
		builder.clearLemmas();
		builder.addLemma(lemmas);
		builder.setClockConstraint(clockConstraint);
		builder.setInitialConstraint(initialConstraint);
		builder.setClockVar(clockVar);
		
		XagreeNode result = builder.build();
		visitedNodes.put(e.compInst, result);

		return result;
	}

	@Override
	public Expr visit(NodeCallExpr e) {
		// If the call is to a linearization, generate ephemeral
		// var and constraint then substitute an id expr to the ephemeral
		// var for the fn call.
		String linCallDef = linearizationMap.get(e.node);

		if (linCallDef != null) {
			XagreeVar ephemeral = new XagreeVar(ephemeralBaseName + Integer.toString(ephemeralIndex),
					jkind.lustre.NamedType.REAL, null, contextStack.peek().componentInstance, null);
			++ephemeralIndex;

			List<Expr> args = visitExprs(e.args);
			args.add(new jkind.lustre.IdExpr(ephemeral.id));
			jkind.lustre.NodeCallExpr constraintExpr = new jkind.lustre.NodeCallExpr(linCallDef, args);

			contextStack.peek().addedVars.add(ephemeral);
			contextStack.peek().liftedConstraintExprs.add(constraintExpr);

			return new jkind.lustre.IdExpr(ephemeral.id);
		}

		return new NodeCallExpr(e.location, e.node, visitExprs(e.args));
	}

}

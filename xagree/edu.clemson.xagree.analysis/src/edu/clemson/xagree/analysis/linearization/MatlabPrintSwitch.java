/**
 * Copyright (c) 2016, Rockwell Collins.
 * 
 * Developed with the sponsorship of Defense Advanced Research Projects Agency
 * (DARPA).
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this data, including any software or models in source or binary form, as
 * well as any drawings, specifications, and documentation (collectively
 * "the Data"), to deal in the Data without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Data, and to permit persons to whom the
 * Data is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Data.
 *
 * THE DATA IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS, SPONSORS, DEVELOPERS, CONTRIBUTORS, OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE DATA OR THE
 * USE OR OTHER DEALINGS IN THE DATA.
 */

package edu.clemson.xagree.analysis.linearization;

import edu.clemson.xagree.xagree.BinaryExpr;
import edu.clemson.xagree.xagree.FnCallExpr;
import edu.clemson.xagree.xagree.IntLitExpr;
import edu.clemson.xagree.xagree.LinearizationDefExpr;
import edu.clemson.xagree.xagree.NestedDotID;
import edu.clemson.xagree.xagree.RealLitExpr;
import edu.clemson.xagree.xagree.UnaryExpr;
import edu.clemson.xagree.xagree.util.XagreeSwitch;

import edu.clemson.xagree.validation.XagreeJavaValidator;

public class MatlabPrintSwitch extends XagreeSwitch<String> {

	@Override
	public String caseLinearizationDefExpr(LinearizationDefExpr ctx) {
		return doSwitch(ctx.getExprBody());
	}

	@Override
	public String caseBinaryExpr(BinaryExpr ctx) {
		String left = doSwitch(ctx.getLeft());
		String op = ctx.getOp();
		String r = doSwitch(ctx.getRight());

		return "(" + left + " ." + op + " " + r + ")";
	}

	@Override
	public String caseUnaryExpr(UnaryExpr ctx) {
		String body = doSwitch(ctx.getExpr());
		return "(-" + body + ")";
	}

	@Override
	public String caseNestedDotID(NestedDotID ctx) {
		return XagreeJavaValidator.getFinalNestId(ctx).getName();
	}

	@Override
	public String caseFnCallExpr(FnCallExpr ctx) {
		String fn = XagreeJavaValidator.getFinalNestId(ctx.getFn()).getName();
		String arg = doSwitch(ctx.getArgs().get(0));
		return fn + "(" + arg + ")";
	}

	@Override
	public String caseIntLitExpr(IntLitExpr ctx) {
		return ctx.getVal();
	}

	@Override
	public String caseRealLitExpr(RealLitExpr ctx) {
		return ctx.getVal();
	}

}

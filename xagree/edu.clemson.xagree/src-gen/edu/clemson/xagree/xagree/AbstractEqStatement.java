/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Eq Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getAbstractEqStatement()
 * @model
 * @generated
 */
public interface AbstractEqStatement extends EqStatement
{
} // AbstractEqStatement

/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synch Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.xagree.xagree.SynchStatement#getVal <em>Val</em>}</li>
 *   <li>{@link edu.clemson.xagree.xagree.SynchStatement#getVal2 <em>Val2</em>}</li>
 *   <li>{@link edu.clemson.xagree.xagree.SynchStatement#getSim <em>Sim</em>}</li>
 * </ul>
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getSynchStatement()
 * @model
 * @generated
 */
public interface SynchStatement extends SpecStatement
{
  /**
   * Returns the value of the '<em><b>Val</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Val</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Val</em>' attribute.
   * @see #setVal(String)
   * @see edu.clemson.xagree.xagree.XagreePackage#getSynchStatement_Val()
   * @model
   * @generated
   */
  String getVal();

  /**
   * Sets the value of the '{@link edu.clemson.xagree.xagree.SynchStatement#getVal <em>Val</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Val</em>' attribute.
   * @see #getVal()
   * @generated
   */
  void setVal(String value);

  /**
   * Returns the value of the '<em><b>Val2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Val2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Val2</em>' attribute.
   * @see #setVal2(String)
   * @see edu.clemson.xagree.xagree.XagreePackage#getSynchStatement_Val2()
   * @model
   * @generated
   */
  String getVal2();

  /**
   * Sets the value of the '{@link edu.clemson.xagree.xagree.SynchStatement#getVal2 <em>Val2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Val2</em>' attribute.
   * @see #getVal2()
   * @generated
   */
  void setVal2(String value);

  /**
   * Returns the value of the '<em><b>Sim</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sim</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sim</em>' attribute.
   * @see #setSim(String)
   * @see edu.clemson.xagree.xagree.XagreePackage#getSynchStatement_Sim()
   * @model
   * @generated
   */
  String getSim();

  /**
   * Sets the value of the '{@link edu.clemson.xagree.xagree.SynchStatement#getSim <em>Sim</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sim</em>' attribute.
   * @see #getSim()
   * @generated
   */
  void setSim(String value);

} // SynchStatement

/**
 */
package edu.clemson.xagree.xagree;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.Element;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getExpr()
 * @model
 * @generated
 */
public interface Expr extends EObject, Element
{
} // Expr

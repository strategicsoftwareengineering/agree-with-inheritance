/**
 */
package edu.clemson.xagree.xagree;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pattern Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getPatternStatement()
 * @model
 * @generated
 */
public interface PatternStatement extends EObject
{
} // PatternStatement

/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Open Right Time Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getOpenRightTimeInterval()
 * @model
 * @generated
 */
public interface OpenRightTimeInterval extends TimeInterval
{
} // OpenRightTimeInterval

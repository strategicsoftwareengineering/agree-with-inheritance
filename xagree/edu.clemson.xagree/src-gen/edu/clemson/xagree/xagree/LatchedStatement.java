/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Latched Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getLatchedStatement()
 * @model
 * @generated
 */
public interface LatchedStatement extends SynchStatement
{
} // LatchedStatement

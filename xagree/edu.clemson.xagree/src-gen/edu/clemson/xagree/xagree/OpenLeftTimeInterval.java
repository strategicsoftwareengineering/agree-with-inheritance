/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Open Left Time Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getOpenLeftTimeInterval()
 * @model
 * @generated
 */
public interface OpenLeftTimeInterval extends TimeInterval
{
} // OpenLeftTimeInterval

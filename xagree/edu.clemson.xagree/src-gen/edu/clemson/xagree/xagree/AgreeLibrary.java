/**
 */
package edu.clemson.xagree.xagree;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.AnnexLibrary;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agree Library</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getAgreeLibrary()
 * @model
 * @generated
 */
public interface AgreeLibrary extends EObject, AnnexLibrary
{
} // AgreeLibrary

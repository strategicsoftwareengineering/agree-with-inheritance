/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getTimeExpr()
 * @model
 * @generated
 */
public interface TimeExpr extends Expr
{
} // TimeExpr

/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Asynch Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getAsynchStatement()
 * @model
 * @generated
 */
public interface AsynchStatement extends SynchStatement
{
} // AsynchStatement

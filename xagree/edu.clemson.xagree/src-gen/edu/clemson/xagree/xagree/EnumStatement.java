/**
 */
package edu.clemson.xagree.xagree;

import org.eclipse.emf.common.util.EList;

import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.xagree.xagree.EnumStatement#getEnums <em>Enums</em>}</li>
 * </ul>
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getEnumStatement()
 * @model
 * @generated
 */
public interface EnumStatement extends NamedElement, SpecStatement
{
  /**
   * Returns the value of the '<em><b>Enums</b></em>' containment reference list.
   * The list contents are of type {@link edu.clemson.xagree.xagree.NamedID}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Enums</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Enums</em>' containment reference list.
   * @see edu.clemson.xagree.xagree.XagreePackage#getEnumStatement_Enums()
   * @model containment="true"
   * @generated
   */
  EList<NamedID> getEnums();

} // EnumStatement

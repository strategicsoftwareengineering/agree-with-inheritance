/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inherit Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getInheritStatement()
 * @model
 * @generated
 */
public interface InheritStatement extends SpecStatement
{
} // InheritStatement

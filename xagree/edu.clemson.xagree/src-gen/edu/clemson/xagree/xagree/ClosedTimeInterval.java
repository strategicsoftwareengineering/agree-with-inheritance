/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Closed Time Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getClosedTimeInterval()
 * @model
 * @generated
 */
public interface ClosedTimeInterval extends TimeInterval
{
} // ClosedTimeInterval

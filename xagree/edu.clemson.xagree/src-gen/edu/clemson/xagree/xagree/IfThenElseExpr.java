/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Then Else Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.xagree.xagree.IfThenElseExpr#getA <em>A</em>}</li>
 *   <li>{@link edu.clemson.xagree.xagree.IfThenElseExpr#getB <em>B</em>}</li>
 *   <li>{@link edu.clemson.xagree.xagree.IfThenElseExpr#getC <em>C</em>}</li>
 * </ul>
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getIfThenElseExpr()
 * @model
 * @generated
 */
public interface IfThenElseExpr extends Expr
{
  /**
   * Returns the value of the '<em><b>A</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>A</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>A</em>' containment reference.
   * @see #setA(Expr)
   * @see edu.clemson.xagree.xagree.XagreePackage#getIfThenElseExpr_A()
   * @model containment="true"
   * @generated
   */
  Expr getA();

  /**
   * Sets the value of the '{@link edu.clemson.xagree.xagree.IfThenElseExpr#getA <em>A</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>A</em>' containment reference.
   * @see #getA()
   * @generated
   */
  void setA(Expr value);

  /**
   * Returns the value of the '<em><b>B</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>B</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>B</em>' containment reference.
   * @see #setB(Expr)
   * @see edu.clemson.xagree.xagree.XagreePackage#getIfThenElseExpr_B()
   * @model containment="true"
   * @generated
   */
  Expr getB();

  /**
   * Sets the value of the '{@link edu.clemson.xagree.xagree.IfThenElseExpr#getB <em>B</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>B</em>' containment reference.
   * @see #getB()
   * @generated
   */
  void setB(Expr value);

  /**
   * Returns the value of the '<em><b>C</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>C</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>C</em>' containment reference.
   * @see #setC(Expr)
   * @see edu.clemson.xagree.xagree.XagreePackage#getIfThenElseExpr_C()
   * @model containment="true"
   * @generated
   */
  Expr getC();

  /**
   * Sets the value of the '{@link edu.clemson.xagree.xagree.IfThenElseExpr#getC <em>C</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>C</em>' containment reference.
   * @see #getC()
   * @generated
   */
  void setC(Expr value);

} // IfThenElseExpr

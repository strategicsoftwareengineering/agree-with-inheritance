/**
 */
package edu.clemson.xagree.xagree;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.AnnexSubclause;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agree Subclause</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getAgreeSubclause()
 * @model
 * @generated
 */
public interface AgreeSubclause extends EObject, AnnexSubclause
{
} // AgreeSubclause

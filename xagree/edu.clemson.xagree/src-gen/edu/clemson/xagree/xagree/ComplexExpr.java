/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getComplexExpr()
 * @model
 * @generated
 */
public interface ComplexExpr extends Expr
{
} // ComplexExpr

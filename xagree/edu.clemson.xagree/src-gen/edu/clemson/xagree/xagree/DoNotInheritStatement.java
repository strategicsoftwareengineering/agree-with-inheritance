/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Do Not Inherit Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getDoNotInheritStatement()
 * @model
 * @generated
 */
public interface DoNotInheritStatement extends InheritStatement
{
} // DoNotInheritStatement

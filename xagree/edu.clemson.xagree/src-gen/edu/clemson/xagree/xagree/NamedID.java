/**
 */
package edu.clemson.xagree.xagree;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Named ID</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getNamedID()
 * @model
 * @generated
 */
public interface NamedID extends EObject, NamedElement
{
} // NamedID

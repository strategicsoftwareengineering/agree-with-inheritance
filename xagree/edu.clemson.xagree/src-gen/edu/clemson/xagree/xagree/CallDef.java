/**
 */
package edu.clemson.xagree.xagree;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Def</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getCallDef()
 * @model
 * @generated
 */
public interface CallDef extends EObject, NamedElement
{
} // CallDef

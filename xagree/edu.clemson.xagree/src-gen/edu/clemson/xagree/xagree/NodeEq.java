/**
 */
package edu.clemson.xagree.xagree;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Eq</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.xagree.xagree.NodeEq#getLhs <em>Lhs</em>}</li>
 * </ul>
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getNodeEq()
 * @model
 * @generated
 */
public interface NodeEq extends NodeStmt
{
  /**
   * Returns the value of the '<em><b>Lhs</b></em>' reference list.
   * The list contents are of type {@link edu.clemson.xagree.xagree.Arg}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lhs</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lhs</em>' reference list.
   * @see edu.clemson.xagree.xagree.XagreePackage#getNodeEq_Lhs()
   * @model
   * @generated
   */
  EList<Arg> getLhs();

} // NodeEq

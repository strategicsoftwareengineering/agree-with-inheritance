/**
 */
package edu.clemson.xagree.xagree;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Open Time Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.clemson.xagree.xagree.XagreePackage#getOpenTimeInterval()
 * @model
 * @generated
 */
public interface OpenTimeInterval extends TimeInterval
{
} // OpenTimeInterval

/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.DoNotInheritStatement;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Do Not Inherit Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DoNotInheritStatementImpl extends InheritStatementImpl implements DoNotInheritStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DoNotInheritStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.DO_NOT_INHERIT_STATEMENT;
  }

} //DoNotInheritStatementImpl

/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.AgreeLibrary;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

import org.osate.aadl2.impl.AnnexLibraryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agree Library</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AgreeLibraryImpl extends AnnexLibraryImpl implements AgreeLibrary
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AgreeLibraryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.AGREE_LIBRARY;
  }

} //AgreeLibraryImpl

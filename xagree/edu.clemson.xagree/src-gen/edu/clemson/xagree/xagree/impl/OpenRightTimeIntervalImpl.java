/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.OpenRightTimeInterval;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Open Right Time Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OpenRightTimeIntervalImpl extends TimeIntervalImpl implements OpenRightTimeInterval
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OpenRightTimeIntervalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.OPEN_RIGHT_TIME_INTERVAL;
  }

} //OpenRightTimeIntervalImpl

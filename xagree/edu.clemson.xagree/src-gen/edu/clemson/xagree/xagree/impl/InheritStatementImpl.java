/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.InheritStatement;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inherit Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InheritStatementImpl extends SpecStatementImpl implements InheritStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InheritStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.INHERIT_STATEMENT;
  }

} //InheritStatementImpl

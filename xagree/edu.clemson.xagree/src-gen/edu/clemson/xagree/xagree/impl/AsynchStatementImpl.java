/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.AsynchStatement;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Asynch Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AsynchStatementImpl extends SynchStatementImpl implements AsynchStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AsynchStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.ASYNCH_STATEMENT;
  }

} //AsynchStatementImpl

/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.SpecStatement;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

import org.osate.aadl2.impl.ElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Spec Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SpecStatementImpl extends ElementImpl implements SpecStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SpecStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.SPEC_STATEMENT;
  }

} //SpecStatementImpl

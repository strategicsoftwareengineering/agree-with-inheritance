/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.AgreeSubclause;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

import org.osate.aadl2.impl.AnnexSubclauseImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agree Subclause</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AgreeSubclauseImpl extends AnnexSubclauseImpl implements AgreeSubclause
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AgreeSubclauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.AGREE_SUBCLAUSE;
  }

} //AgreeSubclauseImpl

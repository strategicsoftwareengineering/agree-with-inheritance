/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.Contract;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

import org.osate.aadl2.impl.ElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Contract</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ContractImpl extends ElementImpl implements Contract
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ContractImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.CONTRACT;
  }

} //ContractImpl

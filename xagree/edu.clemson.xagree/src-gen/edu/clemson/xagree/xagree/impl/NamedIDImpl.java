/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.NamedID;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

import org.osate.aadl2.impl.NamedElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Named ID</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NamedIDImpl extends NamedElementImpl implements NamedID
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NamedIDImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.NAMED_ID;
  }

} //NamedIDImpl

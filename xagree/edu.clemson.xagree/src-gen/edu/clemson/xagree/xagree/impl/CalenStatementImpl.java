/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.CalenStatement;
import edu.clemson.xagree.xagree.XagreePackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.osate.aadl2.NamedElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Calen Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.xagree.xagree.impl.CalenStatementImpl#getEls <em>Els</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CalenStatementImpl extends SynchStatementImpl implements CalenStatement
{
  /**
   * The cached value of the '{@link #getEls() <em>Els</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEls()
   * @generated
   * @ordered
   */
  protected EList<NamedElement> els;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CalenStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.CALEN_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<NamedElement> getEls()
  {
    if (els == null)
    {
      els = new EObjectResolvingEList<NamedElement>(NamedElement.class, this, XagreePackage.CALEN_STATEMENT__ELS);
    }
    return els;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case XagreePackage.CALEN_STATEMENT__ELS:
        return getEls();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case XagreePackage.CALEN_STATEMENT__ELS:
        getEls().clear();
        getEls().addAll((Collection<? extends NamedElement>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case XagreePackage.CALEN_STATEMENT__ELS:
        getEls().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case XagreePackage.CALEN_STATEMENT__ELS:
        return els != null && !els.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //CalenStatementImpl

/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.AbstractEqStatement;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Eq Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AbstractEqStatementImpl extends EqStatementImpl implements AbstractEqStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AbstractEqStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.ABSTRACT_EQ_STATEMENT;
  }

} //AbstractEqStatementImpl

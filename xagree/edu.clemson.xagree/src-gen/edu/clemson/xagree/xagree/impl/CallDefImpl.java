/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.CallDef;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

import org.osate.aadl2.impl.NamedElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CallDefImpl extends NamedElementImpl implements CallDef
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CallDefImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.CALL_DEF;
  }

} //CallDefImpl

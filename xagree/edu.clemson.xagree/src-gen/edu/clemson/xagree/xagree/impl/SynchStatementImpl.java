/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.SynchStatement;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Synch Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.clemson.xagree.xagree.impl.SynchStatementImpl#getVal <em>Val</em>}</li>
 *   <li>{@link edu.clemson.xagree.xagree.impl.SynchStatementImpl#getVal2 <em>Val2</em>}</li>
 *   <li>{@link edu.clemson.xagree.xagree.impl.SynchStatementImpl#getSim <em>Sim</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SynchStatementImpl extends SpecStatementImpl implements SynchStatement
{
  /**
   * The default value of the '{@link #getVal() <em>Val</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVal()
   * @generated
   * @ordered
   */
  protected static final String VAL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVal() <em>Val</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVal()
   * @generated
   * @ordered
   */
  protected String val = VAL_EDEFAULT;

  /**
   * The default value of the '{@link #getVal2() <em>Val2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVal2()
   * @generated
   * @ordered
   */
  protected static final String VAL2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVal2() <em>Val2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVal2()
   * @generated
   * @ordered
   */
  protected String val2 = VAL2_EDEFAULT;

  /**
   * The default value of the '{@link #getSim() <em>Sim</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSim()
   * @generated
   * @ordered
   */
  protected static final String SIM_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSim() <em>Sim</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSim()
   * @generated
   * @ordered
   */
  protected String sim = SIM_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SynchStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.SYNCH_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVal()
  {
    return val;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVal(String newVal)
  {
    String oldVal = val;
    val = newVal;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XagreePackage.SYNCH_STATEMENT__VAL, oldVal, val));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVal2()
  {
    return val2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVal2(String newVal2)
  {
    String oldVal2 = val2;
    val2 = newVal2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XagreePackage.SYNCH_STATEMENT__VAL2, oldVal2, val2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSim()
  {
    return sim;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSim(String newSim)
  {
    String oldSim = sim;
    sim = newSim;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XagreePackage.SYNCH_STATEMENT__SIM, oldSim, sim));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case XagreePackage.SYNCH_STATEMENT__VAL:
        return getVal();
      case XagreePackage.SYNCH_STATEMENT__VAL2:
        return getVal2();
      case XagreePackage.SYNCH_STATEMENT__SIM:
        return getSim();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case XagreePackage.SYNCH_STATEMENT__VAL:
        setVal((String)newValue);
        return;
      case XagreePackage.SYNCH_STATEMENT__VAL2:
        setVal2((String)newValue);
        return;
      case XagreePackage.SYNCH_STATEMENT__SIM:
        setSim((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case XagreePackage.SYNCH_STATEMENT__VAL:
        setVal(VAL_EDEFAULT);
        return;
      case XagreePackage.SYNCH_STATEMENT__VAL2:
        setVal2(VAL2_EDEFAULT);
        return;
      case XagreePackage.SYNCH_STATEMENT__SIM:
        setSim(SIM_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case XagreePackage.SYNCH_STATEMENT__VAL:
        return VAL_EDEFAULT == null ? val != null : !VAL_EDEFAULT.equals(val);
      case XagreePackage.SYNCH_STATEMENT__VAL2:
        return VAL2_EDEFAULT == null ? val2 != null : !VAL2_EDEFAULT.equals(val2);
      case XagreePackage.SYNCH_STATEMENT__SIM:
        return SIM_EDEFAULT == null ? sim != null : !SIM_EDEFAULT.equals(sim);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (val: ");
    result.append(val);
    result.append(", val2: ");
    result.append(val2);
    result.append(", sim: ");
    result.append(sim);
    result.append(')');
    return result.toString();
  }

} //SynchStatementImpl

/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.LatchedStatement;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Latched Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LatchedStatementImpl extends SynchStatementImpl implements LatchedStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LatchedStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.LATCHED_STATEMENT;
  }

} //LatchedStatementImpl

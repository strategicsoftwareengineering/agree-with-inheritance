/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.ComplexExpr;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ComplexExprImpl extends ExprImpl implements ComplexExpr
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComplexExprImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.COMPLEX_EXPR;
  }

} //ComplexExprImpl

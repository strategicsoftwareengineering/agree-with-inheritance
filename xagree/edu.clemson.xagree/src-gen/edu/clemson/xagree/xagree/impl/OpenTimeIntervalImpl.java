/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.OpenTimeInterval;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Open Time Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OpenTimeIntervalImpl extends TimeIntervalImpl implements OpenTimeInterval
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OpenTimeIntervalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.OPEN_TIME_INTERVAL;
  }

} //OpenTimeIntervalImpl

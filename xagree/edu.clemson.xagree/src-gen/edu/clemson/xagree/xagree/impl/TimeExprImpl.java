/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.TimeExpr;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TimeExprImpl extends ExprImpl implements TimeExpr
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TimeExprImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.TIME_EXPR;
  }

} //TimeExprImpl

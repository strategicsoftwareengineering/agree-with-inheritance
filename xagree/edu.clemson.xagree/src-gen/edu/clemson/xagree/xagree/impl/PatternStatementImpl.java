/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.PatternStatement;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pattern Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PatternStatementImpl extends MinimalEObjectImpl.Container implements PatternStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PatternStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.PATTERN_STATEMENT;
  }

} //PatternStatementImpl

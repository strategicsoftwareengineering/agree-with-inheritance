/**
 */
package edu.clemson.xagree.xagree.impl;

import edu.clemson.xagree.xagree.ClosedTimeInterval;
import edu.clemson.xagree.xagree.XagreePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Closed Time Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClosedTimeIntervalImpl extends TimeIntervalImpl implements ClosedTimeInterval
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ClosedTimeIntervalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XagreePackage.Literals.CLOSED_TIME_INTERVAL;
  }

} //ClosedTimeIntervalImpl

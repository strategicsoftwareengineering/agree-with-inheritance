package edu.clemson.xagree.scoping;

public class XagreeScopingException extends RuntimeException {

    private static final long serialVersionUID = -4438958218666564326L;

    public XagreeScopingException(String message) {
        super(message);
    }
}

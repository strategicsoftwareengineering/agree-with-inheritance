package edu.clemson.xagree.visitors;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import edu.clemson.xagree.xagree.BinaryExpr;
import edu.clemson.xagree.xagree.BoolLitExpr;
import edu.clemson.xagree.xagree.EventExpr;
import edu.clemson.xagree.xagree.Expr;
import edu.clemson.xagree.xagree.FloorCast;
import edu.clemson.xagree.xagree.FnCallExpr;
import edu.clemson.xagree.xagree.GetPropertyExpr;
import edu.clemson.xagree.xagree.IfThenElseExpr;
import edu.clemson.xagree.xagree.IntLitExpr;
import edu.clemson.xagree.xagree.NestedDotID;
import edu.clemson.xagree.xagree.PreExpr;
import edu.clemson.xagree.xagree.PrevExpr;
import edu.clemson.xagree.xagree.RealCast;
import edu.clemson.xagree.xagree.RealLitExpr;
import edu.clemson.xagree.xagree.RecordExpr;
import edu.clemson.xagree.xagree.RecordUpdateExpr;
import edu.clemson.xagree.xagree.ThisExpr;
import edu.clemson.xagree.xagree.UnaryExpr;
import edu.clemson.xagree.xagree.util.XagreeSwitch;

public class ExprCycleVisitor extends XagreeSwitch<Set<EObject>> {
  
    private String id;

    public ExprCycleVisitor(String id) {
        this.id = id;
    }
    
    @Override
    public Set<EObject> caseExpr(Expr expr){
    	return new HashSet<EObject>();
    }
    
    @Override
    public Set<EObject> caseBinaryExpr(BinaryExpr e){
        Set<EObject> result = new HashSet<EObject>();
        result.addAll(doSwitch(e.getLeft()));
        result.addAll(doSwitch(e.getRight()));
        return result;
    }
    
    @Override
    public Set<EObject> caseBoolLitExpr(BoolLitExpr e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> caseNestedDotID(NestedDotID e){
        Set<EObject> result = new HashSet<EObject>();
        if(e.getBase().getName().equals(id)){
            result.add(e);
        }
        return result;
    }
    
    @Override
    public Set<EObject> caseFnCallExpr(FnCallExpr e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> caseRecordExpr(RecordExpr e){
       Set<EObject> result = new HashSet<EObject>();
       for(Expr expr : e.getArgExpr()){
           result.addAll(doSwitch(expr));
       }
       return result;
    }
    
    @Override
    public Set<EObject> caseEventExpr(EventExpr e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> caseFloorCast(FloorCast e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> caseGetPropertyExpr(GetPropertyExpr e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> caseIfThenElseExpr(IfThenElseExpr e){
        Set<EObject> result = new HashSet<EObject>();
        result.addAll(doSwitch(e.getA()));
        result.addAll(doSwitch(e.getB()));
        result.addAll(doSwitch(e.getC()));
        
        return result;
    }
    
    @Override
    public Set<EObject> caseIntLitExpr(IntLitExpr e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> casePreExpr(PreExpr e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> casePrevExpr(PrevExpr e){
        Set<EObject> result = new HashSet<EObject>();
        result.addAll(doSwitch(e.getInit()));
        return result;
    }
    
    @Override
    public Set<EObject> caseRealCast(RealCast e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> caseRealLitExpr(RealLitExpr e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> caseRecordUpdateExpr(RecordUpdateExpr e){
        Set<EObject> result = new HashSet<EObject>();
        
        for(Expr expr : e.getArgExpr()){
            result.addAll(doSwitch(expr));
        }
        return result;
    }
    
    @Override
    public Set<EObject> caseThisExpr(ThisExpr e){
        return Collections.emptySet();
    }
    
    @Override
    public Set<EObject> caseUnaryExpr(UnaryExpr e){
        Set<EObject> result = new HashSet<EObject>();
        result.addAll(doSwitch(e.getExpr()));
        return result;
    }
}

package edu.clemson.xagree;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.EnumerationLiteral;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedValue;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;

public class XagreeAADLEnumerationUtils {

	public static boolean isAADLEnumeration(EObject eobj) {
		// TODO: There's a better way to do this...
		// Once "Enumeration_Type" has been canonicalized into Base_Types,
		// we can merely look for that as an ancestor of the dataClass.
		return (eobj instanceof ComponentClassifier) && ((ComponentClassifier) eobj).getAllPropertyAssociations()
				.stream()
				.anyMatch(pa -> "Data_Representation".equals(pa.getProperty().getName()) && pa.getOwnedValues().stream()
						.anyMatch(pv -> (pv.getOwnedValue() instanceof NamedValue)
								&& (((NamedValue) pv.getOwnedValue())
										.getNamedValue() instanceof org.osate.aadl2.EnumerationLiteral)
								&& "Enum"
								.equals(((EnumerationLiteral) ((NamedValue) pv.getOwnedValue()).getNamedValue())
										.getName())));
	}

	public static Property getEnumeratorProperty(ComponentClassifier componentClassifier) {
		Property result = null;
		if (isAADLEnumeration(componentClassifier)) {
			for (PropertyAssociation propertyAssociation : componentClassifier.getOwnedPropertyAssociations().stream()
					.filter(pa -> "Enumerators".equalsIgnoreCase(pa.getProperty().getName()))
					.collect(Collectors.toList())) {
				result = propertyAssociation.getProperty();
			}
		}
		return result;
	}

	public static List<PropertyExpression> getEnumerators(ComponentClassifier componentClassifier) {
		List<PropertyExpression> result = new ArrayList<>();
		if (isAADLEnumeration(componentClassifier)) {
			for (PropertyAssociation propertyAssociation : componentClassifier.getOwnedPropertyAssociations().stream()
					.filter(pa -> "Enumerators".equalsIgnoreCase(pa.getProperty().getName()))
					.collect(Collectors.toList())) {
				for (ModalPropertyValue propertyValue : propertyAssociation.getOwnedValues()) {
					PropertyExpression propertyExpression = propertyValue.getOwnedValue();
					if (propertyExpression instanceof ListValue) {
						for (PropertyExpression listElement : ((ListValue) propertyExpression).getOwnedListElements()) {
							result.add(listElement);
						}
					}
				}
			}
		}
		return result;
	}

}

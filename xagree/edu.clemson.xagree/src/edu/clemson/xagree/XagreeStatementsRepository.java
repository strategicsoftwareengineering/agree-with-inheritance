package edu.clemson.xagree;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import edu.clemson.xagree.xagree.SpecStatement;
import edu.clemson.xagree.xagree.impl.PrimTypeImpl;
import edu.clemson.xagree.xagree.AbstractEqStatement;
import edu.clemson.xagree.xagree.EqStatement;
import edu.clemson.xagree.xagree.GuaranteeStatement;
import edu.clemson.xagree.xagree.AgreeContract;
import edu.clemson.xagree.xagree.AssumeStatement;
import edu.clemson.xagree.xagree.DoNotInheritStatement;

public class XagreeStatementsRepository {
	private Map<String, AgreeContract> componentToStatements = new HashMap<String, AgreeContract>(); 
	private Map<String, Set<String>> children = new HashMap<String, Set<String>>();
	private Map<String, String> parents = new HashMap<String, String>();
	
	public void addChild(String parentName, String childName) {
		if(children.get(parentName) == null) {
			children.put(parentName, new HashSet<String>());
		}
		children.get(parentName).add(childName);
		parents.put(childName, parentName);
	}
	
	public void setStatements(String componentName, AgreeContract contract) {
		componentToStatements.put(componentName, contract);
	}
	
	private List<SpecStatement> getInheritedStatements(String component) {
		ArrayList<SpecStatement> parentStatements = new ArrayList<SpecStatement>();
		ArrayList<SpecStatement> myStatements = new ArrayList<SpecStatement>();
		if(parents.get(component) != null) {
			parentStatements.addAll(getInheritedStatements(parents.get(component)));
		}
		boolean shouldInheritParents = true;
		if(componentToStatements.get(component) != null) {
			for(SpecStatement statement : componentToStatements.get(component).getSpecs()) {
				if(statement instanceof DoNotInheritStatement) {
					shouldInheritParents = false;
				}
				myStatements.add(statement);
			}
		}
		if(shouldInheritParents) {
			myStatements.addAll(0, parentStatements);
		}
		return myStatements;
	}
	
	private List<SpecStatement> compressStatements(List<SpecStatement> statements) throws Exception {
		ArrayList<SpecStatement> compressed = new ArrayList<SpecStatement>();
		for(int i = 0; i < statements.size(); ++i) {
			SpecStatement statement1 = statements.get(i);
			if(statement1 instanceof EqStatement || statement1 instanceof AbstractEqStatement) {
				boolean add = true;
				for(int j = i + 1; j < statements.size(); ++j) {	
					SpecStatement statement2 = statements.get(j);
					if(statement2 instanceof EqStatement || statement2 instanceof AbstractEqStatement) {
						EqStatement eqStatement1 = (EqStatement)statement1;
						EqStatement eqStatement2 = (EqStatement)statement2;
						if(eqStatement1.getLhs().getName().equals(eqStatement2.getLhs().getName())) {
							add = false;
							PrimTypeImpl type1 = (PrimTypeImpl)eqStatement1.getLhs().getType();
							PrimTypeImpl type2 = (PrimTypeImpl)eqStatement2.getLhs().getType();
							if(!type1.getString().equals(type2.getString())) {
								throw new Exception("statements with the same name but different types exist in the heirarchy");
							}
						}
					}
				}
				if(add) {
					compressed.add(statement1);
				}
			} else if(statement1 instanceof AssumeStatement) {
				boolean add = true;
				for(int j = i + 1; j < statements.size(); ++j) {	
					SpecStatement statement2 = statements.get(j);
					if(statement2 instanceof AssumeStatement) {
						AssumeStatement assumeStatement1 = (AssumeStatement)statement1;
						AssumeStatement assumeStatement2 = (AssumeStatement)statement2;
						if(assumeStatement1.getStr().equals(assumeStatement2.getStr())) {
							add = false;
						}
					}
				}
				if(add) {
					compressed.add(statement1);
				}
			} else if(statement1 instanceof GuaranteeStatement) {
				boolean add = true;
				for(int j = i + 1; j < statements.size(); ++j) {	
					SpecStatement statement2 = statements.get(j);
					if(statement2 instanceof GuaranteeStatement) {
						GuaranteeStatement guaranteeStatement1 = (GuaranteeStatement)statement1;
						GuaranteeStatement guaranteeStatement2 = (GuaranteeStatement)statement2;
						if(guaranteeStatement1.getStr().equals(guaranteeStatement2.getStr())) {
							add = false;
						}
					}
				}
				if(add) {
					compressed.add(statement1);
				}
			} else {
				compressed.add(statement1);
			}
		}
		return compressed;
	}
	
	public boolean isAbstract(String componentName) throws Exception {
		boolean isAbstract = false;

		for(SpecStatement statement : compressStatements(getInheritedStatements(componentName))) {
			if(statement instanceof AbstractEqStatement) {
				isAbstract = true;
			}
		}
		return isAbstract;
	}
	
	public void checkEqStatement(String componentName, EqStatement eqStatement) throws Exception {
		compressStatements(getInheritedStatements(componentName));
	}
	
	public void checkAbstractEqStatement(String componentName, AbstractEqStatement eqStatement) throws Exception {
		compressStatements(getInheritedStatements(componentName));
	}


	public List<SpecStatement> getStatements(String componentName) throws Exception { 
		return compressStatements(getInheritedStatements(componentName));
	}

	private static XagreeStatementsRepository repository;

	public static XagreeStatementsRepository getRepistory() {
		if(repository == null) {
			repository = new XagreeStatementsRepository();
		}
		return repository;
	}
}
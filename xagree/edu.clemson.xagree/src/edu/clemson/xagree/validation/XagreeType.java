package edu.clemson.xagree.validation;

public class XagreeType {
    public static final XagreeType BOOL = new XagreeType("bool");
    public static final XagreeType INT = new XagreeType("int");
    public static final XagreeType REAL = new XagreeType("real");
    public static final XagreeType ERROR = new XagreeType("<error>");

    private String name;

    public XagreeType(String name) {
        this.name = name;
    }

    public boolean isPrimitive() {
        return equals(BOOL) || equals(INT) || equals(REAL);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof XagreeType) {
            XagreeType other = (XagreeType) o;
            return name.equals(other.name);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }
}

package edu.clemson.xagree.validation;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import edu.clemson.xagree.xagree.BinaryExpr;
import edu.clemson.xagree.xagree.CallDef;
import edu.clemson.xagree.xagree.Expr;
import edu.clemson.xagree.xagree.FnCallExpr;
import edu.clemson.xagree.xagree.FnDefExpr;
import edu.clemson.xagree.xagree.IfThenElseExpr;
import edu.clemson.xagree.xagree.NestedDotID;
import edu.clemson.xagree.xagree.NodeBodyExpr;
import edu.clemson.xagree.xagree.NodeDefExpr;
import edu.clemson.xagree.xagree.NodeEq;
import edu.clemson.xagree.xagree.NodeLemma;
import edu.clemson.xagree.xagree.NodeStmt;
import edu.clemson.xagree.xagree.PreExpr;
import edu.clemson.xagree.xagree.PrevExpr;
import edu.clemson.xagree.xagree.UnaryExpr;
import edu.clemson.xagree.xagree.util.XagreeSwitch;

public class FnCallRecursionHelper extends XagreeSwitch<Expr>{
    
    public LinkedList<CallDef> visited;
    public List<LinkedList<CallDef>> loops;
    
    public void doSwitchPreserveVisited(Expr expr){
    	LinkedList<CallDef> copyOfVisited = new LinkedList<>(visited);
    	doSwitch(expr);
        visited = copyOfVisited;
    }
    
    public FnCallRecursionHelper(){
        visited = new LinkedList<>();
        loops = new ArrayList<>();
    }

    @Override
    public Expr caseBinaryExpr(BinaryExpr object) {
        doSwitchPreserveVisited(object.getLeft());
        doSwitchPreserveVisited(object.getRight());
        return null;
    }

    @Override
    public Expr caseUnaryExpr(UnaryExpr object) {
        doSwitchPreserveVisited(object.getExpr());
        return null;
    }

    @Override
    public Expr caseIfThenElseExpr(IfThenElseExpr object) {
        doSwitchPreserveVisited(object.getC());
        doSwitchPreserveVisited(object.getB());
        doSwitchPreserveVisited(object.getA());
        return null;
    }

    @Override
    public Expr casePrevExpr(PrevExpr object) {
        doSwitchPreserveVisited(object.getDelay());
        doSwitchPreserveVisited(object.getInit());
        return null;
    }

    @Override
    public Expr casePreExpr(PreExpr object) {
        doSwitchPreserveVisited(object.getExpr());
        return null;
    }

    @Override
    public Expr caseNodeBodyExpr(NodeBodyExpr object){
        
        for(NodeStmt stmt : object.getStmts()){
            doSwitch(stmt);
        }
        return null;
    }
    
    @Override
    public Expr caseNodeEq(NodeEq object){
        doSwitchPreserveVisited(object.getExpr());
        return null;
    }
    
    @Override
    public Expr caseNodeLemma(NodeLemma object){
        doSwitchPreserveVisited(object.getExpr());
        return null;
    }
    
    @Override
    public Expr caseNodeDefExpr(NodeDefExpr object){
        
        if(visited.contains(object)){
            LinkedList<CallDef> loop = new LinkedList<CallDef>(visited);
            loop.push(object);
            loops.add(loop);
        }else{
            visited.push(object);
            doSwitch(object.getNodeBody());
        }
        
        return null;
    }
    
    @Override
    public Expr caseFnDefExpr(FnDefExpr object){
        
        if(visited.contains(object)){
            LinkedList<CallDef> loop = new LinkedList<CallDef>(visited);
            loop.push(object);
            loops.add(loop);
        }else{
            visited.push(object);
            doSwitchPreserveVisited(object.getExpr());
        }
        return null;
    }
    
    @Override
    public Expr caseFnCallExpr(FnCallExpr object) {
        NestedDotID id = object.getFn();
        
        while(id.getSub() != null){
            id = id.getSub();
        }
        CallDef callDef = null;
        
        try{
            callDef = (CallDef)id.getBase();
        }catch(ClassCastException e){
            return null;
        }
        
        doSwitch(callDef);
        return null;
    }


}

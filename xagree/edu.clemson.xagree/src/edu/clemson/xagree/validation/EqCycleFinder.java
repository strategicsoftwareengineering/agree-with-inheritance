package edu.clemson.xagree.validation;

import java.util.ArrayList;
import java.util.List;


import edu.clemson.xagree.xagree.AgreeContract;
import edu.clemson.xagree.xagree.EqStatement;
import edu.clemson.xagree.xagree.SpecStatement;

public class EqCycleFinder {

    public static List<EqStatement> noDelayCycles(AgreeContract contract){
        List<EqStatement> eqs = new ArrayList<>();
        for(SpecStatement spec : contract.getSpecs()){
            if(spec instanceof EqStatement){
                eqs.add((EqStatement) spec);
            }
        }
        return getFirstCycle(eqs);
    }

    private static List<EqStatement> getFirstCycle(List<EqStatement> eqs) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
